/** @flow */
import React, {Component} from 'react'
import withStyles from '@material-ui/core/es/styles/withStyles'
import styles from './styles'
import {localize} from "../../services";
import {formMode, layoutType, positionButton} from "../../config/Constants";
import InputForm from "../../components/UI/InputForm/InputForm";
import {registerForm} from "../../forms";


class LoginDialog extends Component {
    constructor(props) {
        super(props)
        this.state = {
            registerFormFields: JSON.parse(JSON.stringify(registerForm))
        }
    }

    render() {
        const {registerFormFields} = this.state
        return (
            <div style={styles.container}>
                    <div style={styles.dialogHeaderRegisterResetScreen}>
                        <label style={styles.dialogTitle}>{localize('registerScreen.welcome')}</label>
                    </div>
                    <InputForm formElements={registerFormFields}
                               prepareDataAndSubmit={this.submitFormAction} formContainer={styles.formContainer}
                               buttonLabel={localize('registerScreen.register')}
                               mode={formMode.NEW} data={null} layout={layoutType.COLUMNS_1}
                               lastElementInLayout={layoutType.COLUMNS_1}
                               saveButtonPosition={positionButton.CENTER}>
                    </InputForm>

                </div>

        )
    }
}


export default withStyles(styles)(LoginDialog)
