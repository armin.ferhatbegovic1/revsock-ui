/** @flow */
import React, {Component} from 'react'
import connect from 'react-redux/es/connect/connect'
import withStyles from '@material-ui/core/es/styles/withStyles'
import RegisterDialog from "./RegisterDialog";
import styles from './styles'

class RegisterScreen extends Component {
    render() {

        return (
            <div style={styles.loginScreenContainer}>
                <RegisterDialog/>
            </div>
        )
    }
}

const mapStateToProps = state => {
    return {
        auth: state.auth,
        screen: state.screen
    }
}

export default connect(mapStateToProps)(withStyles(styles)(RegisterScreen))
