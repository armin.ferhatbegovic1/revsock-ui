import Dimensions from '../../utils/Dimensions'
import Colors from '../../utils/Colors'
import Images from "../../utils/Images";

const RootContainerStyles = {
    root: {
        height: 'inherit',
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center'
    },
    card: {
        width: '350px',
        height: '300px'
    },
    containerFull: {
        width: '100% !important',
        height: '100% !important',
        overflowY: 'auto !important',
        overflowX: 'hidden !important'
    },
    paddingFullStandard: {
        padding: '16px !important'
    },
    marginTop50: {
        marginTop: '50px'
    },
    marginTop25: {
        marginTop: '25px'
    },
    width75: {
        width: '70%'
    },
    flexEnd: {
        justifyContent: 'flex-end'
    },
    centered: {
        alignItems: 'center'
    },
    flex: {
        display: 'flex'
    },
    flexFiller: {
        flex: 1
    },
    fullWidth: {
        width: '100%'
    },
    fullHeight: {
        height: '100%'
    },
    horizontalCenter: {
        display: 'flex',
        justifyContent: 'center',
        flexDirection: 'row'
    },
    horizontalStart: {
        display: 'flex',
        justifyContent: 'flex-start',
        flexDirection: 'row'
    },
    horizontalEnd: {
        display: 'flex',
        justifyContent: 'flex-end',
        flexDirection: 'row'
    },
    verticalCenter: {
        display: 'flex',
        alignItems: 'center',
        flexDirection: 'column'
    },
    fullCenter: {
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center'
    },
    flexDirectionRow: {
        display: 'flex',
        flexDirection: 'row',
    },
    flexDirectionRowWrap: {
        display: 'flex',
        flexDirection: 'row',
        flexWrap: 'wrap'
    },
    flexDirectionColumn: {
        display: 'flex',
        flexDirection: 'column'
    },
    textCenter: {
        textAlign: 'center'
    },
    textLeft: {
        textAlign: 'left'
    }, checkBoxSecondary: {
        color: Colors.secondary
    },
    loginCardForm: {
        width: '380px',
        height: '530px'
    },
    loginCardMobileForm: {
        height: '100vh',
        minWidth: 400
    },
    registerCardForm: {
        width: '700px',
        height: '600px'
    },
    registerCardMobileForm: {
        minWidth: 400,
        height: '100vh'
    },
    introText: {
        marginTop: Dimensions.baseMargin,
        marginBottom: Dimensions.baseMargin,
        color: Colors.color5,
        textAlign: 'center',
        display: 'flex',
        justifyContent: 'center',
        fontSize: Dimensions.textMedium
    },
    paper: {
        margin: 24
    },
    responseTitle: {
        fontWeight: 'bold',
        fontSize: Dimensions.textX4Large,
        marginTop: Dimensions.doubleBaseMargin,
        marginBottom: Dimensions.doubleBaseMargin,
        color: Colors.secondary
    },
    responseSubTitle: {
        fontWeight: 'bold',
        fontSize: Dimensions.textLarge,
        color: Colors.color5
    },
    responseGetStartedButton: {
        display: 'flex',
        flexFlow: 'row wrap',
        justifyContent: 'space-evenly',
        width: '100%',
        margin: Dimensions.doubleBaseMargin * 2
    },
    responseTextLabel: {
        margin: Dimensions.baseMargin,
        fontWeight: 'bold',
        color: Colors.color5
    },
    filterContainer: {
        flex: 1
    },
    loginDialogContainer: {
        position: 'absolute', /*it can be fixed too*/
        left: 0, right: 0,
        top: 0, bottom: 0,
        margin: 'auto',
        width: 680,
        maxHeight: 400,
        background: 'rgba(255,255,255,1)',
        borderRadius: '6px',
        display: 'flex'
    },
    loginDialogContainerLeft: {
        flex: '40%',
        backgroundImage: "url(" + Images.loginScreenPattern + ")",
        // background-position: center; /* Center the image */
        backgroundRepeat: 'no-repeat',
        backgroundSize: '100% 100%'
    },

    loginDialogContainerRight: {
        flex: '60%',
        padding: 30,
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'space-evenly',
        height:'100%'
    },
    dialogTitle: {
        fontSize: Dimensions.textXSmall,
        fontFamily: 'PingFang SC',
        fontWeight: 'bold',
        color: 'rgba(1,1,1,1)',
        lineHeight: '25px',
        marginBottom:Dimensions.doubleBaseMargin
    },
    screenContainer: {
        width: '80%',
        margin: '0 auto'
    },
    radioButton: {color: Colors.primary},
    paperInnerContainer: {
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'flex-start',
        marginLeft: Dimensions.doubleBaseMargin,
        marginRight: Dimensions.doubleBaseMargin,
        justifyContent: 'space-around'
    },
    circlesContainer: {
        display: 'flex',
        flexDirection: 'row',
        width: 140,
        justifyContent: 'space-evenly',
        marginTop: Dimensions.baseMargin,
        marginBottom: Dimensions.baseMargin,
        alignSelf: 'flex-end',
        height: 30

    },
    paperFooter: {
        width: Dimensions.fullWidth,
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginTop: Dimensions.tripeBaseMargin
    },
    dialogHeader: {
        display: 'flex',
        flexDirection: 'row',
        marginBottom:Dimensions.doubleBaseMargin,
    },
    dialogTitleLabel:{
        fontFamily: "PingFang Regular",
        fontWeight: 'bold',
        fontSize: Dimensions.textSmall,
        color: Colors.textGray2TitleColor
    },
    dialogFooter: {
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginBottom:Dimensions.doubleBaseMargin
    },
    redirectMainButton: {
        fontFamily: "PingFang Regular",
        fontWeight: 'bold',
        fontSize: Dimensions.textSmall-2,
        color: Colors.textGrayTitleColor
    },
    loginScreenContainer: {
        width: '100%',
        height: '100%',
    },
    container: {
        position: 'relative', /*it can be fixed too*/
        left: 0, right: 0,
        top: '10%',
        margin: 'auto',
        maxWidth: 650,
        maxHeight: 700,
        justifyContent: 'center',
        background: 'rgba(255,255,255,1)',
        borderRadius: '6px',
        display: 'flex',
        flexDirection: 'column',
        paddingLeft: '10%',
        paddingRight: '10%',
        paddingTop:'4%',
        paddingBottom:'4%'
    },
    containerColumn2: {
        flex: '70%',
        padding: 30,
        backgroundColor: 'blue'
    },
    dialogHeaderRegisterResetScreen: {
        display: 'flex',
        flexDirection: 'row'
    },
    dialogFooterRegisterResetScreen: {
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'space-between'
    }
}

export default RootContainerStyles