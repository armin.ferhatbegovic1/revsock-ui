/** @flow */
import React, {Component} from 'react'
import withStyles from '@material-ui/core/es/styles/withStyles'
import styles from './styles'
import {localize} from "../../services";
import {formMode, layoutType, positionButton} from "../../config/Constants";
import InputForm from "../../components/UI/InputForm/InputForm";
import {resetPasswordForm} from "../../forms";


class LoginDialog extends Component {
    constructor(props) {
        super(props)
        this.state = {
            resetPasswordFormFields: JSON.parse(JSON.stringify(resetPasswordForm))
        }
    }

    render() {
        const {resetPasswordFormFields} = this.state
        return (
            <div style={styles.container}>
                    <div style={styles.dialogHeader}>
                        <label style={styles.dialogTitle}>{localize('resetPasswordScreen.youAreResettingYourPassword')}</label>
                    </div>
                    <InputForm formElements={resetPasswordFormFields}
                               prepareDataAndSubmit={this.submitFormAction} formContainer={styles.formContainer}
                               buttonLabel={localize('resetPasswordScreen.resetPassword')}
                               mode={formMode.NEW} data={null} layout={layoutType.COLUMNS_1}
                               lastElementInLayout={layoutType.COLUMNS_1}
                               saveButtonPosition={positionButton.CENTER}>
                    </InputForm>

                </div>
        )
    }
}


export default withStyles(styles)(LoginDialog)
