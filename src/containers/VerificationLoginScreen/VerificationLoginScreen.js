/** @flow */
import React, {Component} from 'react'
import connect from 'react-redux/es/connect/connect'
import withStyles from '@material-ui/core/es/styles/withStyles'
import LoginDialog from "./VerificationLoginDialog";
import styles from './styles'

class VerificationLoginScreen extends Component {
    render() {

        return (
            <div style={styles.loginScreenContainer}>
                <LoginDialog/>
            </div>
        )
    }
}

const mapStateToProps = state => {
    return {
        auth: state.auth,
        screen: state.screen
    }
}

export default connect(mapStateToProps)(withStyles(styles)(VerificationLoginScreen))
