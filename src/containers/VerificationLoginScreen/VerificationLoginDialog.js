/** @flow */
import React, {Component} from 'react'
import withStyles from '@material-ui/core/es/styles/withStyles'
import styles from './styles'
import {localize} from "../../services";
import {formMode, layoutType, positionButton} from "../../config/Constants";
import InputForm from "../../components/UI/InputForm/InputForm";
import {verificationLoginScreen} from "../../forms";
import RedirectButton from "../../components/UI/RedirectButton/RedirectButton";

class VerificationLoginDialog extends Component {
    constructor(props) {
        super(props)
        this.state = {
            verificationLoginScreenFields: JSON.parse(JSON.stringify(verificationLoginScreen))
        }
    }

    render() {
        const {verificationLoginScreenFields} = this.state
        return (
            <div style={styles.loginDialogContainer}>
                <div style={styles.loginDialogContainerLeft}></div>
                <div style={styles.loginDialogContainerRight}>
                    <div style={styles.dialogHeader}>
                        <label style={styles.dialogTitleLabel}>{localize('siteName')}</label>
                    </div>
                    <InputForm formElements={verificationLoginScreenFields}
                               prepareDataAndSubmit={this.submitFormAction} formContainer={styles.formContainer}
                               buttonLabel={localize('verificationLoginScreen.logIn')}
                               mode={formMode.NEW} data={null} layout={layoutType.COLUMNS_1}
                               lastElementInLayout={layoutType.COLUMNS_1}
                               saveButtonPosition={positionButton.CENTER}>
                    </InputForm>
                    <div style={styles.dialogFooter}>
                        <RedirectButton label={localize('verificationLoginScreen.passwordLogin')}
                                        route='/login-password'/>
                        <RedirectButton label={localize('verificationLoginScreen.notRegistered')}
                                        route='/register'/>
                    </div>

                </div>

            </div>
        )
    }
}


export default withStyles(styles)(VerificationLoginDialog)
