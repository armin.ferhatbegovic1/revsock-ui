import React from 'react';
import {makeStyles} from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import InnerSectionTitle from "../../../components/UI/InnerSectionTitle/InnerSectionTitle";
import {localize} from "../../../services";
import {Area, WayOfReturn} from "./helpers";
import ShowPrice from '../../../components/UI/ShowPrice/ShowPrice'
import IncrementerDisplay from "../../../components/UI/IncrementerDisplay/IncrementerDisplay";
import MainButton from "../../../components/UI/MainButton/MainButton";
import {subMainButtonType} from "../../../config/Constants";
import RootContainerStyles from "../../Styles/RootContainerStyles";

const useStyles = makeStyles(theme => ({
    ...RootContainerStyles,
    root: {
        flexGrow: 1,
    },
    paper: {
        padding: '16px 0',
        textAlign: 'center',
        color: theme.palette.text.secondary,
    },
    paperInnerContainerHeight: {
        height: 300
    }
}));

export default function MyAPISection() {
    const classes = useStyles();

    return (
        <Grid container spacing={3}>
            <Grid item xs={12} sm={12}>
                <Paper className={classes.paper}>
                    <InnerSectionTitle>{localize('homeScreen.myAPI')}</InnerSectionTitle>
                    <div className={classes.paperInnerContainer + ' ' + classes.paperInnerContainerHeight}>
                        <WayOfReturn label={localize('homeScreen.wayOfReturn')}/>
                        <Area title={localize('homeScreen.area')}/>
                        <IncrementerDisplay label={localize('homeScreen.purchaseQuantity')}/>
                        <div className={classes.paperFooter}>
                            <ShowPrice label={localize('homeScreen.price')} currency='BTC' value='0.00032'/>

                            <MainButton width={200} label={localize('homeScreen.buyNow')}
                                        buttonType={subMainButtonType.PRIMARY}/>

                        </div>
                    </div>
                </Paper>
            </Grid>
        </Grid>

    );
}