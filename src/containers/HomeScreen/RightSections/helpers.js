import React from 'react'
import RootContainerStyles from "../../Styles/RootContainerStyles";
import RadioGroup from '@material-ui/core/RadioGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormControl from '@material-ui/core/FormControl';
import FormLabel from '@material-ui/core/FormLabel';
import CircleChecked from '@material-ui/icons/CheckCircleOutline';
import Checkbox from '@material-ui/core/Checkbox';
import CircleUnchecked from '@material-ui/icons/RadioButtonUnchecked';
import {localize} from "../../../services";
import Dimensions from "../../../utils/Dimensions";
import Colors from "../../../utils/Colors";

const styles = {
    ...RootContainerStyles,
    containerWrapper: {
        marginTop: Dimensions.doubleBaseMargin,
        display: 'flex',
        flexDirection: 'column'
    },
    radioButtons: {},
    title: {textAlign: 'left'},
    showPriceLabel: {
        fontSize: Dimensions.textSmall,
        color: Colors.black,
        textAlign: 'left'
    },
    showPriceValueCurrency: {
        fontSize: Dimensions.textMedium,
        color: Colors.red
    }
}

export function ServiceAgreement(props) {
    const {title} = props
    const [value, setValue] = React.useState('female');
    const handleChange = event => {
        setValue(event.target.value);
    };
    return <div style={styles.containerWrapper}>
        <div style={styles.radioButtons}>
            <FormControl component="fieldset" style={styles.formControl}>
                <FormLabel component="legend" style={styles.title}>{title}</FormLabel>
                <RadioGroup style={styles.flexDirectionRowWrap} aria-label="gender" name="gender1" value={value}
                            onChange={handleChange}>
                    <FormControlLabel value="female" control={<Checkbox
                        icon={<CircleUnchecked/>}
                        checkedIcon={<CircleChecked style={styles.radioButton}/>}
                    />} label={localize('homeScreen.https')}/>

                    <FormControlLabel value="female" control={<Checkbox
                        icon={<CircleUnchecked/>}
                        checkedIcon={<CircleChecked style={styles.radioButton}/>}
                    />} label={localize('homeScreen.sock4')}/>

                    <FormControlLabel value="female" control={<Checkbox
                        icon={<CircleUnchecked/>}
                        checkedIcon={<CircleChecked style={styles.radioButton}/>}
                    />} label={localize('homeScreen.sock5')}/>

                    <FormControlLabel value="female" control={<Checkbox
                        icon={<CircleUnchecked/>}
                        checkedIcon={<CircleChecked style={styles.radioButton}/>}
                    />} label={localize('homeScreen.http')}/>
                </RadioGroup>
            </FormControl>
        </div>
    </div>
}

export function TypesOf(props) {
    const {title} = props
    const [value, setValue] = React.useState('female');
    const handleChange = event => {
        setValue(event.target.value);
    };
    return <div style={styles.containerWrapper}>
        <div style={styles.radioButtons}>
            <FormControl component="fieldset" style={styles.formControl}>
                <FormLabel component="legend" style={styles.title}>{title}</FormLabel>
                <RadioGroup style={styles.flexDirectionRowWrap} aria-label="gender" name="gender1" value={value}
                            onChange={handleChange}>
                    <FormControlLabel value="female" control={<Checkbox
                        icon={<CircleUnchecked/>}
                        checkedIcon={<CircleChecked style={styles.radioButton}/>}
                    />} label={localize('homeScreen.homeIP')}/>

                    <FormControlLabel value="female" control={<Checkbox
                        icon={<CircleUnchecked/>}
                        checkedIcon={<CircleChecked style={styles.radioButton}/>}
                    />} label={localize('homeScreen.mobileIP')}/>
                    <FormControlLabel value="female" control={<Checkbox
                        icon={<CircleUnchecked/>}
                        checkedIcon={<CircleChecked style={styles.radioButton}/>}
                    />} label={localize('homeScreen.machineRoomIP')}/>
                </RadioGroup>
            </FormControl>
        </div>
    </div>
}

export function Area(props) {
    const {title} = props
    const [value, setValue] = React.useState('female');
    const handleChange = event => {
        setValue(event.target.value);
    };
    return <div style={styles.containerWrapper}>
        <div style={styles.radioButtons}>
            <FormControl component="fieldset" style={styles.formControl}>
                <FormLabel component="legend" style={styles.title}>{title}</FormLabel>
                <RadioGroup style={styles.flexDirectionRowWrap} aria-label="gender" name="gender1" value={value}
                            onChange={handleChange}>
                    <FormControlLabel value="female" control={<Checkbox
                        icon={<CircleUnchecked/>}
                        checkedIcon={<CircleChecked style={styles.radioButton}/>}
                    />} label={localize('homeScreen.china')}/>

                    <FormControlLabel value="female" control={<Checkbox
                        icon={<CircleUnchecked/>}
                        checkedIcon={<CircleChecked style={styles.radioButton}/>}
                    />} label={localize('homeScreen.america')}/>
                </RadioGroup>
            </FormControl>
        </div>
    </div>
}

export function ShowPrice(props) {
    const {currency, value, label} = props
    return <div style={styles.flexDirectionColumn}>
        <label style={styles.showPriceLabel}>{label}</label>
        <div style={Object.assign({}, styles.flexDirectionRow, styles.showPriceValueCurrency)}>
            <label>{currency}</label> <label>{value}</label>
        </div>
    </div>
}

export function WayOfReturn(props) {
    const {title} = props
    const [value, setValue] = React.useState('female');
    const handleChange = event => {
        setValue(event.target.value);
    };
    return <div style={styles.containerWrapper}>
        <div style={styles.radioButtons}>
            <FormControl component="fieldset" style={styles.formControl}>
                <FormLabel component="legend" style={styles.title}>{title}</FormLabel>
                <RadioGroup style={styles.flexDirectionRowWrap} aria-label="gender" name="gender1" value={value}
                            onChange={handleChange}>
                    <FormControlLabel value="female" control={<Checkbox
                        icon={<CircleUnchecked/>}
                        checkedIcon={<CircleChecked style={styles.radioButton}/>}
                    />} label={localize('homeScreen.realTimeReturn')}/>

                    <FormControlLabel value="female" control={<Checkbox
                        icon={<CircleUnchecked/>}
                        checkedIcon={<CircleChecked style={styles.radioButton}/>}
                    />} label={localize('homeScreen.delayReturn')}/>
                </RadioGroup>
            </FormControl>
        </div>
    </div>
}
