/** @flow */
import React, {Component} from 'react'
import connect from 'react-redux/es/connect/connect'
import withStyles from '@material-ui/core/es/styles/withStyles'
import HomeScreenLayout from "./HomeScreenLayout";
import styles from './styles'

class HomeScreen extends Component {
    render() {
        return (
            <div style={Object.assign({}, styles.screenContainer)}>
                <HomeScreenLayout/>
            </div>
        )
    }
}

const mapStateToProps = state => {
    return {
        auth: state.auth,
        screen: state.screen
    }
}

export default connect(mapStateToProps)(withStyles(styles)(HomeScreen))
