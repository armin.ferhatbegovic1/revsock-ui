import React from 'react';
import {makeStyles} from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import MyProxySection from './RightSections/MyProxySection'
import MyAPISection2 from './RightSections/MyAPISection'

const useStyles = makeStyles(theme => ({
    root: {
        flexGrow: 1,
    },
    paper: {
        padding: theme.spacing(2),
        textAlign: 'center',
        color: theme.palette.text.secondary,
    },
}));

export default function HomeScreenLayout() {
    const classes = useStyles();

    return (
        <div className={classes.root}>
            <Grid container spacing={3}>

                <Grid item xs={12} sm={6}>
                    <MyProxySection/>
                </Grid>
                <Grid item xs={12} sm={6}>
                    <MyAPISection2/>
                </Grid>
            </Grid>
        </div>
    );
}