/** @flow */
import React, {Component} from 'react'
import withStyles from '@material-ui/core/es/styles/withStyles'
import styles from './styles'
import {localize, prepareDataFormForSubmitAction} from "../../services";
import {formMode, layoutType, positionButton} from "../../config/Constants";
import InputForm from "../../components/UI/InputForm/InputForm";
import {passwordLoginScreen} from "../../forms";
import RedirectButton from '../../components/UI/RedirectButton/RedirectButton'
import {loginUser} from '../../store/actions/authentication'
import {connect} from "react-redux";
import {withRouter} from "react-router-dom";

class PasswordLoginDialog extends Component {
    constructor(props) {
        super(props)
        this.state = {
            passwordLoginScreenFields: JSON.parse(JSON.stringify(passwordLoginScreen))
        }
    }

    submitFormAction = () => {
        const {passwordLoginScreenFields} = this.state
        const { dispatch, history} = this.props
        const data = prepareDataFormForSubmitAction(passwordLoginScreenFields)
        dispatch(loginUser(data, history))
    }

    render() {
        const {passwordLoginScreenFields} = this.state
        return (
            <div style={styles.loginDialogContainer}>
                <div style={styles.loginDialogContainerLeft}></div>
                <div style={styles.loginDialogContainerRight}>
                    <div style={styles.dialogHeader}>
                        <label>{localize('siteName')}</label>
                    </div>
                    <InputForm formElements={passwordLoginScreenFields}
                               prepareDataAndSubmit={this.submitFormAction} formContainer={styles.formContainer}
                               buttonLabel={localize('verificationLoginScreen.logIn')}
                               mode={formMode.NEW} data={null} layout={layoutType.COLUMNS_1}
                               lastElementInLayout={layoutType.COLUMNS_1}
                               saveButtonPosition={positionButton.CENTER}>
                    </InputForm>
                    <div style={styles.dialogFooter}>
                        <RedirectButton label={localize('verificationLoginScreen.verificationCodeLogin')}
                                        route='/login-verification'/>
                        <RedirectButton label={localize('verificationLoginScreen.forgetPassword')}
                                        route='/reset-password'/>
                        <RedirectButton label={localize('verificationLoginScreen.notRegistered')} route='/register'/>
                    </div>

                </div>

            </div>
        )
    }
}


const mapStateToProps = state => ({
    auth: state.auth,
    screen: state.screen
})

export default connect(mapStateToProps)(withRouter(withStyles(styles)(PasswordLoginDialog)))
