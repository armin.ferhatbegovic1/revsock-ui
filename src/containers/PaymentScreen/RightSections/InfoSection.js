import React from 'react';
import {makeStyles} from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import RootContainerStyles from "../../Styles/RootContainerStyles";

const useStyles = makeStyles(theme => ({
    ...RootContainerStyles,
    root: {
        flexGrow: 1,
    },
    paper: {
        padding: '16px 0',
        textAlign: 'center',
        color: theme.palette.text.secondary,
    },
    paperInnerContainerHeight: {
        height: 300
    }
}));

export default function InfoSection() {
    const classes = useStyles();

    return (
        <Grid container spacing={3}>
            <Grid item xs={12} sm={12}>
                <Paper className={classes.paper}>


                </Paper>
            </Grid>
        </Grid>

    );
}