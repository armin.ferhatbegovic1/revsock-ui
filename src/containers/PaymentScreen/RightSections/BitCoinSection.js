import React from 'react';
import {makeStyles} from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import InnerSectionTitle from "../../../components/UI/InnerSectionTitle/InnerSectionTitle";
import {localize} from "../../../services";
import RootContainerStyles from "../../Styles/RootContainerStyles";
import ShowPrice from '../../../components/UI/ShowPrice/ShowPrice'

const useStyles = makeStyles(theme => ({
    ...RootContainerStyles,
    root: {
        flexGrow: 1,
    },
    paper: {
        padding: '20px 30px',
        textAlign: 'center',
        color: theme.palette.text.secondary,
    },
    paperInnerContainerHeight: {
        minHeight: 250,
    },
    titleContainer: {
        display: 'flex',
        flexDirection: 'row',

    }
}));

export default function BitCoinSection() {
    const classes = useStyles();

    return (
        <Grid container spacing={3}>
            <Grid item xs={12} sm={12}>
                <Paper className={classes.paper}>
                    <div className={classes.titleContainer}>
                        <InnerSectionTitle bottomLine={false}>{localize('paymentScreen.buyProxy')}</InnerSectionTitle>
                        <ShowPrice row={true} label={localize('homeScreen.price')} currency='BTC' value='0.00032'/>
                    </div>
                    <div className={classes.paperInnerContainer + ' ' + classes.paperInnerContainerHeight}>
                        <div className={classes.flexDirectionRowWrap}>

                        </div>
                        <div className={classes.paperFooter}>


                        </div>
                    </div>
                </Paper>
            </Grid>
        </Grid>

    );
}