import React from 'react';
import {makeStyles} from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import BitCoinSection from './RightSections/BitCoinSection'
import InfoSection from './RightSections/InfoSection'

const useStyles = makeStyles(theme => ({
    root: {
        flexGrow: 1,
    },
    paper: {
        padding: theme.spacing(2),
        textAlign: 'center',
        color: theme.palette.text.secondary,
    },
}));

export default function PaymentScreenLayout() {
    const classes = useStyles();

    return (
        <div className={classes.root}>
            <Grid container spacing={3}>
                <Grid item xs={12} sm={12}>
                    <BitCoinSection/>
                </Grid>
                <Grid item xs={12} sm={12}>
                    <InfoSection/>
                </Grid>
            </Grid>
        </div>
    );
}