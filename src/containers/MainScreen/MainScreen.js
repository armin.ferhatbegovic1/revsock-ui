/** @flow */
import React, {Component} from 'react'
import connect from 'react-redux/es/connect/connect'
import withStyles from '@material-ui/core/es/styles/withStyles'
import MainScreenLayout from "./MainScreenLayout";
import styles from './styles'

class MainScreen extends Component {
    render() {
        return (
            <div style={Object.assign({}, styles.screenContainer)}>
                <MainScreenLayout/>
            </div>
        )
    }
}

const mapStateToProps = state => {
    return {
        auth: state.auth,
        screen: state.screen
    }
}

export default connect(mapStateToProps)(withStyles(styles)(MainScreen))
