import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import SectionMainTitle from "../../../components/UI/SectionMainTitle/SectionMainTitle";
import {localize} from "../../../services";
import SectionSubTitle from "../../../components/UI/SectionSubTitle/SectionSubTitle";
import {DemoParagraph2} from "./helpers";

const useStyles = makeStyles(theme => ({
    root: {
        flexGrow: 1,
    },
    paper: {
        padding: theme.spacing(2),
        textAlign: 'center',
        color: theme.palette.text.secondary,
        display: 'flex',
        flexDirection: 'row',
        flexWrap: 'wrap'
    },
}));

export default function MyAPISection() {
    const classes = useStyles();

    return (
        <Grid container spacing={3}>
            <SectionMainTitle>{localize('homeScreen.myAPI')}</SectionMainTitle>
            <SectionSubTitle>{localize('homeScreen.isUsing')}</SectionSubTitle>
            <Grid item xs={12} sm={12}>
                <Paper className={classes.paper}><DemoParagraph2/></Paper>
            </Grid>
            <SectionSubTitle>{localize('homeScreen.expired')}</SectionSubTitle>
            <Grid item xs={12} sm={12}>
                <Paper className={classes.paper}><DemoParagraph2/></Paper>
            </Grid>
        </Grid>

    );
}