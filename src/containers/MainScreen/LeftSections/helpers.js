import React, {Fragment} from 'react'
import Colors from '../../../utils/Colors'
import Dimensions from '../../../utils/Dimensions'
import MainButton from "../../../components/UI/MainButton/MainButton";
import {buttonTypeHandler, subMainButtonType} from "../../../config/Constants";
import {localize} from "../../../services";

const headerData = ['HTTPS', 'IP', 'IP', 'IP2', 'IP/30min']
const headerData2 = ['HTTPS', 'IP']
const styles = {
    redirectMainButton: {},
    chipsContainer: {
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'flex-start',
    },
    chip: {
        color: Colors.primary,
        backgroundColor: Colors.primaryLight,
        paddingLeft: Dimensions.baseMargin,
        paddingRight: Dimensions.baseMargin,
        paddingTop: 5,
        paddingBottom: 5,
        borderRadius: 5,
        marginRight: 10
    },
    paperSection70: {
        flex: '70%'
    },
    paperSection30: {
        flex: '30%',
        display: 'flex',
        flexDirection: 'column',
        padding: 20,
        justifyContent: 'center'
    }
}

export const Chips = (props) => {
    const {data} = props
    return <div style={styles.chipsContainer}>{data.map((item, index) => {
        return <div style={styles.chip} key={index}><label key={index} className='redirectButton' onClick={() => {
        }}>{item}</label>
        </div>
    })}</div>
}
export const DemoParagraph = () => {
    return <Fragment>
        <div style={styles.paperSection70}>
            <Chips data={headerData}/>
            <label> abc abc abc abd
                abc abc abc abd</label>
            <label> abc abc abc abd
                abc abc abc abd</label>
            <label> abc abc abc abd
                abc abc abc abd</label>
        </div>
        <div style={styles.paperSection30}>
            <MainButton label={localize('homeScreen.seeDetails')}
                        buttonType={subMainButtonType.PRIMARY}
                        type={buttonTypeHandler.SUBMIT}/>
        </div>
    </Fragment>
}


export const DemoParagraph2 = () => {
    return <Fragment>
        <div style={styles.paperSection70}>
            <Chips data={headerData2}/>
            <label> abc abc abc abd
                abc abc abc abd</label>
            <label> abc abc abc abd
                abc abc abc abd</label>
            <label> abc abc abc abd
                abc abc abc abd</label>
        </div>
        <div style={styles.paperSection30}>
            <MainButton label={localize('homeScreen.seeDetails')}
                        buttonType={subMainButtonType.SECONDARY}
                        type={buttonTypeHandler.SUBMIT}/>
        </div>
    </Fragment>
}


