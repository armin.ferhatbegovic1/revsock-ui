import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import SectionSubTitle from "../../../components/UI/SectionSubTitle/SectionSubTitle";
import {localize} from "../../../services";
import {DemoParagraph} from "./helpers";

const useStyles = makeStyles(theme => ({
    root: {
        flexGrow: 1,
    },
    paper: {
        padding: theme.spacing(2),
        textAlign: 'center',
        color: theme.palette.text.secondary,
        display: 'flex',
        flexDirection: 'row',
        flexWrap: 'wrap'
    },
}));

export default function ExpiredSection() {
    const classes = useStyles();

    return (
        <Grid container spacing={3}>
            <SectionSubTitle>{localize('homeScreen.expired')}</SectionSubTitle>
            <Grid item xs={12} sm={12}>
                <Paper className={classes.paper}><DemoParagraph/></Paper>
            </Grid>

            <Grid item xs={12} sm={12}>
                <Paper className={classes.paper}><DemoParagraph/></Paper>
            </Grid>
        </Grid>

    );
}