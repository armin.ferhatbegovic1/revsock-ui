import React from 'react';
import {makeStyles} from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import InnerSectionTitle from "../../../components/UI/InnerSectionTitle/InnerSectionTitle";
import {localize} from "../../../services";
import {Area, ServiceAgreement, TypesOf} from "./helpers";
import ShowPrice from '../../../components/UI/ShowPrice/ShowPrice'
import IncrementerDisplay from '../../../components/UI/IncrementerDisplay/IncrementerDisplay'
import RootContainerStyles from "../../Styles/RootContainerStyles";
import CircleInnerText from "../../../components/UI/CircleInnerText/CircleInnerText";
import MainButton from "../../../components/UI/MainButton/MainButton";
import {subMainButtonType} from "../../../config/Constants";

const useStyles = makeStyles(theme => ({
    ...RootContainerStyles,
    root: {
        flexGrow: 1,
    },
    paper: {

        padding: '16px 0',
        textAlign: 'center',
        color: theme.palette.text.secondary,
    },
    paperInnerContainerHeight: {
        minHeight: 450,
    }

}));

export default function MyProxySection() {
    const classes = useStyles();

    return (
        <Grid container spacing={3}>
            <Grid item xs={12} sm={12}>
                <Paper className={classes.paper}>
                    <InnerSectionTitle>{localize('homeScreen.myProxy')}</InnerSectionTitle>
                    <div className={classes.paperInnerContainer + ' ' + classes.paperInnerContainerHeight}>
                        <ServiceAgreement title={localize('homeScreen.serviceAgreement')}/>
                        <TypesOf title={localize('homeScreen.typesOf')}/>
                        <Area title={localize('homeScreen.area')}/>
                        <div className={classes.flexDirectionRowWrap}>
                            <IncrementerDisplay label={localize('homeScreen.purchaseQuantity')}/>
                            <IncrementerDisplay label={localize('homeScreen.usageTime')}/>
                            <div className={classes.circlesContainer}>
                                <CircleInnerText label={localize('homeScreen.day')}/>
                                <CircleInnerText label={localize('homeScreen.week')}/>
                                <CircleInnerText label={localize('homeScreen.year')}/>
                            </div>
                        </div>
                        <div className={classes.paperFooter}>
                            <ShowPrice label={localize('homeScreen.price')} currency='BTC' value='0.00032'/>

                            <MainButton width={200} label={localize('homeScreen.buyNow')}
                                        buttonType={subMainButtonType.PRIMARY}/>

                        </div>
                    </div>
                </Paper>
            </Grid>
        </Grid>

    );
}