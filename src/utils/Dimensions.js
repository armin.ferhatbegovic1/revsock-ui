import { deviceType } from '../config/Constants'

const Dimensions = {
  phone: 768,
  tablet: 992,
  desktop: 1200,
  largeDesktop: 1400,
  textLarge: 26,
  textXLarge: 28,
  textXXLarge: 32,
  textX3Large: 36,
  textX4Large: 48,
  textMedium: 20,
  textSmall: 16,
  textXSmall: 14,
  textXXSmall: 12,
  textXXXSmall: 10,
  inputFields: {
    inputSmall: 75,
    inputLarge: 100,
    inputXLarge: 125,
    inputXXLarge: 250,
    input3XLarge: 300
  },
  baseMargin: 10,
  doubleBaseMargin: 20,
  tripeBaseMargin: 30,
  smallMargin: 5,
  doubleSection: 50,
  basePadding: 10,
  doubleBasePadding: 20,
  tripleBasePadding: 30,
  halfWidth: '50%',
  fullWidth: '100%',
  fullHeight: '100vh',
  logoWidth: 150,
  logoHeight: 150,
  logoMiniWidth: 70,
  logoMiniHeight: 70,
  heightOfDealCard:700,
  heightOfBidCard:800,
  userImageMiniWidth:40,
  userImageMiniHeight:40,
  userImageWidth:150,
  userImageHeight:150,
  userImageTabletDimensions:120,
  userImagePhoneDimensions:100,
  miniButtonIcon:45,
  userAvatar:30
}

Dimensions.getWidth = () => {
  return Math.max(
    document.body.scrollWidth,
    document.documentElement.scrollWidth,
    document.body.offsetWidth,
    document.documentElement.offsetWidth,
    document.documentElement.clientWidth
  )
}

Dimensions.getHeight = () => {
  return Math.max(
    document.body.scrollHeight,
    document.documentElement.scrollHeight,
    document.body.offsetHeight,
    document.documentElement.offsetHeight,
    document.documentElement.clientHeight
  )
}

Dimensions.getDevice = () => {
  const deviceWidth = Dimensions.getWidth()
  if (deviceWidth > Dimensions.desktop) {
    return deviceType.LARGE_DESKTOP
  }

  if (Dimensions.tablet <deviceWidth && deviceWidth <= Dimensions.desktop) {
    return deviceType.DESKTOP
  }
  if (Dimensions.phone < deviceWidth && deviceWidth <= Dimensions.tablet) {
    return deviceType.TABLET
  }
  if (deviceWidth <= Dimensions.phone) {
    return deviceType.PHONE
  }
}

export default Dimensions