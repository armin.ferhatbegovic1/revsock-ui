const metrics = {
  tinyMargin: 2,
  smallMargin: 4,
  baseMargin: 8,
  doubleBaseMargin: 16,
  tripeBaseMargin: 24,
  tinyPadding: 2,
  smallPadding: 4,
  basePadding: 8,
  doubleBasePadding: 16,
  tripleBasePadding: 24,
  horizontalLineHeight: 1,
  baseRadius: 4,
  doubleRadius: 8,
  tripleRadius: 16,
  baseBorder: 2,
  doubleBorder: 4,
  tripleBorder: 6,
  icons: {
    tiny: 8,
    small: 16,
    medium: 24,
    large: 32,
    xl: 48
  },
  images: {
    small: 50,
    medium: 100,
    large: 200,
    xl: 300
  }
}

export default metrics
