import apiEndpoint from '../../config/config'
import store from '../../store/index'
import { setToastNotification } from '../../store/actions/toastAction'
import { handleErrorResponse, handleResponseCode, isEmpty, localize } from '../'
import { loaderVisbility, request, requestHeader, toastNotification, toastType } from '../../config/Constants'
import { setLoaderVisibility } from '../../store/actions/loaderAction'
import AxiosInstance from '../Axios'

const RESPONSE_TIMEOUT = 5000

function timeout (ms, promise) {
  return new Promise(function (resolve, reject) {
    setTimeout(function () {
      reject(new Error('timeout'))
    }, ms)
    promise.then(resolve, reject)
  })
}

export async function getRequest (apiLocation, toastShow = false, loaderShow = true) {

  if (loaderShow) {
    store.dispatch(setLoaderVisibility(loaderVisbility.SHOW))
  }
  return timeout(RESPONSE_TIMEOUT, fetch(apiEndpoint.apiEndpoint + apiLocation, {
    method: request.GET,
    headers: requestHeader()
  }))
  .then(function (response) {
    return response.clone().json()
  })
  .then(function (data) {
    if (toastShow === true) {
      store.dispatch(setToastNotification(toastShow, handleResponseCode(data), toastType.SUCCESS))
    }
    return data
  })
  .catch(function (error) {
    store.dispatch(setToastNotification(true, handleErrorResponse(error), toastType.FAILURE))
    return error
  }).finally(function () {
    if (loaderShow) {
      store.dispatch(setLoaderVisibility(loaderVisbility.HIDE))
    }
  })
}



export function postRequest (apiLocation, data, toastShow = true, redirectionCallBack = null, toastMessage = localize('successfullyDataPost')) {
  store.dispatch(setLoaderVisibility(loaderVisbility.SHOW))
  return fetch( apiLocation, {
    method: request.POST,
    headers: requestHeader(),
    body: JSON.stringify(data)
  })
  .then(function (response) {

    return response.clone().json()
  })
  .then(function (dataResponse) {
    console.log("dataResponse ",dataResponse)
    if (!isEmpty(dataResponse['error'])) {
      store.dispatch(setToastNotification(toastShow, handleErrorResponse(dataResponse), 'error'))
    } else if (toastShow === true) {
      store.dispatch(setToastNotification(toastShow, handleResponseCode(dataResponse, toastMessage), toastType.SUCCESS))
    }
    if (redirectionCallBack != null) {
      redirectionCallBack(dataResponse)
    }
    return dataResponse
  })
  .catch(function (error) {
 console.log("error ",error)
    return error
  }).finally(function () {
    store.dispatch(setLoaderVisibility(loaderVisbility.HIDE))
  })

}

export function deleteRequest (apiLocation, toastShow = true, toastMessage = localize('successfullyDataDelete')) {
  return fetch(apiEndpoint.apiEndpoint + apiLocation, {
    method: request.DELETE,
    headers: requestHeader()
  })
  .then(function (response) {
    return response.clone().json()
  })
  .then(function (dataResponse) {
    if (toastShow === true) {
      store.dispatch(setToastNotification(toastShow, handleResponseCode(dataResponse, toastMessage), toastType.SUCCESS))
    }
    return dataResponse
  })
  .catch(function (error) {
    store.dispatch(setToastNotification(true, handleErrorResponse(error), toastType.FAILURE))
  })

}

export function putRequest (apiLocation, data, toastShow = true, toastMessage = localize('successfullyDataPut')) {
  store.dispatch(setLoaderVisibility(loaderVisbility.SHOW))
  return fetch(apiEndpoint.apiEndpoint + apiLocation, {
    method: request.PUT,
    headers: requestHeader(),
    body: JSON.stringify(data)
  })
  .then(function (response) {
    return response.clone().json()
  })
  .then(function (dataResponse) {
    if (toastShow === true) {
      store.dispatch(setToastNotification(true, typeof dataResponse.message === 'undefined' ? toastMessage : dataResponse.message, 'success'))
    }
    return dataResponse
  })
  .catch(function (error) {
    store.dispatch(setToastNotification(true, handleErrorResponse(error), toastType.FAILURE))
  }).finally(function () {
    store.dispatch(setLoaderVisibility(loaderVisbility.HIDE))
  })
}

export function axiosPostRequest (apiLocation, data, toastShow = true, successRedirectionCallBack = null, failureRedirectionCallBack = null) {
  store.dispatch(setLoaderVisibility(loaderVisbility.SHOW))
  AxiosInstance.post(apiLocation, data)
  .then(res => {
      if (toastShow) {
        store.dispatch(setToastNotification(toastNotification.SHOW, handleResponseCode(res), toastType.SUCCESS))
      }
      if (!isEmpty(successRedirectionCallBack)) {
        successRedirectionCallBack(res)
      }
    }
  )
  .catch(error => {
    store.dispatch(setToastNotification(true, handleErrorResponse(error), toastType.FAILURE))
    if (!isEmpty(failureRedirectionCallBack)) {
      failureRedirectionCallBack()
    }
  })
  .finally(() => {
    store.dispatch(setLoaderVisibility(loaderVisbility.HIDE))
  })
}
