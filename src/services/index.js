import AxiosInstance from './Axios'
import {changeLanguage, getCurrentLanguage, localize} from './Localization/LocalizationManager'
import {deleteRequest, getRequest, postRequest, putRequest} from './RequestManager/RequestManager'
import {
    awaitFunction,
    calculateCountOfItemsInRow,
    changeDateFormat,
    changeListDateFormat,
    checkIsBoolean,
    getCurrentHostName,
    getDataFromURL,
    handleErrorResponse,
    handleResponseCode,
    isEmpty,
    loadSortConfigurationInLocalStorage,
    prepareDataFormForSubmitAction,
    saveSortConfigurationInLocalStorage
} from './GlobalServices'

export {
    AxiosInstance,
    isEmpty,
    changeLanguage,
    localize,
    getCurrentLanguage,
    postRequest,
    getRequest,
    awaitFunction,
    putRequest,
    changeDateFormat,
    calculateCountOfItemsInRow,
    deleteRequest,
    getDataFromURL,
    checkIsBoolean,
    prepareDataFormForSubmitAction,
    handleResponseCode,
    handleErrorResponse,
    getCurrentHostName,
    changeListDateFormat,
    saveSortConfigurationInLocalStorage,
    loadSortConfigurationInLocalStorage
}