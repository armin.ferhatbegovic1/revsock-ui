import {localize} from '../services'


export const resetPasswordForm = {
    userName: {
        label: localize('resetPasswordScreen.enterUserName'),
        value: '',
        errorMessages: localize('thisFieldIsRequired'),
        validation: {
            required: true,
        },
        valid: true,
        icon: 'User'
    },
    enterPhoneNumber: {
        label: localize('resetPasswordScreen.enterPhoneNumber'),
        value: '',
        errorMessages: localize('thisFieldIsRequired'),
        validation: {
            required: true,
        },
        valid: true,
        icon:'PhoneAndroid'
    },
    enterVerificationCode: {
        label: localize('resetPasswordScreen.enterVerificationCode'),
        value: '',
        errorMessages: localize('thisFieldIsRequired'),
        validation: {
            required: true,
        },
        valid: true,
        icon:'VerifiedUserOutlinedIcon'
    },
    password: {
        label: localize('resetPasswordScreen.enterPassword'),
        value: '',
        errorMessages: localize('thisFieldIsRequired'),
        validation: {
            required: true,
            min:6
        },
        valid: true,
        icon: 'Lock'
    },
    passwordAgain: {
        label: localize('resetPasswordScreen.passwordAgain'),
        value: '',
        errorMessages: localize('thisFieldIsRequired'),
        validation: {
            required: true,
            min:6
        },
        valid: true,
        icon: 'Lock'
    },

}

