import {verificationLoginScreen,passwordLoginScreen} from "./loginForm";
import {registerForm} from './registerForm'
import {resetPasswordForm} from './resetPasswordForm'

export {verificationLoginScreen, registerForm,resetPasswordForm,passwordLoginScreen}