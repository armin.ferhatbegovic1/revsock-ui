import { localize, postRequest } from '../services'
import {
  alertDialogType,
  autoCloseDialog,
  formDialog,
  inputTypeFormElement,
  toastNotification
} from '../config/Constants'
import store from '../store/index'
import { setFormDialog } from '../store/actions/formDialogAction'
import { itemEndpoints } from '../config/Endpoints'

export const requestOfferForm =()=>({
  title: {
    label: localize('newItemScreen.title'),
    value: '',
    errorMessages: localize('thisFieldIsRequired'),
    validation: {
      required: true
    },
    valid: true
  },
  type: {
    label: localize('newItemScreen.itemType'),
    placeholder: localize('newDealScreen.selectCategories'),
    value: [],
    errorMessages: localize('thisFieldIsRequired'),
    validation: {
      required: true
    },
    inputType: inputTypeFormElement.SELECT_LIST,
    itemsList: ['Offer', 'Request'],
    valid: true
  },
  description: {
    label: localize('newItemScreen.description'),
    value: '',
    errorMessages: localize('thisFieldIsRequired'),
    validation: {
      required: true
    },
    valid: true
  },
  price: {
    label: localize('newItemScreen.price'),
    value: '',
    errorMessages: localize('thisFieldIsRequired'),
    validation: {
      required: true,
      isNumeric: true
    },
    valid: true,
    id:'price-field'
  },
  category: {
    label: localize('newDealScreen.dealCategory'),
    placeholder: localize('newDealScreen.selectCategories'),
    value: [],
    errorMessages: localize('thisFieldIsRequired'),
    validation: {
      multiple: true,
      required: true,
      minLength: 1,
      maxLength: 3
    },
    inputType: inputTypeFormElement.SELECT_LIST_CATEGORIES,
    valid: true
  },
  newCategory: {
    label: localize('cardDeal.addNewCategory'),
    value: null,
    errorMessages: localize('thisFieldIsRequired'),
    inputType: inputTypeFormElement.MAIN_BUTTON,
    valid: true,
    actionCallBack: () => openDialogForAddCategory(),
    validation: {
      required: false
    },

  },

})
export const previewRequestOfferForm = {
  title: {
    label: localize('newItemScreen.title'),
    value: '',
    errorMessages: localize('thisFieldIsRequired'),
    validation: {
      required: true
    },
    valid: true
  },
  type: {
    label: localize('newItemScreen.itemType'),
    placeholder: localize('newDealScreen.selectCategories'),
    value: [],
    errorMessages: localize('thisFieldIsRequired'),
    validation: {
      required: true
    },
    inputType: inputTypeFormElement.SELECT_LIST,
    itemsList: ['Offer', 'Request'],
    valid: true
  },
  price: {
    label: localize('newItemScreen.price'),
    value: '',
    errorMessages: localize('thisFieldIsRequired'),
    validation: {
      required: true,
      isNumeric: true
    },
    valid: true
  },
  description: {
    label: localize('newItemScreen.description'),
    value: '',
    errorMessages: localize('thisFieldIsRequired'),
    validation: {
      required: true
    },
    multiline: true,
    valid: true
  },
  createdBy: {
    label: localize('previewItemScreen.createdBy'),
    value: '',
    errorMessages: localize('thisFieldIsRequired'),
    validation: {
      required: true
    },
    valid: true
  },
  numberOfBid: {
    label: localize('newItemScreen.bidCount'),
    value: '',
    errorMessages: localize('thisFieldIsRequired'),
    validation: {
      required: false
    },
    valid: true
  }
}
export const submitDealForm = {
  description: {
    label: localize('cardDeal.additionInformations'),
    value: '',
    errorMessages: localize('thisFieldIsRequired'),
    validation: {
      required: false
    },
    inputType: inputTypeFormElement.TEXT_AREA,
    valid: true
  },
  value: {
    label: localize('cardBid.priceOffer'),
    value: '',
    errorMessages: localize('thisFieldIsRequired'),
    validation: {
      required: false
    },
    inputType: inputTypeFormElement.TEXT_FIELD,
    valid: true,
    isNumeric: true
  }
}

export const addNewCategoryForm = {
  name: {
    label: localize('newItemScreen.newCategoryName'),
    value: '',
    errorMessages: localize('thisFieldIsRequired'),
    validation: {
      required: true
    },
    valid: true
  }
}

function openDialogForAddCategory () {
  store.dispatch(setFormDialog(formDialog.SHOW, null, localize('cardDeal.newCategory'), alertDialogType.CONFIRM, addNewCategoryForm, (data) => {return newCategoryPost(data)}, null, autoCloseDialog.AUTO_CLOSE_DIALOG_FALSE))
}

async function newCategoryPost (data) {
   return await postRequest(itemEndpoints.NEW_CATEGORY, data, toastNotification.SHOW, null, localize('cardDeal.newCategorySuccessfullySaved'))
}


