import {localize} from '../services'
import {inputTypeFormElement} from "../config/Constants";

export const registerForm = {
    userName: {
        label: localize('registerScreen.enterUserName'),
        value: '',
        errorMessages: localize('thisFieldIsRequired'),
        validation: {
            required: true,
        },
        valid: true,
        icon: 'User'
    },
    password: {
        label: localize('registerScreen.enterPassword'),
        value: '',
        errorMessages: localize('thisFieldIsRequired'),
        validation: {
            required: true,
            min: 6
        },
        valid: true,
        icon: 'Lock'
    },
    passwordAgain: {
        label: localize('registerScreen.enterPasswordAgain'),
        value: '',
        errorMessages: localize('thisFieldIsRequired'),
        validation: {
            required: true,
            min: 6
        },
        valid: true,
        icon: 'Lock'
    },
    phone: {
        label: localize('registerScreen.enterPhone'),
        value: '',
        errorMessages: localize('thisFieldIsRequired'),
        validation: {
            required: true,
            min: 6
        },
        valid: true,
        icon: 'PhoneAndroid'
    },
    verificationCode: {
        label: localize('registerScreen.enterVerificationCode'),
        value: '',
        errorMessages: localize('thisFieldIsRequired'),
        validation: {
            required: true,
        },
        valid: true,
        inputType: inputTypeFormElement.TEXT_FIELD_WITH_BUTTON,
        buttonLabel: localize('passwordLoginScreen.getVerificationCode'),
        icon: 'VerifiedUserOutlinedIcon'
    },

}

