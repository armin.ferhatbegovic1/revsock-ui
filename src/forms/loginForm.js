import {localize} from '../services'
import React from 'react'
import {inputTypeFormElement} from '../config/Constants'

export const passwordLoginScreen = {
    email: {
        label: localize('verificationLoginScreen.enterName'),
        value: 'admin@email.com',
        errorMessages: localize('thisFieldIsRequired'),
        validation: {
            required: true,
            email: true
        },
        valid: true,
        icon: 'User'
    },
    password: {
        label: localize('verificationLoginScreen.enterVerificationCode'),
        value: '56f5c8c0-97c6-44f9-b0e9-bcb0a793ccc9',
        errorMessages: localize('thisFieldIsRequired'),
        validation: {
            required: true
        },
        type: 'password',
        valid: true,
        icon: 'Lock'
    }
}
export const verificationLoginScreen = {
    enterPhone: {
        label: localize('passwordLoginScreen.enterPhone'),
        value: '',
        errorMessages: localize('thisFieldIsRequired'),
        validation: {
            required: true,
            email: true
        },
        valid: true,
        icon: 'User'
    },
    verificationCode: {
        label: localize('passwordLoginScreen.enterVerificationCode'),
        value: '',
        errorMessages: localize('thisFieldIsRequired'),
        validation: {
            required: true
        },
        type: 'password',
        valid: true,
        icon: 'Lock',
        inputType: inputTypeFormElement.TEXT_FIELD_WITH_BUTTON,
        buttonLabel: localize('passwordLoginScreen.getVerificationCode')
    }
}