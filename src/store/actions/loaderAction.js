import { LOADER_VISIBLE } from './types'


export const setLoaderVisibility = (visible)  => dispatch => {
  dispatch({
    type: LOADER_VISIBLE,
    payload: visible
  })
}
