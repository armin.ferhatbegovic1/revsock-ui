import {
    loaderVisbility,
    REMEMBER_USER,
    toastNotification,
    TOKEN_EXPIRATION_DATE,
    USER_TOKEN
} from '../../config/Constants'
import {setCurrentUser} from './userActions'
import Cookies from 'universal-cookie'
import {setLoaderVisibility} from "./loaderAction";
import {axiosPostRequest} from "../../services/RequestManager/RequestManager";
import {userEndpoints} from "../../config/Endpoints";

export const logoutUser = (history) => dispatch => {
    /* localStorage.setItem(USER_TOKEN, '')
     localStorage.removeItem(TOKEN_EXPIRATION_DATE)*/
    const cookie = new Cookies()
    cookie.remove(USER_TOKEN)
    cookie.remove(TOKEN_EXPIRATION_DATE)
    cookie.remove(REMEMBER_USER)
    dispatch(setCurrentUser({}))
    history.push('/')
}

export const loginUser = (data, history) => dispatch => {

    dispatch(setLoaderVisibility(loaderVisbility.SHOW))
    axiosPostRequest(userEndpoints.LOGIN, data, toastNotification.HIDE,
        (response) => dispatch(setUserAfterLogin(response.data, history)))
}

export const setUserAfterLogin = (data,history) => dispatch=>{
const cookie = new Cookies()
    dispatch(setCurrentUser(data))
    let tokenValue=data['authToken'].replace(USER_TOKEN,'')
    cookie.set(USER_TOKEN,tokenValue.trim(),{path:'/',expires: new Date(Date.now()+2592000)})
    history.push('/')
}