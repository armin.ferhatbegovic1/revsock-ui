import * as types from './types'
import Dimensions from '../../utils/Dimensions'

export const updateWidth = () => {
  const {dispatch} = this.props
  dispatch({
    type: types.CHANGE_SCREEN_WIDTH,
    payload: Dimensions.getWidth()
  })
}

export const updateDimensions = () =>dispatch=> {
  dispatch({
    type: 'CHANGE_SCREEN_WIDTH',
    payload: {width: Dimensions.getWidth()}
  })
  dispatch({
    type: 'CHANGE_SCREEN_HEIGHT',
    payload: {height: Dimensions.getHeight()}
  })
}