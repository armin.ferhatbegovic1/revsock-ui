import { DIALOG_ACTION, DIALOG_MESSAGE, DIALOG_OPEN, DIALOG_TITLE, DIALOG_TYPE } from './types'

export const setAlertDialog = (open, message, title, type, action = null) => dispatch => {
  dispatch({
    type: DIALOG_OPEN,
    payload: open
  })
  dispatch({
    type: DIALOG_MESSAGE,
    payload: message
  })
  dispatch({
    type: DIALOG_TITLE,
    payload: title
  })
  dispatch({
    type: DIALOG_TYPE,
    payload: type
  })
  dispatch({
    type: DIALOG_ACTION,
    payload: action
  })
}

export const closeAlertDialog = () => dispatch => {
  dispatch({type: DIALOG_OPEN, payload: false})
  dispatch({type: DIALOG_MESSAGE, payload: ''})
}
