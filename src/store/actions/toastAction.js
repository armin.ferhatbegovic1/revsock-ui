import { TOAST_TYPE, TOAST_OPEN, TOAST_MESSAGE } from './types'


export const setToastNotification = (open, message, type)  => dispatch => {
  dispatch({
    type: TOAST_OPEN,
    payload: open
  })
  dispatch({
    type: TOAST_MESSAGE,
    payload: message
  })
  dispatch({
    type: TOAST_TYPE,
    payload: type
  })
}
