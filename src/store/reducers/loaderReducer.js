import { LOADER_VISIBLE } from '../actions/types'
import { loaderVisbility } from '../../config/Constants'

export default function reducer(
    state = {
        visible: loaderVisbility.HIDE
    },
    action
) {
    switch (action.type) {
        case LOADER_VISIBLE: {
            return { ...state, visible: action.payload };
        }
        default:
            return state;
    }
}
