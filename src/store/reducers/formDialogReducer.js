import {
  FORM_DIALOG_FIELDS,
  FORM_DIALOG_ACTION,
  FORM_DIALOG_TITLE,
  FORM_DIALOG_TYPE,
  FORM_DIALOG_MESSAGE,
  FORM_DIALOG_OPEN,
  FORM_DIALOG_ACTION_CALLBACK,
  FORM_DIALOG_AUTO_CLOSE
} from '../actions/types'

export default function reducer(
  state = {
    open: false,
    message: '',
    type: '',
    title:'',
    action:null,
    formFields:null,
    afterResponseCallback:null,
    autoClose:true
  },
  action
) {
  switch (action.type) {
    case FORM_DIALOG_OPEN: {
      return { ...state, open: action.payload };
    }
    case FORM_DIALOG_MESSAGE: {
      return { ...state, message: action.payload };
    }
    case FORM_DIALOG_TYPE: {
      return { ...state, type: action.payload };
    }
    case FORM_DIALOG_TITLE: {
      return { ...state, title: action.payload };
    }
    case FORM_DIALOG_ACTION: {
      return { ...state, action: action.payload };
    }
    case FORM_DIALOG_FIELDS: {
      return { ...state, formFields: action.payload };
    }
    case FORM_DIALOG_ACTION_CALLBACK: {
      return { ...state, afterResponseCallback: action.payload };
    }
    case FORM_DIALOG_AUTO_CLOSE: {
      return { ...state, autoClose: action.payload };
    }
    default:
      return state;
  }
}
