import settings from './settings'
import authReducer from './authReducer'
import {combineReducers} from 'redux'
import {reducer as formReducer} from 'redux-form'
import errorReducer from './errorReducer'
import toastReducer from './toastReducer'
import alertDialogReducer from './alertDialogReducer'
import screenReducer from './screenReducer'
import formDialogReducer from './formDialogReducer'
import loaderReducer from './loaderReducer'

export default combineReducers({
    settings,
    screen: screenReducer,
    auth: authReducer,
    form: formReducer,
    errors: errorReducer,
    toast: toastReducer,
    dialog: alertDialogReducer,
    formDialog: formDialogReducer,
    loader: loaderReducer,
})

