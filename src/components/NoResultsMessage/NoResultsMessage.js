import React, { Fragment } from 'react'
import { isEmpty, localize } from '../../services'
import RootComponentStyles from '../Styles/RootComponentStyles'
import { loaderVisbility } from '../../config/Constants'

const styles = ({
  ...RootComponentStyles
})

function NoResultsMessage (props) {
  const {show} = props
  if (!isEmpty(props.setLoaderVisibility)) {
    setTimeout(() => {
      props.setLoaderVisibility(loaderVisbility.HIDE)
    }, 100)
  }
  return <Fragment>{show ? <label style={styles.noResultsContainer}>{localize('noResults')}</label> : null}</Fragment>
}

export default NoResultsMessage
