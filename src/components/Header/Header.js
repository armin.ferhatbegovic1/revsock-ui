import React from 'react'
import AppBar from '@material-ui/core/AppBar'
import Toolbar from '@material-ui/core/Toolbar'
import IconButton from '@material-ui/core/IconButton'
import Badge from '@material-ui/core/Badge'
import MenuItem from '@material-ui/core/MenuItem'
import Menu from '@material-ui/core/Menu'
import {withStyles} from '@material-ui/core/styles'
import AccountCircle from '@material-ui/icons/AccountCircle'
import MailIcon from '@material-ui/icons/Mail'
import NotificationsIcon from '@material-ui/icons/NotificationsOutlined'
import {connect} from 'react-redux'
import {logoutUser} from '../../store/actions/authentication'
import {withRouter} from 'react-router-dom'
import {localize} from '../../services'
import Dimensions from '../../utils/Dimensions'
import Button from '@material-ui/core/Button'
import {deviceType} from '../../config/Constants'
import Colors from '../../utils/Colors'
import styles from './styles'
import Logo from "../UI/Logo/Logo";
import Avatar from "@material-ui/core/Avatar";
import Images from "../../utils/Images";

const mobileMenuItems = (that) => [{
    label: localize('header.messages'),
    icon: <MailIcon/>,
    action: that.handleMobileMenuClose
},
    {
        label: localize('header.notifications'),
        icon: <NotificationsIcon/>,
        action: that.handleMobileMenuClose
    },
    {
        label: localize('header.profile'),
        icon: <AccountCircle/>,
        action: that.handleProfileMenuOpen
    }
]
const menuHeadersItem = [
    {
        label: localize('header.home'),
        path: '/home',
        logged: false
    },
    {
        label: localize('header.chinese'),
        path: '#section2',
        logged: false
    },
    {
        label: localize('header.home'),
        path: '#section1',
        logged: true
    },
    {
        label: localize('header.transaction'),
        path: '#section1',
        logged: true
    },
    {
        icon: Images.userAvatar,
        path: '#section1',
        alt: 'user',
        logged: true
    },
    {
        label: localize('header.user'),
        path: '#section1',
        logged: true
    },
    {
        label: localize('header.logout'),
        path: '#section1',
        logged: true,
        logout: true
    },
    {
        label: localize('header.chinese'),
        path: '#section2',
        logged: true
    },
]

class Header extends React.Component {
    state = {
        anchorEl: null,
        mobileMoreAnchorEl: null,
        active: '#section1'
    }

    handleProfileMenuOpen = event => {
        this.setState({anchorEl: event.currentTarget})
    }

    handleMenuClose = () => {
        this.setState({anchorEl: null})
        this.handleMobileMenuClose()
    }

    handleMobileMenuOpen = event => {
        this.setState({mobileMoreAnchorEl: event.currentTarget})
    }

    handleMobileMenuClose = () => {
        this.setState({mobileMoreAnchorEl: null})
    }

    onLogout = (e) => {
        const {history, dispatch} = this.props
        e.preventDefault()
        dispatch(logoutUser(history))
    }

    goToMyProfile = () => {
        this.props.history.push('/profile')
    }

    renderHomePageToolbar() {
        return <Toolbar style={styles.homePageToolbarRoot}><Button
            color='inherit'>{localize('howItWorks')}</Button></Toolbar>
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.screen !== this.props.screen) {
            this.resize()
        }
    }

    componentWillMount() {
        this.unlisten = this.props.history.listen((location, action) => {
            console.log("on route change", location.hash);
            if (location.hash.includes('section'))
                this.setState({active: location.hash})
        });
    }

    componentWillUnmount() {
        this.unlisten();
    }

    resize = () => {
        const {open, handleToggleDrawer} = this.props
        if ((Dimensions.getDevice() === deviceType.PHONE && open) || (Dimensions.getDevice() !== deviceType.PHONE && !open)) {
            handleToggleDrawer()
            this.handleMenuClose()
            this.forceUpdate()
        }
    }

    renderLogoOrIconButton() {
        const {history} = this.props
        return <Logo mini={true} header={true} history={history}/>
    }

    renderMenuItem(item) {
        const {classes, history, auth} = this.props
        if (item.logged === auth.isAuthenticated) {
            if (item.icon) {
                return <Avatar key={item.alt} alt={item.alt} className={classes.avatar + ' headerAvatar'}
                               src={item.icon} onClick={() => history.push(item.path)}/>
            } else {
                return <label key={item.label + 1} onClick={(e) => {
                    item.logout ? this.onLogout(e) : history.push(item.path)
                }} className={classes.menuItem}> {item.label}</label>
            }
        } else return null
    }

    renderHeaderMenu() {
        const {classes, history, auth} = this.props
        return menuHeadersItem.map(item => {
            return this.renderMenuItem(item)
        })
    }


    renderBrandToolbar() {
        const {classes} = this.props
        return <Toolbar classes={{root: classes.toolbarRoot}} style={{color: Colors.white}}>
            <label className={classes.headerTitle}
                   style={Object.assign({}, {display: this.state.active !== '#section1' ? 'none' : 'flex'})}>
                网站名称 | 登录</label>
            <div className={classes.grow}/>
            <div className={classes.sectionDesktop}>
                {this.renderHeaderMenu()}
            </div>
        </Toolbar>

    }

    renderMobileMenu() {
        const {mobileMoreAnchorEl} = this.state
        const isMobileMenuOpen = Boolean(mobileMoreAnchorEl)
        return <Menu
            anchorEl={mobileMoreAnchorEl}
            anchorOrigin={{vertical: 'top', horizontal: 'right'}}
            transformOrigin={{vertical: 'top', horizontal: 'right'}}
            open={isMobileMenuOpen}
            onClose={this.handleMenuClose}
        >
            {mobileMenuItems(this).map((item, i) => {
                return <MenuItem onClick={item.action} key={i}>
                    <IconButton color='inherit'>
                        <Badge badgeContent={4} color='secondary'>
                            {item.icon}
                        </Badge>
                    </IconButton>
                    <p>{item.label}</p>
                </MenuItem>
            })
            }
        </Menu>
    }

    renderMenu() {
        const {anchorEl} = this.state
        const isMenuOpen = Boolean(anchorEl)
        return <Menu
            anchorEl={anchorEl}
            anchorOrigin={{vertical: 'top', horizontal: 'right'}}
            transformOrigin={{vertical: 'top', horizontal: 'right'}}
            open={isMenuOpen}
            onClose={this.handleMenuClose}
        >
            <MenuItem onClick={this.goToMyProfile}>{localize('myAccount')}</MenuItem>
        </Menu>
    }

    render() {
        const {classes} = this.props
        return (
            <div className={classes.root}>
                <AppBar position='absolute' className={classes.appBar}>
                    {this.renderBrandToolbar()}
                </AppBar>
            </div>
        )
    }
}

const mapStateToProps = state => ({
    auth: state.auth,
    screen: state.screen
})

export default connect(mapStateToProps)(withRouter(withStyles(styles)(Header)))
