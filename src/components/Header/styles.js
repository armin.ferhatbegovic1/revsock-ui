import RootComponentStyles from '../Styles/RootComponentStyles'
import Dimensions from '../../utils/Dimensions'
import {fade} from '@material-ui/core/styles/colorManipulator'
import Colors from "../../utils/Colors";

const styles = theme => ({
    ...RootComponentStyles,
    appBar: {
        boxShadow: 'unset',
        backgroundColor: Colors.primary,
        height: 64,
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'center'
    },
    root: {},
    grow: {
        flexGrow: 1
    },
    menuButton: {
        marginLeft: -12,
        marginRight: 20
    },
    title: {
        display: 'none',
        [theme.breakpoints.up('sm')]: {
            display: 'block'
        }
    },
    search: {
        position: 'relative',
        borderRadius: theme.shape.borderRadius,
        backgroundColor: fade(theme.palette.common.white, 0.15),
        '&:hover': {
            backgroundColor: fade(theme.palette.common.white, 0.25)
        },
        marginRight: 1 * 2,
        marginLeft: 0,
        width: '100%',
        [theme.breakpoints.up('sm')]: {
            marginLeft: 1 * 3,
            width: 'auto'
        }
    },
    searchIcon: {
        width: 1 * 9,
        height: '100%',
        position: 'absolute',
        pointerEvents: 'none',
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center'
    },
    inputRoot: {
        color: 'inherit',
        width: '100%'
    },
    inputInput: {
        paddingTop: 1,
        paddingRight: 1,
        paddingBottom: 1,
        paddingLeft: 1 * 10,
        transition: theme.transitions.create('width'),
        width: '100%',
        [theme.breakpoints.up('md')]: {
            width: 200
        }
    },
    sectionDesktop: {
        display: 'none',
        [theme.breakpoints.up('md')]: {
            display: 'flex'
        }
    },
    sectionMobile: {
        display: 'flex',
        [theme.breakpoints.up('md')]: {
            display: 'none'
        }
    },
    toolbarRoot: {
        backgroundColor: Colors.primary,
        width: '80%',
        margin: '0 auto'
    },
    homePageToolbarRoot: {
        justifyContent: 'flex-end',
        backgroundColor: Colors.white,
        paddingTop: Dimensions.baseMargin,
        paddingBottom: Dimensions.baseMargin,

    },
    profileImage: {
        width: 35,
        height: 35,
        borderRadius: 50
    },
    dividersVert: {
        borderLeft: '1px solid #dadada'
    },
    menuItem: {
        color: Colors.white,
        fontSize: Dimensions.textSmall,
        whiteSpace: 'nowrap',
        textDecoration: 'unset',
        padding: '10px 15px',
        fontFamily: "PingFang Regular",
        fontWeight: 'bold',
        '&:hover': {
            fontWeight: 'bold',
            color: '#3d4f9f',
            cursor: 'pointer'
        },
    },
    menuItem2: {
        color: '#3d4f9f',
        fontSize: 16,
        fontFamily: "PingFang Bold",
        textDecoration: 'unset',
        padding: '10px 15px',
        '&:hover': {
            color: '#3d4f9f',
            fontWeight: 'bold',
            cursor: 'pointer'
        },
    },
    menuItemActive: {
        whiteSpace: 'nowrap',
        fontFamily: "PingFang Bold",
        padding: '10px 15px',
        fontSize: 16,
        color: '#3d4f9f',
        fontWeight: 'bold',
        textDecoration: 'unset',
        cursor: 'pointer'
    },
    logoTitle: {
        color: Colors.black,
        fontSize: 30,
        marginLeft: '8%'
    },
    logoTitleMobile: {
        fontSize: 20,
        marginLeft: '8%',
        color: Colors.black
    },
    phoneHeader: {
        marginLeft: Dimensions.doubleBaseMargin
    },
    headerTitle: {
        color: Colors.white,
        marginLeft: 17,
        fontFamily: "PingFang Regular",
        fontWeight: 'bold',
        fontSize: Dimensions.textSmall
    }, avatar: {
       marginTop:7,
        width: Dimensions.userAvatar,
        height: Dimensions.userAvatar
    }
})
export default styles