import React, {Component} from 'react'
import styles from './styles'
import Images from '../../utils/Images'
import 'react-circular-progressbar/dist/styles.css';


export default class SpeechDemoBox extends Component {

    render() {
        const {headerColor, wordsSpeechContent} = this.props

        return <div
            style={Object.assign({}, styles.flexDirectionColumn, styles.speechDemoBoxPanel)}>
            <div style={Object.assign({}, {backgroundColor: headerColor}, styles.headerBox)}></div>
            <div
                style={Object.assign({}, styles.flexDirectionColumn, styles.speechDemoBoxPanelPadding)}>
                <div style={styles.contentRow}>
                    <div style={styles.contentColumn}>
                        <img src={Images.section2Icon} style={styles.boxIcon}/>
                    </div>
                    <div style={styles.contentColumn}>
                        <label style={styles.boxContent}>{wordsSpeechContent['title']}</label>
                        <label style={styles.boxContent}>{wordsSpeechContent['subtitle']}</label>
                    </div>
                </div>
            </div>
        </div>
    }
}


