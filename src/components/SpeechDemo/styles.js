import RootComponentStyles from '../Styles/RootComponentStyles'
import Dimensions from '../../utils/Dimensions'
import Images from "../../utils/Images";

const styles = {
    ...RootComponentStyles,
    speechDemoBoxPanel: {
        width: 340,
        minHeight: 200,
        borderRadius: 10,
        boxShadow: '0px 10px 45px 0px rgba(0, 0, 0, 0.2)',
        margin: '20px auto',
        backgroundImage: 'url(' + Images.section2BoxBackgroundIcon + ')',
        backgroundRepeat: 'no-repeat',
        backgroundPosition: 'bottom right',
        backgroundSize: '20% 70%'
    },
    speechDemoBoxPanelPadding: {
        padding: 20
    },
    headerBox: {
        width: 340,
        height: 15,
        borderTopLeftRadius: 10,
        borderTopRightRadius: 10
    },
    contentRow: {
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'space-evenly',
        alignItems: 'center',
        alignContent: 'center',
        height: '90px',

    },
    contentColumn: {
        display: 'flex',
        flexDirection: 'column'
    },
    tag: {
        backgroundColor: '#ECF0F5',
        color: '#8D9EB0',
        padding: '5px 7px',
        fontSize: 12
    },
    tableRow: {
        display: 'flex',
        flexDirection: 'row',
        flexWrap: 'no-wrap',
        boxSizing: 'border-box'
    },
    wrapper: {
        display: 'flex',
        flexDirection: 'row'
    },
    column: {
        fontSize: 12,
        fontWeight: 'bold',
        flexGrow: 0,
        flexShrink: 0,
        verticalAlign: 'top',
        color: '#8798AB'
    },
    columnCell: {
        flexGrow: 0,
        flexShrink: 0,
        borderTop: '1px solid rgba(160,160,160,1)',
        flexBasis: 65,
        textAlign: 'center',
        height: 37,
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center'
    },
    columnHeaderCell: {
        flexGrow: 0,
        flexShrink: 0,
        flexBasis: 65,
        textAlign: 'center',
        height: 37,
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center'
    },
    headerContent: {
        fontSize: 10,
        fontFamily: 'PingFang Medium',
        fontWeight: 500,
        color: 'rgba(49,49,49,1)',
        lineHeight: '13px'
    },
    content: {
        textAlign: 'center',
        fontFamily: 'PingFang Heavy',
        fontSize: 12,
        fontWeight: 800,
        color: 'rgba(49,49,49,1)',
        lineHeight: '13px'
    },
    title: {
        fontSize: 26,
        fontFamily: 'PingFang Regular',
        fontWeight: 400,
        color: 'rgba(49,49,49,1)',
        lineHeight: '32px',
        marginLeft: Dimensions.tripeBaseMargin
    },
    correctIcon: {
        width: 45,
        height: 45
    },
    greenPlay: {
        width: 45,
        height: 45
    },
    redPlay: {
        width: 45,
        height: 45
    },
    progressBar: {
        width: 90,
        alignSelf: 'center',
        marginTop: Dimensions.doubleBaseMargin,
        marginBottom: Dimensions.tripeBaseMargin,
    },
    boxIcon: {
        width: 30,
        height: 32
    },
    boxContent: {
        fontSize: '20px',
        fontWeight: '300',
        lineHeight: '24px',
        color: 'rgba(19,19,19,1)'
    }
}
export default styles
