import RootComponentStyles from '../Styles/RootComponentStyles'
import Dimensions from '../../utils/Dimensions'
import Colors from '../../utils/Colors'

const styles = {
    ...RootComponentStyles,
    subscribeForm: {
        marginTop:Dimensions.tripleBasePadding,
        paddingTop: Dimensions.doubleBaseMargin,
        paddingBottom: Dimensions.doubleBaseMargin,
        paddingLeft: Dimensions.baseMargin,
        paddingRight: Dimensions.baseMargin,
        borderRadius: 8,
        maxWidth: 520,
      width:'100%',
        height:400
    },
    subscribeButton: {
        backgroundColor: Colors.blue,
        borderRadius: 8
    },
    inputForm: {
        height: 40,
        backgroundColor: '#efefef',
        border: 'unset',
        color: Colors.black,
        fontSize: 12,
        borderRadius: 40,
        padding: Dimensions.baseMargin
    },
    textArea: {
        maxWidth: 500,
         width:'100%',
        height: 100,
        backgroundColor: '#efefef',
        border: 'unset',
        color: Colors.black,
        fontSize: 12,
        borderRadius: 10,
        padding: Dimensions.baseMargin,
        marginBottom: Dimensions.tripleBasePadding
    },
    label: {
        color: '#515151',
        fontSize: 10,
        marginTop: Dimensions.baseMargin,
        marginBottom: Dimensions.baseMargin
    }
}
export default styles
