import React, { Component } from 'react'
import styles from './styles'
import SubMainButton from '../UI/SubMainButton/SubMainButton'
import {alertDialogType, subMainButtonType} from '../../config/Constants'
import Dimensions from '../../utils/Dimensions'
import Mattermost from 'node-mattermost'
import config from '../../config/config'
import {connect} from "react-redux";
import {setToastNotification} from "../../store/actions/toastAction";
import {setAlertDialog} from "../../store/actions/alertDialogAction";
import {setFormDialog} from "../../store/actions/formDialogAction";
const { hookURL,username,channel } = config

class SubscribeForm extends Component {
    constructor (props) {
        super(props)
        this.state = {
            email: '',
            name: '',
            message: ''
        }
        this.mattermost = new Mattermost(hookURL,{});
    }

    handleChange = (event) => {
        let label = event.target['name']
        this.setState({[label]: event.target.value})
    }
    submitForm = async (event) => {
        event.preventDefault()
        const {email,name,message}=this.state
        let sms="Email: "+email+" Name: "+name+" message: "+message
        this.mattermost.send({
            text: sms,
            channel: channel,
            username: username
        });
        this.props.setAlertDialog(true, 'Message Successful Sent', 'Response', alertDialogType.WARNING)

    }

    render () {
        return <form onSubmit={this.submitForm} style={Object.assign({}, styles.flexDirectionColumn,styles.subscribeForm)}>
            <div style={Object.assign({}, styles.flexDirectionRow, {justifyContent: 'space-between',marginBottom:Dimensions.tripeBaseMargin,flexWrap:'wrap'})}>
                <div style={Object.assign({}, styles.flexDirectionColumn, {
                    flex: 1,
                  marginTop:10,
                    marginRight: Dimensions.getWidth()>400?Dimensions.baseMargin:0
                })}>
                    <input id='subscribeForm' type='text' value={this.state.name} name='name'
                           onChange={this.handleChange}
                           placeholder='姓名 . 请输入' style={styles.inputForm} />
                </div>
                <div style={Object.assign({}, styles.flexDirectionColumn, {
                    flex: 1,
                  marginTop:10,
                    marginLeft: Dimensions.getWidth()>400?Dimensions.baseMargin:0
                })}>
                    <input id='subscribeForm' type='text' value={this.state.email} name='email'
                           onChange={this.handleChange}
                           placeholder='联系方式 .' style={styles.inputForm} />
                </div>
            </div>

            <div style={Object.assign({}, styles.flexDirectionRow)}>
                    <input id='subscribeForm' type='text' value={this.state.message} name='message'
                           onChange={this.handleChange}
                           placeholder='请输入内容' style={styles.textArea} />
            </div>
            <div style={Object.assign({}, styles.flexDirectionRow, {justifyContent: 'center',marginTop:30})}>
                <SubMainButton mini={true} label={'发送'} buttonType={subMainButtonType.SECONDARY}
                               type='submit' />
            </div>
        </form>

    }
}
export default connect(null, {setToastNotification, setAlertDialog, setFormDialog})((SubscribeForm))
