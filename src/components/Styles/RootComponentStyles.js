import Dimensions from '../../utils/Dimensions'
import Colors from '../../utils/Colors'
import Metrics from '../../utils/Metrics'

const RootComponentStyles = {
    mainContainer: {
        flex: 1,
        backgroundColor: Colors.background
    },
    mainButton: {
        borderRadius: 12,
        borderWidth: 2,
        borderColor: Colors.primary,
        padding: Metrics.basePadding,
        paddingTop: Metrics.basePadding
    },
    backgroundImage: {
        position: 'absolute',
        top: 0,
        left: 0,
        bottom: 0,
        right: 0
    },
    backgroundColor: {
        backgroundImage: Colors.backgroundImage
    },
    container: {
        flex: 1,
        paddingTop: Metrics.baseMargin
    },
    section: {
        margin: Metrics.section,
        padding: Metrics.baseMargin
    },
    titleText: {
        fontSize: 14,
        color: Colors.text
    },
    centered: {
        display: 'flex',
        alignItems: 'center'
    },
    justifyCenter: {
        display: 'flex',
        justifyContent: 'center'
    },
    flexStart: {
        alignItems: 'flex-start'
    },
    alignRight: {
        display: 'flex',
        justifyContent: 'flex-end'
    },
    alignCenter: {
        display: 'flex',
        justifyContent: 'center'
    },
    spaceAround: {
        display: 'flex',
        justifyContent: 'space-around'
    },
    spaceBetween: {
        display: 'flex',
        justifyContent: 'space-between'
    },
    advancedContainer: {
        paddingTop: 50, paddingBottom: 50
    },
    hidden: {
        display: 'none'
    },
    flex: {
        display: 'flex'
    },
    initialDisplay: {
        display: 'initial'
    },
    flexFiller: {
        flex: 1
    },
    autoWidth: {
        width: 'auto'
    },
    fullWidth: {
        width: '100%',
        boxSizing: 'border-box'
    },
    autoHeight: {
        height: 'auto'
    },
    fullHeight: {
        height: '100%',
        boxSizing: 'border-box'
    },
    fullMinWidth: {
        minWidth: '100%',
        boxSizing: 'border-box'
    },
    fullMinHeight: {
        minHeight: '100%',
        boxSizing: 'border-box'
    },
    fullDimensions: {
        width: '100%',
        height: '100%',
        boxSizing: 'border-box'
    },
    halfWidth: {
        width: '50%'
    },
    halfHeight: {
        height: '50%'
    },
    thirdWidth: {
        width: '75%'
    },
    thirdHeight: {
        height: '75%'
    },
    horizontalCenter: {
        display: 'flex',
        justifyContent: 'center',
        flexDirection: 'row'
    },
    horizontalStart: {
        display: 'flex',
        justifyContent: 'flex-start',
        flexDirection: 'row'
    },
    horizontalEnd: {
        display: 'flex',
        justifyContent: 'flex-end',
        flexDirection: 'row'
    },
    horizontalSpaceAround: {
        display: 'flex',
        justifyContent: 'space-around',
        flexDirection: 'row'
    },
    verticalSpaceAround: {
        display: 'flex',
        justifyContent: 'space-around',
        flexDirection: 'column'
    },
    verticalCenter: {
        display: 'flex',
        alignItems: 'center',
        flexDirection: 'column'
    },
    verticalEnd: {
        display: 'flex',
        justifyContent: 'flex-end',
        flexDirection: 'column'
    },
    fullCenter: {
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center'
    },
    flexDirectionRow: {
        display: 'flex',
        flexDirection: 'row'
    },
    flexDirectionColumn: {
        display: 'flex',
        flexDirection: 'column'
    },
    textCenter: {
        textAlign: 'center'
    },
    textLeft: {
        textAlign: 'left'
    },
    textRight: {
        textAlign: 'right'
    },
    textJustify: {
        textAlign: 'justify'
    },
    halfMarginFull: {
        margin: Metrics.smallMargin
    },
    baseMarginFull: {
        margin: Metrics.baseMargin
    },
    doubleMarginFull: {
        margin: Metrics.doubleBaseMargin
    },
    tripleMarginFull: {
        margin: Metrics.tripeBaseMargin
    },
    tinyMarginHorizontal: {
        marginLeft: Metrics.tinyMargin,
        marginRight: Metrics.tinyMargin
    },
    halfMarginHorizontal: {
        marginLeft: Metrics.smallMargin,
        marginRight: Metrics.smallMargin
    },
    tinyMarginTop: {
        marginTop: Metrics.tinyMargin
    },
    baseMarginHorizontal: {
        marginLeft: Metrics.baseMargin,
        marginRight: Metrics.baseMargin
    },
    halfMarginVertical: {
        marginTop: Metrics.smallMargin,
        marginBottom: Metrics.smallMargin
    },
    baseMarginVertical: {
        marginTop: Metrics.baseMargin,
        marginBottom: Metrics.baseMargin
    },
    doubleMarginHorizontal: {
        marginLeft: Metrics.doubleBaseMargin,
        marginRight: Metrics.doubleBaseMargin
    },
    doubleMarginVertical: {
        marginTop: Metrics.doubleBaseMargin,
        marginBottom: Metrics.doubleBaseMargin
    },
    baseMarginLeft: {
        marginLeft: Metrics.baseMargin
    },
    baseMarginRight: {
        marginRight: Metrics.baseMargin
    },
    doubleMarginLeft: {
        marginLeft: Metrics.doubleBaseMargin
    },
    doubleMarginRight: {
        marginRight: Metrics.doubleBaseMargin
    },
    baseMarginTop: {
        marginTop: Metrics.baseMargin
    },
    doubleMarginTop: {
        marginTop: Metrics.doubleBaseMargin
    },
    tripleMarginVertical: {
        marginTop: Metrics.tripeBaseMargin,
        marginBottom: Metrics.tripeBaseMargin
    },
    tripleMarginTop: {
        marginTop: Metrics.tripeBaseMargin
    },
    tripleMarginLeft: {
        marginLeft: Metrics.tripeBaseMargin
    },
    tripleMarginRight: {
        marginRight: Metrics.tripeBaseMargin
    },
    halfMarginBottom: {
        marginBottom: Metrics.smallMargin
    },
    halfMarginTop: {
        marginTop: Metrics.smallMargin
    },
    baseMarginBottom: {
        marginBottom: Metrics.baseMargin
    },
    doubleMarginBottom: {
        marginBottom: Metrics.doubleBaseMargin
    },
    tripleMarginBottom: {
        marginBottom: Metrics.tripeBaseMargin
    },
    noMargin: {
        margin: 0
    },
    halfPaddingFull: {
        padding: Metrics.smallPadding
    },
    basePaddingFull: {
        padding: Metrics.basePadding
    },
    doublePaddingFull: {
        padding: Metrics.doubleBasePadding
    },
    tripleBasePaddingFull: {
        padding: Metrics.tripleBasePadding
    },
    tinyPaddingHorizontal: {
        paddingLeft: Metrics.tinyPadding,
        paddingRight: Metrics.tinyPadding
    },
    basePaddingHorizontal: {
        paddingLeft: Metrics.basePadding,
        paddingRight: Metrics.basePadding
    },
    doublePaddingHorizontal: {
        paddingLeft: Metrics.doubleBasePadding,
        paddingRight: Metrics.doubleBasePadding
    },
    halfPaddingVertical: {
        paddingTop: Metrics.smallPadding,
        paddingBottom: Metrics.smallPadding
    },
    basePaddingVertical: {
        paddingTop: Metrics.basePadding,
        paddingBottom: Metrics.basePadding
    },
    doublePaddingVertical: {
        paddingTop: Metrics.doubleBasePadding,
        paddingBottom: Metrics.doubleBasePadding
    },
    basePaddingRight: {
        paddingRight: Metrics.basePadding
    },
    basePaddingTop: {
        paddingTop: Metrics.basePadding
    },
    doublePaddingRight: {
        paddingRight: Metrics.doubleBasePadding
    },
    noPadding: {
        padding: 0
    },
    noPaddingLeft: {
        paddingLeft: 0
    },
    noPaddingRight: {
        paddingRight: 0
    },
    noPaddingHorizontal: {
        paddingLeft: 0,
        paddingRight: 0
    },
    positionAbsolute: {
        position: 'absolute'
    },
    positionFixed: {
        position: 'fixed'
    },
    positionStatic: {
        position: 'static'
    },
    positionRelative: {
        position: 'relative'
    },
    textLight: {
        color: Colors.white
    },
    textDark: {
        color: Colors.dark
    },
    textLarge: {
        fontSize: 18
    },
    textXLarge: {
        fontSize: 20
    },
    textXXLarge: {
        fontSize: 24
    },
    textX3Large: {
        fontSize: 32
    },
    textX4Large: {
        fontSize: 48
    },
    textMedium: {
        fontSize: 15
    },
    textSmall: {
        fontSize: 14
    },
    textXSmall: {
        fontSize: 12
    },
    textXXSmall: {
        fontSize: 10
    },
    textXXXSmall: {
        fontSize: 8
    },
    fontLight: {
        fontWeight: 100
    },
    fontMediumLight: {
        fontWeight: 400
    },
    fontMediumBold: {
        fontWeight: 500
    },
    textBold: {
        fontWeight: 'bold'
    },
    flex2: {
        flex: 2
    },
    flex3: {
        flex: 3
    },
    flex4: {
        flex: 4
    },
    flex5: {
        flex: 5
    },
    textDefault: {
        color: Colors.secondary
    },
    textDefaultLight: {
        color: Colors.secondaryLight
    },
    textDecorationNone: {
        textDecoration: 'none'
    },
    wordBreak: {
        wordBreak: 'break-word'
    },
    selectButton: {
        backgroundColor: Colors.white,
        borderRadius: 100,
        paddingTop: Metrics.basePadding,
        borderWidth: 2,
        borderColor: Colors.primary
    },
    selectButtonSize: {
        width: 100,
        height: 100
    },
    selectButtonSizeTablet: {
        width: 140,
        height: 140
    },
    primaryColor: {
        color: Colors.primary
    },
    primaryColorBackground: {
        backgroundColor: Colors.primary
    },
    secondaryColor: {
        color: Colors.secondary
    },
    secondaryColorBackground: {
        backgroundColor: Colors.secondary
    },
    tripleMarginHorizontal: {
        marginLeft: Metrics.tripeBaseMargin,
        marginRight: Metrics.tripeBaseMargin
    },
    flexWrap: {
        flexWrap: 'wrap'
    },
    clickable: {
        cursor: 'pointer'
    },
    defaultCursor: {
        cursor: 'default'
    },
    autoOverflow: {
        overflow: 'auto'
    },
    scrollable: {
        overflow: 'scroll',
        overflowY: 'auto',
        overflowX: 'auto'
    },
    standardBorder: {
        borderColor: Colors.primary,
        borderStyle: 'solid',
        borderWidth: Metrics.baseBorder,
        borderRadius: Metrics.baseRadius
    },
    whiteBackground: {
        background: Colors.background
    },
    noBorderRadius: {
        borderRadius: 'none'
    },
    baseBorderRadius: {
        borderRadius: Metrics.baseRadius
    },
    doubleBorderRadius: {
        borderRadius: Metrics.doubleRadius
    },
    tripleBorderRadius: {
        borderRadius: Metrics.tripleRadius
    },
    trustBackground: {
        background: Colors.primary
    },
    noTextDecoration: {
        textDecoration: 'none'
    },
    noBorder: {
        border: 'none'
    },
    fitScreenHeight: {
        minHeight: '100vh'
    },
    scroller: {
        '&::-webkit-scrollbar': {
            width: '6px!important',
            height: '6px!important'
        },
        '&::-webkit-scrollbar-thumb': {
            background: '#432f7f',
            width: '12px',
            borderRadius: '20px'
        }
    },
    headerBackground: {
        background: Colors.backgroundForCardHeader
    },
    attachTop: {
        top: 0
    },
    attachLeft: {
        left: 0
    },
    noOverflow: {
        overflow: 'hidden'
    },
    containerFull: {
        width: '100% !important',
        height: '100% !important',
        overflowY: 'auto !important',
        overflowX: 'hidden !important'
    },
    paddingFullStandard: {
        padding: '16px'
    },
    errorMessage: {
        color: 'red'
    },
    formControl: {
        width: Dimensions.inputFields.input3XLarge,
        maxWidth: Dimensions.inputFields.input3XLarge,
        marginTop: '16px'
    },
    chips: {
        display: 'flex',
        flexWrap: 'wrap'
    },
    primaryButton: {
        color: Colors.white,
        backgroundColor: Colors.primary
    },
    secondaryButton: {
        color: Colors.primary,
        backgroundColor: Colors.white,
        borderRadius: 0,
        border:'1px solid'+Colors.primary
    },
    card: {
        maxWidth: 400
    },
    media: {
        height: 0,
        paddingTop: '56.25%' // 16:9
    },
    placeholderMedia: {
        height: 0,
        paddingTop: '56.25%', // 16:9
        opacity: '0.4'
    },
    actions: {
        display: 'flex'
    },
    expand: {
        transform: 'rotate(0deg)',
        marginLeft: 'auto'
        /*  transition: theme.transitions.create('transform', {
            duration: theme.transitions.duration.shortest
          })*/
    },
    expandOpen: {
        transform: 'rotate(180deg)'
    },
    avatar: {
        backgroundColor: 'red' //red[500]
    },
    dealActive: {
        backgroundColor: Colors.active
    },
    dealDraft: {
        backgroundColor: Colors.draft
    },
    requirementLabel: {
        fontWeight: 'bold'
    },
    cardButton: {
        display: 'block',
        textAlign: 'initial'
    },
    previewDealLink: {
        textDecoration: 'none',
        color: Colors.black
    },
    dealCard: {zIndex: 0, textAlign: 'unset'},
    readMore: {
        color: Colors.secondary,
        fontSize: Dimensions.textSmall - 3
    },
    placeholder: {
        opacity: '0.5'
    },
    cardTitleLargeDesktop: {
        fontSize: Dimensions.textMedium
    },
    cardTitleDesktop: {
        fontSize: Dimensions.textMedium
    },
    cardTitleTablet: {
        fontSize: Dimensions.textSmall
    },
    cardTitlePhone: {
        fontSize: Dimensions.textSmall
    },
    categoryChip: {
        marginTop: Dimensions.baseMargin,
        marginLeft: Dimensions.baseMargin / 2,
        marginRight: Dimensions.baseMargin / 2,
        /* color:Colors.blackLight,
         backgroundColor:Colors.gray,*/
        color: Colors.white,
        backgroundColor: Colors.secondary,
        boxShadow: '0px 2px 4px -1px rgba(0,0,0,0.2), 0px 4px 5px 0px rgba(0,0,0,0.14), 0px 1px 10px 0px rgba(0,0,0,0.12)'

    },
    cardDealActions: {
        display: 'flex',
        justifyContent: 'space-between',
        marginTop: Dimensions.baseMargin
    },
    cardDealActionsContainer: {
        flex: 1
    },
    dealDescription: {
        fontStyle: 'italic',
        color: Colors.blackLight
    },
    propertyValueField: {
        color: Colors.secondary,
        lineHeight: 1.8,
        fontStyle: 'italic'
    },
    propertyValueFieldLargeDesktop: {
        fontFamily: "PingFang Regular",
        fontWeight: 'bold',
        fontSize: Dimensions.textSmall,
        color: Colors.textGray2TitleColor
    },

    propertyValueFieldDesktop: {
        fontFamily: "PingFang Regular",
        fontWeight: 'bold',
        fontSize: Dimensions.textSmall,
        color: Colors.textGray2TitleColor
    },

    propertyValueFieldTablet: {
        fontFamily: "PingFang Regular",
        fontWeight: 'bold',
        fontSize: Dimensions.textSmall,
        color: Colors.textGray2TitleColor
    },
    propertyValueFieldPhone: {
        fontFamily: "PingFang Regular",
        fontWeight: 'bold',
        fontSize: Dimensions.textSmall,
        color: Colors.textGray2TitleColor
    },

    propertyField: {
        fontFamily: "PingFang Regular",
        fontWeight: 'bold',
        fontSize: Dimensions.textSmall,
        color: Colors.textGray2TitleColor,
        display: 'inline-flex'
    },

    propertyFieldLargeDesktop: {
        fontSize: Dimensions.textXSmall
    },

    propertyFieldDesktop: {
        fontSize: Dimensions.textXSmall
    },

    propertyFieldTablet: {
        fontSize: Dimensions.textXXSmall
    },
    propertyFieldPhone: {
        fontSize: Dimensions.textXXSmall
    },
    noResultsContainer: {
        color: Colors.secondary,
        fontSize: Dimensions.textX4Large,
        marginLeft: Dimensions.doubleBaseMargin
    },
    noResultsContainerMini: {
        color: Colors.secondary,
        fontSize: Dimensions.textXLarge,
        marginLeft: Dimensions.doubleBaseMargin
    },
    messageContainer: {
        height: 250
    },
    message404: {
        fontSize: 150,
        color: Colors.secondary,
        margin: 0
    },
    messageNotFound: {
        fontSize: 48,
        color: Colors.secondary,
        margin: 0
    },
    cardGlobalTitle: {
        textDecoration: 'unset',
        color: Colors.secondary
    },
    primaryShadow: {
        boxShadow: '0px 2px 4px -1px rgba(0,0,0,0.2), 0px 4px 5px 0px rgba(0,0,0,0.14), 0px 1px 10px 0px rgba(0,0,0,0.12)'
    },
    userImageRound: {
        borderRadius: '50%'
    },
    miniUserImage: {
        width: Dimensions.userImageMiniWidth,
        height: Dimensions.userImageMiniHeight
        //height: 'auto'
    },
    boldText: {
        fontWeight: 'bold'
    },
    noBullets: {
        listStyleType: 'none'
    },
    column: {
        display: 'flex',
        flexDirection: 'column',
        flex: 1
    },
    row: {
        display: 'flex',
        flexDirection: 'row'
    },
    numSubtitle: {},
    textSubtitle: {},
    topBorder1: {
        borderTop: '2px solid #31a2ff'
    },
    topBorder2: {
        borderTop: '2px solid #bbbbbb'
    },
    topBorder3: {
        borderLeft: '2px solid #bbbbbb',
        alignSelf: 'center',
        height: 50
    },
    mainTitleContainer: {
        textAlign: 'left',
        fontSize: Dimensions.textXXLarge,
        paddingTop: Dimensions.tripleBasePadding,
        paddingBottom: Dimensions.tripleBasePadding
    },
    mainTitleContainerNum: {
        paddingLeft: Dimensions.baseMargin,
        paddingRight: Dimensions.baseMargin,
        textAlign: 'left',
        paddingTop: Dimensions.baseMargin,
        fontSize: Dimensions.textX3Large,
        color: 'black'
    },
    circle: {
        backgroundColor: Colors.circleColor,
        borderRadius: 100,
        width: 30,
        height: 30
    },
    squareLilaDark: {
        width: 30,
        height: 30,
        backgroundColor: Colors.bluePrimary,
        borderRadius: 12
    },
    squareLilaLight: {
        width: 30,
        height: 30,
        backgroundColor: Colors.bluePrimary,
        borderRadius: 12
    },
    progressImg: {
        paddingTop: Dimensions.baseMargin
    },
    column2: {
        display: 'flex',
        flexDirection: 'column'
    },
    flow: {
        marginTop: Dimensions.doubleBaseMargin,
        marginBottom: Dimensions.doubleBaseMargin,
        width: '100%'
    },
    opacityHalf: {
        opacity: '0.5'
    },
    sectionTitle: {
        fontSize: 24,
        lineHeight: '27px',
        marginTop: 5,
        marginBottom: 60,
        color: '#3C3C50',
        fontFamily: "PingFang Medium",
        fontWeight: '400px'
    },
    sectionTitleWhite: {
        fontSize: 24,
        lineHeight: '27px',
        marginTop: 5,
        marginBottom: 60,
        color: Colors.white,
        fontFamily: "PingFang Medium",
        fontWeight: '400px'
    },
    sectionSubTitle: {
        fontSize: 15,
        lineHeight: 2
    },
    sectionSubTitle2: {
        fontSize: 15,
        lineHeight: 2,
        color: '#6e7175'
    },
    sectionHeight: {
        flex: 1,
        height: 500,
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center'
    },
    root: {
        flexGrow: 1,
        paddingTop: '10%',
        paddingBottom: '10%',
        minHeight: 500
    },
    footerRoot: {
        flexGrow: 1,
        paddingTop: Dimensions.doubleBaseMargin,
        paddingBottom: Dimensions.doubleBaseMargin,
        justifyContent: 'space-between',
        display: 'flex',
        flexDirection: 'row',
        backgroundColor: '#35343C',
        margin: '0 auto'
    },
    rootWithoutPadding: {
        flexGrow: 1,
        minHeight: 500
    },
    circleBlueMini: {
        width: 20,
        height: 20,
        borderRadius: 50,
        fontSize: Dimensions.textMedium,
        color: Colors.blue,
        lineHeight: 4,
        textAlign: 'center',
        background: '#294af1'
    },
    circleRoseMini: {
        width: 20,
        height: 20,
        borderRadius: 50,
        fontSize: Dimensions.textMedium,
        color: Colors.blue,
        lineHeight: 4,
        textAlign: 'center',
        background: '#f6d5cc'
    },
    circleRoseRed: {
        width: 20,
        height: 20,
        borderRadius: 50,
        fontSize: Dimensions.textMedium,
        color: Colors.blue,
        lineHeight: 4,
        textAlign: 'center',
        background: '#f9ad90'
    }
}

export default RootComponentStyles
