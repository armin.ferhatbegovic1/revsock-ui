import RootComponentStyles from '../Styles/RootComponentStyles'
import Dimensions from '../../utils/Dimensions'
import Colors from '../../utils/Colors'
import { deviceType } from '../../config/Constants'

const styles = {
  ...RootComponentStyles,
  userImage: {
    border: '4px solid ' + Colors.yellow,
    borderRadius: '50%',
    boxShadow: '5px 5px 5px 5px' + Colors.color5
  },
  userImagePhone: {
    width: Dimensions.userImagePhoneDimensions,
    height: Dimensions.userImagePhoneDimensions
  },
  userImageTablet: {
    width: Dimensions.userImageTabletDimensions,
    height: Dimensions.userImageTabletDimensions
  },
  userImageDesktop: {
    width: Dimensions.userImageWidth,
    height: Dimensions.userImageHeight
  },
  userImageLargeDesktop: {
    width: Dimensions.userImageWidth,
    height: Dimensions.userImageHeight
  },

  miniUserImage2: {
    width: Dimensions.userImageMiniWidth,
//    height:'auto',
    height: Dimensions.userImageMiniHeight,
    borderRadius: '50%'
  },
  userImageContainer: {
    width: Dimensions.fullWidth,
    display: 'flex',
    marginLeft: Dimensions.getDevice() === deviceType.PHONE ? 0 : Dimensions.doubleBaseMargin,
    marginTop: Dimensions.getDevice() === deviceType.PHONE ? 0 : Dimensions.baseMargin,
    marginBottom: Dimensions.baseMargin,
    justifyContent: Dimensions.getDevice() === deviceType.PHONE ? 'center' : 'flex-start'
  },
  userImageHeaderContainer: {
    marginTop: 0,
    display: 'flex',
    justifyContent: 'start'
  },
  userImageAddButtonContainer: {
    position: 'relative',
    top: '-79px',
    backgroundColor: Colors.secondary,
    height: 75,
    width: 150,
    borderBottomLeftRadius: 75,
    borderBottomRightRadius: 75,
    opacity: '0.7'

  },
  imageUploadButton: {
    marginLeft: 45,
    marginTop: 0,
    paddingTop: 7
  }
}

export default styles