import ButtonIcon from '../UI/ButtonIcon/ButtonIcon'
import Images from '../../utils/Images'
import styles from '../Section6/styles'
import React, { Component } from 'react'
import { ReactMic } from 'react-mic'
import { subMainButtonType } from '../../config/Constants'

export default class Recorder extends Component {

  constructor (props) {
    super(props)
    this.state = {
      blobObject: null,
      isRecording: false,
      isPaused: false,
      isPlaying: false
    }
  }

  startStopRecording = async () => {
    const {isRecording} = this.state
    if (isRecording) {
      this.setState({isRecording: false})
    } else {
      this.setState({isRecording: true})
    }
  }
  startStopPlaying = () => {
    const {isPlaying} = this.state
    if (isPlaying) {
      this.setState({isPlaying: false})
      this.audioRef.pause()
    } else {
      this.setState({isPlaying: true})
      this.audioRef.play()
    }
  }

  onSave = (blobObject) => {
  }

  onStart = () => {
    console.log('You can tap into the onStart callback')
  }

  onStop = (blobObject) => {
    const {setBlobURL} = this.props
    setBlobURL(blobObject.blobURL)

    this.audioRef.addEventListener('ended', () => {
      this.setState({isPlaying: false})
    })
  }
  componentDidMount() {
    this.audioRef.addEventListener('ended', () => {
      this.setState({isPlaying: false})
    })
  }
  onData (recordedBlob) {
    console.log('ONDATA CALL IS BEING CALLED! ', recordedBlob)
  }

  render () {
    const {isRecording, isPlaying} = this.state
    const {blobURL,audioSampleForPlay, showControls} = this.props
    return <div style={Object.assign({}, styles.flexDirectionColumn, styles.formContainer)}>
      <ReactMic
        record={isRecording}
        className='sound-wave'
        audioBitsPerSecond={128000}
        onStop={this.onStop}
        onStart={this.onStart}
        onSave={this.onSave}
        onData={this.onData}
        strokeColor='#000000'
        backgroundColor='#FF4081' />
      <audio ref={(input) => {
        this.audioRef = input
      }} controls='controls' src={audioSampleForPlay}
        style={{display: 'none'}}
      ></audio>
      {showControls && <div style={Object.assign({}, styles.flexDirectionRow, styles.panelButtons)}>
        <ButtonIcon handleAction={this.startStopPlaying} style={{width: 130, height: 130}} disabled={ audioSampleForPlay === null }
          buttonType={subMainButtonType.NEUTRAL}>
          {!isPlaying ? <img src={Images.playIcon} alt='play' style={Object.assign({}, styles.buttonIcon,
            )} /> :
            <img src={Images.stopIcon} alt='play' style={Object.assign({}, styles.buttonIcon, {
              opacity: audioSampleForPlay === null ? '0.5' : 1
            })} />}
        </ButtonIcon>
        <ButtonIcon handleAction={() => {
          this.startStopRecording()
        }} style={{width: 130, height: 130}} buttonType={subMainButtonType.NEUTRAL}>
          {!isRecording ? <img src={Images.recordIcon} alt='play' style={styles.buttonIcon} /> :
            <img src={Images.stopIcon} alt='play' style={styles.buttonIcon} />}
        </ButtonIcon>
      </div>}
    </div>
  }
}
