import {LayoutType, role} from '../../config/Constants'
import MainScreen from '../../containers/MainScreen/MainScreen'
import HomeScreen from '../../containers/HomeScreen/HomeScreen'
import NotFound from '../NotFound/NotFound'
import EmptyRoute from '../EmptyRoute/EmptyRoute'
import Dashboard from '../DashboardContainer/DashboardContainer'
import Home from '../HomeContainer/HomeContainer'
import React from 'react'
import VerificationLoginScreen from "../../containers/VerificationLoginScreen/VerificationLoginScreen";
import PasswordLoginScreen from "../../containers/PasswordLoginScreen/PasswordLoginScreen";
import RegisterScreen from '../../containers/RegisterScreen/RegisterScreen'
import ResetPasswordScreen from '../../containers/ResetPasswordScreen/ResetPasswordScreen'
import PaymentScreen from '../../containers/PaymentScreen/PaymentScreen'
import { isEmpty } from '../../services/index'

export const routes = [
    {
        path: '/',
        exact: true,
        component: VerificationLoginScreen,
        layoutType: LayoutType.home,
        authenticate: false,
    },
    {
        path: '/',
        exact: true,
        component: MainScreen,
        layoutType: LayoutType.home,
        authenticate: true,
        role: [role.ADMIN,role.USER]
    },
    {
        path: '/main',
        exact: true,
        component: MainScreen,
        layoutType: LayoutType.home,
        authenticate: true,
        role: [role.ADMIN,role.USER]
    },
    {
        path: '/payment',
        exact: true,
        component: PaymentScreen,
        layoutType: LayoutType.home,
        authenticate: true,
        role: [role.ADMIN,role.USER]
    },
    {
        path: '/home',
        exact: true,
        component: HomeScreen,
        layoutType: LayoutType.home,
        authenticate: false
    },
    {
        path: '/login-verification',
        exact: true,
        component: VerificationLoginScreen,
        layoutType: LayoutType.home,
        authenticate: false
    },
    {
        path: '/login-password',
        exact: true,
        component: PasswordLoginScreen,
        layoutType: LayoutType.home,
        authenticate: false
    },
    {
        path: '/register',
        exact: true,
        component: RegisterScreen,
        layoutType: LayoutType.home,
        authenticate: false
    },{
        path: '/reset-password',
        exact: true,
        component: ResetPasswordScreen,
        layoutType: LayoutType.home,
        authenticate: false
    },
    {
        component: NotFound,
        layoutType: LayoutType.empty,
        authenticate: false
    },
    {
        component: NotFound,
        layoutType: LayoutType.empty,
        authenticate: true,
        role: [role.ADMIN,role.USER]
    }
]

export function getLayoutByType(route, index, authenticate) {
    switch (route.layoutType) {
        case LayoutType.empty:
            return <EmptyRoute key={index} path={route.path} exact={route.exact} component={route.component}/>
        case LayoutType.dashboard:
            return <Dashboard key={index} path={route.path} exact={route.exact} component={route.component}/>
        case LayoutType.home:
            return <Home key={index} path={route.path} exact={route.exact} component={route.component}/>
        default:
            return null
    }
}

export function getPage (route, index, authenticate) {
    const user = authenticate['user']

    if (route.authenticate && !authenticate.isAuthenticated && route.path !== '/login') {
        return <EmptyRoute key={index} path={route.path} exact={route.exact} component={NotFound} />
    }

    if (!isEmpty(route.role) && !isEmpty(user) && route.role.indexOf(user.role) !== -1) {
        return getLayoutByType(route, index, authenticate)
    }
    else if (isEmpty(route.role) && isEmpty(user) && route.authenticate === false) {
        return getLayoutByType(route, index, authenticate)
    } else return null
}
