import { BrowserRouter as Router, Switch } from 'react-router-dom'
import React from 'react'
import { getPage, routes } from './helpers'


const RoutesList = (props) => {
  const {authenticate} = props
  return (
    <Router>
      <Switch>
        {routes.map((route, index) => (
            getPage(route, index, authenticate)
          )
        )}
      </Switch>
    </Router>
  )
}

export default RoutesList