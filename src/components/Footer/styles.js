import RootComponentStyles from '../Styles/RootComponentStyles'
import Colors from '../../utils/Colors'
import Dimensions from '../../utils/Dimensions'
import React from 'react'

const styles = {
  ...RootComponentStyles,
  barcodeFooter: {
    width: 150,
    height: 'auto',
    marginBottom: Dimensions.doubleBaseMargin
  },
  footerContent1: {
    fontFamily: 'PingFang Regular',
    color: '#414250',
    fontSize: 14,
    lineHeight: '20px'
  },
  footerContent2: {
    color: Colors.white,
    fontFamily: 'PingFang Regular',
    fontSize: 14,
    lineHeight: '20px'
  },
  footerFirstSection: {
    textAlign: 'center',
    marginTop: Dimensions.doubleBaseMargin,
    marginBottom: Dimensions.doubleBaseMargin
  },
  footerSecondSection: {
    marginTop: Dimensions.tripeBaseMargin,
    width: '75%',
    justifyContent: 'space-evenly',
    flexWrap: 'wrap'
  },
  footerIcon: {
    marginRight: Dimensions.doubleBaseMargin - 5,
    width: '25px',
    height: '20px',
    alignSelf: 'center'
  },
  footerBarCode: {
    width: 120,
    height: 120,
    borderRadius: '3px'
  },
  footerLabel: {
    color: Colors.white,
    marginLeft: 10,
    fontSize: 15
  },
  footerPanel: {
    minHeight: 300,
    margin: '0 auto',
    alignItems: 'center',
    justifyContent: 'space-between',
    maxWidth: 1100,
    width: '100%',
    paddingLeft:20,
    paddingRight:20,
    flexWrap:'wrap'
  },
  columnFooter: {
    height: 200,
    justifyContent: 'space-evenly'
  }

}
export default styles
