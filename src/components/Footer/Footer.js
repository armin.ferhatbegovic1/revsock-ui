import React, {Component} from 'react'
import styles from './styles'
import Images from '../../utils/Images'
import Dimensions from '../../utils/Dimensions'

export default class Footer extends Component {
    render() {
        return <div style={Object.assign({}, styles.footerRoot)}>
            <div style={Object.assign({}, styles.flexDirectionRow,styles.footerPanel)}>
            <div style={Object.assign({}, styles.flexDirectionColumn)}>
                <img src={Images.footerBarCode} style={styles.footerBarCode} alt='footerBarCode'/>
            </div>
            <div style={Object.assign({}, styles.flexDirectionColumn,styles.columnFooter)}>
                <div style={Object.assign({}, styles.flexDirectionRow)}>
                    <img src={Images.footerIcon1} style={styles.footerIcon} />
                    <label style={styles.footerLabel}>+0755-86329013</label>
                </div>
                <div style={Object.assign({}, styles.flexDirectionRow)}>
                    <img src={Images.footerIcon2} style={styles.footerIcon} />
                    <label style={styles.footerLabel}>nlau@intlcx.com</label>
                </div>
            </div>
            <div style={Object.assign({}, styles.flexDirectionColumn ,styles.columnFooter)}>
                <div style={Object.assign({}, styles.flexDirectionRow)}>
                    <img src={Images.footerIcon3} style={styles.footerIcon} />
                    <div style={Object.assign({}, styles.flexDirectionColumn)}>
                        <label style={styles.footerLabel}>深圳市数字星河科技有限公司 </label>
                    <label style={styles.footerLabel}>SHEN ZHEN DIGITAL GALAXY TECHNOLOGYCO.,LTD</label>
                    </div>
                </div>
                <div style={Object.assign({}, styles.flexDirectionRow)}>
                    <img src={Images.footerIcon4} style={styles.footerIcon} />
                    <div style={Object.assign({}, styles.flexDirectionColumn)}>
                    <label style={styles.footerLabel}>深圳市南山区深圳湾生态科技园6栋909室</label>
                    <label style={styles.footerLabel}>Room 909, S. Bldg. 6, Shen Zhen Bay EcoPar</label>
                    </div></div>
            </div>
            </div></div>
    }
}