import Images from "../../utils/Images";

export const users1 = [
    {
        shortName: 'UT',
        src: Images.ut,
        date: '29 April',
        time: '11:30 AM',
        title: 'Kashif added the card build graph for iOS app ho...'
    },
    {
        shortName: 'KA',
        src: Images.ka,
        date: '29 April',
        time: '11:30 AM',
        title: 'Hyaat merged tagged Ahad in the card Imrpve GPS perf...'
    },
    {
        shortName: 'SA',
        src: Images.sa,
        date: '29 April',
        time: '11:30 AM',
        title: 'Hibba removed Gassan from the Therify board.'
    },
    {
        shortName: 'MA',
        src: Images.ma,
        date: '29 April',
        time: '11:30 AM',
        title: 'Hyaat tagged Riza in the card Remove the edit u...'
    },
    {
        shortName: 'QS',
        src: Images.qs,
        date: '29 April',
        time: '11:30 AM',
        title: 'Meeran tagged Neelam in the card get laptop ap...'
    }, {
        shortName: 'TM',
        src: Images.tm,
        date: '29 April',
        time: '11:30 AM',
        title: 'Nasiha Archived the list Ask the client'
    }, {
        shortName: 'TM',
        src: Images.tm,
        date: '29 April',
        time: '11:30 AM',
        title: 'Nasiha Archived the list Ask the client'
    }, {
        shortName: 'TM',
        src: Images.tm,
        date: '29 April',
        time: '11:30 AM',
        title: 'Nasiha Archived the list Ask the client'
    }
]
export const users2 = [
    {
        shortName: 'UT',
        src: Images.ut,
        date: '29 April',
        time: '11:30 AM',
        title: 'Kashif commited code to the repo therifyv2.'
    },
    {
        shortName: 'KA',
        src: Images.ka,
        date: '29 April',
        time: '11:30 AM',
        title: 'Hyaat merged pull request edits to therify.'
    },
    {
        shortName: 'SA',
        src: Images.sa,
        date: '29 April',
        time: '11:30 AM',
        title: 'Hibbs removed Gassan from the Therify repo.'
    },
    {
        shortName: 'MA',
        src: Images.ma,
        date: '29 April',
        time: '11:30 AM',
        title: 'Hyaat commited on Rayyas pull request.'
    },
    {
        shortName: 'QS',
        src: Images.qs,
        date: '29 April',
        time: '11:30 AM',
        title: 'Meeran tagged Neelam in the comment how ma...'
    }, {
        shortName: 'TM',
        src: Images.tm,
        date: '29 April',
        time: '11:30 AM',
        title: 'Nasiha reviewed talhas code and commented on...'
    }
    , {
        shortName: 'TM',
        src: Images.tm,
        date: '29 April',
        time: '11:30 AM',
        title: 'Nasiha merged the pull request number 203.'
    }
]
