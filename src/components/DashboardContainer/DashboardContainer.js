import { Route } from 'react-router-dom'
import MainLayout from '../../layouts/MainLayout/MainLayout'
import React from 'react'

const dashboardContainer = ({component: Component, ...rest}) => {

  return (
    <Route
      {...rest}
      render={matchProps => (
        <MainLayout>
          <Component {...matchProps} />
        </MainLayout>
      )}
    />
  )
}

export default dashboardContainer;