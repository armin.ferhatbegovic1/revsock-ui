import { menuItemType } from '../../config/Constants'
import { localize } from '../../services'

export const menuItems = [
  {
    type: menuItemType.TITLE,
    name: localize('requestOffers')
  },
  {
    type: menuItemType.LINK,
    name: localize('marketScreen.marketPlace'),
    path: '/list-items'
  },
  {
    type: menuItemType.LINK,
    name: localize('settings'),
    path: '/setting'
  }
]
