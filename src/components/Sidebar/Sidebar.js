import React from 'react';
import classNames from 'classnames';
import {withStyles} from '@material-ui/core/styles';
import Drawer from '@material-ui/core/Drawer';
import List from '@material-ui/core/List';
import Divider from '@material-ui/core/Divider';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import PowerSettingsNewIcon from '@material-ui/icons/PowerSettingsNew'
import Colors from "../../utils/Colors";
import Images from "../../utils/Images";
import Dimensions from "../../utils/Dimensions";
import connect from "react-redux/es/connect/connect";

const drawerWidth = 240;

const styles = theme => ({
    root: {

    },
    appBar: {
        zIndex: theme.zIndex.drawer + 1,
        transition: theme.transitions.create(['width', 'margin'], {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.leavingScreen,
        }),
    },
    appBarShift: {
        marginLeft: 10,
        width: `calc(100% - ${drawerWidth}px)`,
        transition: theme.transitions.create(['width', 'margin'], {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.enteringScreen,
        }),

    },
    menuButton: {
        marginLeft: 12,
        marginRight: 36,
    },
    hide: {
        display: 'none',
    },
    drawer: {
        width: drawerWidth,
        flexShrink: 0,
        whiteSpace: 'nowrap',
    },
    drawerOpen: {

        width: drawerWidth,
        transition: theme.transitions.create('width', {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.enteringScreen,
        }),
    },
    drawerCloseMobile: {
        marginTop: 65,
        backgroundColor: '#222222',
        transition: theme.transitions.create('width', {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.leavingScreen,
        }),
        overflowX: 'hidden',
        width: 1 * 7 + 1,
        [theme.breakpoints.up('sm')]: {
            width: 1 * 12 + 1,
        },
    },
    drawerClose: {
        marginTop: 85,
        backgroundColor: '#222222',
        transition: theme.transitions.create('width', {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.leavingScreen,
        }),
        overflowX: 'hidden',
        width: 1 * 7 + 1,
        [theme.breakpoints.up('sm')]: {
            width: 1 * 12 + 1,
        },
    },
    toolbar: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'flex-end',
        padding: '0 8px',
        ...theme.mixins.toolbar
    },
    content: {
        flexGrow: 1,
        padding: 1 * 3,
    },
    menuItem: {
        display: 'flex',
        flexDirection: 'column',
        paddingBottom: Dimensions.tripleBasePadding
    },
    menuItemAdvanced: {
        display: 'flex',
        flexDirection: 'column',
        paddingBottom: Dimensions.tripleBasePadding * 3
    }
});

class MiniDrawer extends React.Component {
    state = {
        open: false,
    };

    handleDrawerOpen = () => {
        this.setState({open: true});
    };


    render() {
        const {classes} = this.props;

        return (
            <div className={classes.root}>

                <Drawer
                    variant="permanent"
                    className={classNames(classes.drawer, {
                        [classes.drawerOpen]: this.state.open,
                        [Dimensions.getWidth() > 620 ? classes.drawerClose : classes.drawerCloseMobile]: !this.state.open,
                    })}
                    classes={{
                        paper: classNames({
                            [classes.drawerOpen]: this.state.open,
                            [Dimensions.getWidth() > 620 ? classes.drawerClose : classes.drawerCloseMobile]: !this.state.open,
                        }),
                    }}
                    open={this.state.open}
                >
                    <Divider/>
                    <List style={{marginTop: 70}}>
                        <ListItem button key={1} className={classes.menuItem}>
                            <ListItemIcon style={{marginRight: 0}}>
                                <img src={Images.heart} style={styles.icon} alt='icon'/>
                            </ListItemIcon>
                        </ListItem>

                        <ListItem button key={2} className={classes.menuItem}>
                            <ListItemIcon style={{marginRight: 0}}>
                                <img src={Images.clock} style={styles.icon} alt='icon'/>
                            </ListItemIcon>
                        </ListItem>
                        <ListItem button key={3} className={classes.menuItem}>
                            <ListItemIcon style={{marginRight: 0}}>
                                <img src={Images.document} style={styles.icon} alt='icon'/>
                            </ListItemIcon>
                        </ListItem>

                        <ListItem button key={4} className={classes.menuItemAdvanced}>
                            <ListItemIcon style={{marginRight: 0}}>
                                <img src={Images.cat} style={styles.icon} alt='icon'/>
                            </ListItemIcon>
                        </ListItem>

                        <ListItem button key={5} className={classes.menuItem}>
                            <ListItemIcon style={{marginRight: 0}}>
                                <img src={Images.settings} style={styles.icon} alt='icon'/>
                            </ListItemIcon>
                        </ListItem>

                        <ListItem button key={0} className={classes.menuItem}>
                            <ListItemIcon style={{marginRight: 0}}>
                                <PowerSettingsNewIcon style={{color: Colors.white}}/>
                            </ListItemIcon>
                        </ListItem>

                    </List>
                </Drawer>

            </div>
        );
    }
}

const mapStateToProps = state => {
    return {
        auth: state.auth,
        screen: state.screen
    }
}


export default connect(mapStateToProps)(withStyles(styles, {withTheme: true})(MiniDrawer))