import React, { Component, Fragment } from 'react'
import { setFormDialog } from '../../store/actions/formDialogAction'
import { setLoaderVisibility } from '../../store/actions/loaderAction'
import connect from 'react-redux/es/connect/connect'
import { awaitFunction, calculateCountOfItemsInRow, changeListDateFormat, getRequest, isEmpty } from '../../services'
import {
  BATCH_SIZE,
  cardType,
  dateFormat,
  deviceType,
  loaderVisbility,
  request,
  requestHeader,
  THRESHOLD_COUNT
} from '../../config/Constants'
import { calculateGrid, createLinkForFetchingListData, getHeaderHeight } from '../../services/GlobalServices'
import InfiniteLoader from 'react-virtualized/dist/es/InfiniteLoader/InfiniteLoader'
import { AutoSizer, List } from 'react-virtualized'
import styles from './styles'
import { fetchingCardDataStatus, placeholderDeal, recalculatePageHeight, renderNoResultsMessage } from './helpers'
import apiEndpoint from '../../config/config'
import Grid from '@material-ui/core/Grid/Grid'
import Dimensions from '../../utils/Dimensions'

const INITIAL_NUM_OF_CARDS_IN_ROW = 4
const VIEW_HEIGHT = Dimensions.getDevice() === deviceType.PHONE ? 450 : 847
export const InfiniteLoaderListHOC = (props) => (WrappedComponent) => {

  class InfiniteLoaderListObject extends Component {
    constructor (props) {
      super(props)
      this.state = {
        loadedRowsMap: {},
        totalCount: 0,
        rowCount: 0,
        viewHeight: VIEW_HEIGHT,
        numberOfCardsInRow: INITIAL_NUM_OF_CARDS_IN_ROW,
        noResultsMessageVisible: false,
        dataCards: []
      }
      this._isMounted = false
      this.InfiniteLoader = React.createRef()
    }

    async componentWillReceiveProps (nextProps) {
      if (nextProps.filter !== this.props.filter) {
        await awaitFunction(this.setState({filter: nextProps.filter}))

        const totalCountOfCardsForRender = await this.getCountOfCardsForRender()
        if (this.calculateCountOfCardsAndRows(totalCountOfCardsForRender)) {
          this.rerenderInfiniteLoader()
          this.forceUpdate()
        }
        const viewHeight = window.innerHeight - getHeaderHeight()
        if (this.state.viewHeight !== viewHeight && this._isMounted) {
          this.setState({viewHeight})
        }
      }
    }

    componentWillUnmount () {
      this._isMounted = false
    }

    getCountOfCardsForRender = async () => {
      const data = await this.getData()
      if (isEmpty(data.totalDocs)) {
        this.showNoResultsMessage()
      }
      return data.totalDocs
    }

    calculateCountOfCardsAndRows (totalCount) {
      const {numberOfCardsInRow, rowCount} = this.state
      const {rowHeight} = props
      if (isEmpty(totalCount)) {
        totalCount = this.state.totalCount
      }
      const newCountOfCardsInRow = this.getCountOfItemsInRowByScreenWidth()
      let changed = false
      if (numberOfCardsInRow !== newCountOfCardsInRow) {
        this.setState({numberOfCardsInRow: newCountOfCardsInRow})
        changed = true
      }
      const roundNumRows = Math.ceil(totalCount / numberOfCardsInRow)
      if (roundNumRows !== rowCount) {
        const newRowCount = roundNumRows !== 1 ? roundNumRows + 1 : roundNumRows
        this.setState({
          noResultsMessageVisible: totalCount === 0,
          totalCount,
          rowCount: newRowCount
        })
        changed = true
        recalculatePageHeight(newRowCount, rowHeight)
      }

      return changed
    }

    getCountOfItemsInRowByScreenWidth () {
      return calculateCountOfItemsInRow()
    }

    async componentDidMount () {
      this._isMounted = true
      const data = await this.getData()
      if (!isEmpty(data.totalDocs)) {
        await awaitFunction(this.calculateCountOfCardsAndRows(data.totalDocs))
        this.props.setLoaderVisibility(loaderVisbility.HIDE)
      } else {
        this.showNoResultsMessage()
      }

    }

    showNoResultsMessage () {
      this.setState({noResultsMessageVisible: true})
    }

    async getData () {
      const {endpoint, setLoaderVisibility} = this.props
      const {filter} = this.state
      setLoaderVisibility(loaderVisbility.SHOW)
      if (this._isMounted)
        return await getRequest(createLinkForFetchingListData(endpoint, 1, 1, 'name', filter))
    }

    renderDataCard (index, showLoadedCard) {
      const {dataCards, rowCount} = this.state
      const {history, listMenuDealActions} = this.props
      const gridCAL = calculateGrid()
      const dataListForShow = showLoadedCard ? dataCards[index] : [1, 2, 3, 4]
      const loaded = showLoadedCard ? cardType.PLACEHOLDER_IMAGE : cardType.PLACEHOLDER

      return dataListForShow.map((item, i) => {
        return (
          <Grid key={i} item xs sm={gridCAL} md={gridCAL} lg={gridCAL} xl={3}>
            <WrappedComponent deal={showLoadedCard ? item : placeholderDeal} key={item._id} history={history}
              listMenuDealActions={listMenuDealActions}
              type={loaded} rerenderInfiniteLoader={this.rerenderInfiniteLoader} index={index} />
          </Grid>
        )
      })
    }

    rerenderInfiniteLoader = () => {
      const {totalCount} = this.state
      if (totalCount !== 0) {
        this.InfiniteLoader.current.resetLoadMoreRowsCache()
        this._loadMoreRows({
          startIndex: 0,
          stopIndex: 11
        })
      } else {
        this.props.setLoaderVisibility(loaderVisbility.HIDE)
      }
    }

    createListOfData (startIndex, stopIndex, data) {
      const {loadedRowsMap, dataCards, numberOfCardsInRow} = this.state
     /* changeListDateFormat(data['docs'], 'startDate', dateFormat.MMMMDDYYYY)
      changeListDateFormat(data['docs'], 'endDate', dateFormat.MMMMDDYYYY)*/

      let c = 0
      const t = stopIndex !== startIndex ? stopIndex : stopIndex + 1
      for (let i = startIndex; i < t; i++) {
        loadedRowsMap[i] = fetchingCardDataStatus.STATUS_LOADED
        dataCards[i] = data.docs.slice(numberOfCardsInRow * c, numberOfCardsInRow * c + numberOfCardsInRow)
        c++
      }
      if (this._isMounted)
        this.setState({dataCards})
    }

    _isRowLoaded = ({index}) => {
      const {loadedRowsMap} = this.state
      return !!loadedRowsMap[index]
    }

    fetchDataList (startIndex, stopIndex, link, resolve) {
      fetch(link, {
        method: request.GET,
        headers: requestHeader()
      })
      .then(function (response) {
        return response.json()
      })
      .then((data) => {
        this.createListOfData(startIndex, stopIndex, data)
        this.forceUpdate()
        this.calculateNumberOfRows(data.totalDocs)
        this.props.setLoaderVisibility(loaderVisbility.HIDE)
        resolve()
      })
    }

    calculateNumberOfRows (totalCount) {
      const {numberOfCardsInRow} = this.state
      const roundNumRows = Math.ceil(totalCount / numberOfCardsInRow)
      const rowsNum = roundNumRows !== 1 ? roundNumRows + 1 : roundNumRows
      this._isMounted && this.setState({rowCount: rowsNum, totalCount, noResultsMessageVisible: totalCount === 0})
    }

    _loadMoreRows = ({startIndex, stopIndex}) => {
      const {loadedRowsMap, totalCount, numberOfCardsInRow, filter} = this.state
      const {endpoint} = this.props
      //if (rowCount !== 1 && rowCount - 1 === deals.length) return
      if (totalCount !== 0) {

        for (let i = startIndex; i < stopIndex; i++) {
          loadedRowsMap[i] = fetchingCardDataStatus.STATUS_LOADING
        }

        return new Promise((resolve) => {
          let size = (stopIndex === startIndex) ? numberOfCardsInRow : (stopIndex - startIndex) * numberOfCardsInRow
          let skip = 0

          if (startIndex !== 0) {
            skip = (startIndex) * numberOfCardsInRow
          }
          const link = createLinkForFetchingListData(apiEndpoint.apiEndpoint + endpoint, skip, size, 'name', filter)
          this._isMounted && this.fetchDataList(startIndex, stopIndex, link, resolve)
        })
      }
    }

    recalculateRowHeight (index, style) {
      const {advancedCardHeight} = props
      const newStyle = {...style}
      if (index !== 0) {
        newStyle['top'] = newStyle['top'] - advancedCardHeight * (index - 1)
      }
      return newStyle
    }

    renderFirstRow (index) {
      const {children} = this.props
      return index === 0 ? children : null
    }

    renderDataCardRow (index) {
      const {loadedRowsMap} = this.state
      return <Grid container spacing={24}>
        {this.renderDataCard(index, loadedRowsMap[index] === fetchingCardDataStatus.STATUS_LOADED)}
      </Grid>
    }

    renderCardRows (index, key) {
      return <div key={key} style={styles.row} id={'row-' + key}>
        {this.renderDataCardRow(index)}
      </div>
    }

    _rowRenderer = ({index, key, style}) => {
      if (index === this.state.rowCount - 1) return null
      return <div key={key} style={this.recalculateRowHeight(index, style)}>
        {this.renderFirstRow(index)}
        {this.renderCardRows(index, key)}
      </div>
    }

    renderCardView () {
      const {dataCards, rowCount, viewHeight} = this.state
      const {rowHeight} = props
      return <InfiniteLoader
        isRowLoaded={this._isRowLoaded}
        loadMoreRows={this._loadMoreRows}
        rowCount={rowCount}
        ref={this.InfiniteLoader}
        minimumBatchSize={BATCH_SIZE}
        threshold={THRESHOLD_COUNT}
      >
        {({onRowsRendered, registerChild}) => (
          <AutoSizer disableHeight>
            {({width, height}) => {
              return (
                <List
                  {...dataCards}
                  ref={registerChild}
                  height={viewHeight}
                  onRowsRendered={onRowsRendered}
                  rowCount={rowCount}
                  rowHeight={rowHeight}
                  rowRenderer={this._rowRenderer}
                  width={width}
                />

              )
            }}
          </AutoSizer>
        )}
      </InfiniteLoader>
    }

    renderCardDataList () {
      const {totalCount, noResultsMessageVisible} = this.state
      const {children, setLoaderVisibility} = this.props
      return totalCount !== 0 ? this.renderCardView() :
        <div>{children} {renderNoResultsMessage(noResultsMessageVisible, setLoaderVisibility)}</div>
    }

    render () {
      return (
        <Fragment>
          {this.renderCardDataList()}
        </Fragment>
      )
    }

  }

  const mapStateToProps = state => {
    return {
      auth: state.auth,
      screen: state.screen
    }
  }
  return connect(mapStateToProps, {setFormDialog, setLoaderVisibility})((InfiniteLoaderListObject))
}

export default InfiniteLoaderListHOC