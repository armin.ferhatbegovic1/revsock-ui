import React, { Component } from 'react'
import { calculateCountOfItemsInRow, isEmpty } from '../../services'
import { loaderVisbility, request, requestHeader } from '../../config/Constants'
import { fetchingCardDataStatus } from '../ListInfluencers/helpers'
import { createLinkForFetchingListData, getHeaderHeight } from '../../services/GlobalServices'
import apiEndpoint from '../../config/config'

const INITIAL_NUM_OF_CARDS_IN_ROW = 4

export class InfiniteLoaderList extends Component {
  constructor (rowHeight, viewHeight, rowHeightAdvancedSufix, props) {
    super(props)
    this.loadedRowsMap = {}
    this.totalCount = 0
    this.rowCount = 0
    this._isMounted = false
    this.viewHeight = viewHeight
    this.rowHeight = rowHeight
    this.rowHeightAdvancedSufix = rowHeightAdvancedSufix
    this.numberOfCardsInRow = INITIAL_NUM_OF_CARDS_IN_ROW
    this.InfiniteLoader = React.createRef()
    this.noResultsMessageVisible = false
  }

  componentWillUnmount () {
    this._isMounted = false
  }

  recalculatePageHeight () {
    const existStyle = document.getElementsByClassName('ReactVirtualized__Grid__innerScrollContainer')[0]
    if (!isEmpty(existStyle) && this.rowCount > 1) {
      document.getElementsByClassName('ReactVirtualized__Grid__innerScrollContainer')[0].style.maxHeight = (this.rowCount - 1) * this.rowHeight + 'px'
    }
  }

  setNumberOfCardsInRow (numberOfCardsInRow) {
    this.numberOfCardsInRow = numberOfCardsInRow
  }

  showNoResultsMessage () {
    this.noResultsMessageVisible = true
  }

  fetchDealsList (startIndex, stopIndex, link, resolve) {
    fetch(link, {
      method: request.GET,
      headers: requestHeader()
    })
    .then(function (response) {
      return response.json()
    })
    .then((data) => {
      this.createListOfCards(startIndex, stopIndex, data)
      this.forceUpdate()
      this.calculateCountOfCardsAndRows(data.totalDocs)
      this.props.setLoaderVisibility(loaderVisbility.HIDE)

      resolve()
    })
  }

  rerenderInfiniteLoader = () => {
    if (this.totalCount !== 0) {
      this.InfiniteLoader.current.resetLoadMoreRowsCache()
      this._loadMoreRows({
        startIndex: 0,
        stopIndex: 11
      })
    } else {
      this.props.setLoaderVisibility(loaderVisbility.HIDE)
    }
  }

  calculateViewHeight () {
    const viewHeight = window.innerHeight - getHeaderHeight() - 100
    if (this.viewHeight !== viewHeight && this._isMounted) {
      this.viewHeight = viewHeight
    }
  }

  recalculateRowHeight (index, style) {
    const newStyle = {...style}
    if (index !== 0) {
      newStyle['top'] = newStyle['top'] - this.rowHeightAdvancedSufix * (index - 1)
    }
    return newStyle
  }

  getCountOfItemsInRowByScreenWidth () {
    return calculateCountOfItemsInRow()
  }

  calculateCountOfCardsAndRows (totalCount) {
    const {numberOfCardsInRow} = this
    if (isEmpty(totalCount)) {
      totalCount = this.totalCount
    }
    const newCountOfCardsInRow = this.getCountOfItemsInRowByScreenWidth()
    let changed = false
    if (numberOfCardsInRow !== newCountOfCardsInRow) {
      this.setNumberOfCardsInRow(newCountOfCardsInRow)
      changed = true
    }
    const roundNumRows = Math.ceil(totalCount / numberOfCardsInRow)
    if (roundNumRows !== this.rowCount) {
      this.totalCount = totalCount
      this.rowCount = roundNumRows !== 1 ? roundNumRows + 1 : roundNumRows
      this.noResultsMessageVisible = totalCount === 0
      changed = true
    }
    return changed
  }

  _isRowLoaded = ({index}) => {
    const {loadedRowsMap} = this
    return !!loadedRowsMap[index]
  }
  _loadMoreRows = ({startIndex, stopIndex}) => {
    const {numberOfCardsInRow, loadedRowsMap} = this
    const {filter, endpoint} = this.props
    if (this.totalCount !== 0) {

      for (let i = startIndex; i < stopIndex; i++) {
        loadedRowsMap[i] = fetchingCardDataStatus.STATUS_LOADING
      }

      return new Promise((resolve) => {
        let size = (stopIndex === startIndex) ? numberOfCardsInRow : (stopIndex - startIndex) * numberOfCardsInRow
        let skip = 0

        if (startIndex !== 0) {
          skip = (startIndex) * numberOfCardsInRow
        }
        const link = createLinkForFetchingListData(apiEndpoint.apiEndpoint + endpoint, skip, size, 'name', filter)
        this._isMounted && this.fetchDealsList(startIndex, stopIndex, link, resolve)
      })
    }
  }

}