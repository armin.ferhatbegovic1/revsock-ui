import { isEmpty } from '../../services'

export const fetchingCardDataStatus = {
  STATUS_LOADING: 1,
  STATUS_LOADED: 2
}

export const placeholderDeal = {
  description: '*************************',
  endDate: '1554371709143',
  name: '****************',
  numberOfFollowers: 10000000000,
  startDate: '1554371709143',
  status: 'Active',
  _id: '5c76a15b0ef9d017e0547996'
}

export function recalculatePageHeight (rowCount, rowHeight) {
  const existStyle = document.getElementsByClassName('ReactVirtualized__Grid__innerScrollContainer')[0]
  if (!isEmpty(existStyle) && rowCount > 1) {
    document.getElementsByClassName('ReactVirtualized__Grid__innerScrollContainer')[0].style.maxHeight = (rowCount - 1) * rowHeight + 'px'
  }
}
