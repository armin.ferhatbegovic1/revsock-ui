import React from "react";
import Dimensions from "../../../utils/Dimensions";
import RootComponentStyles from "../../Styles/RootComponentStyles";
import Colors from "../../../utils/Colors";

const borderSize = 5
const styles = {
    ...RootComponentStyles,
    shortName: {
        fontSize: Dimensions.textMedium,
        color: Colors.shortNameColor,
        fontWeight:'bold',
        marginTop:Dimensions.baseMargin
    },
    userImage: {
        width: 30,
        height: 30
    },
    subMainProgress: {
        width: 10,
        backgroundColor: Colors.lila,
        margin: '0px auto 10px auto ',
        borderBottomLeftRadius: borderSize,
        borderBottomRightRadius: borderSize
    },
    mainProgress: {
        width: 10,
        backgroundColor: 'black',
        margin: '10px auto 0 auto',
        borderTopLeftRadius: borderSize,
        borderTopRightRadius: borderSize
    },
    alignElements: {
        justifyContent: 'flex-end'
    }

}

const UserTaskProgress2 = (props) => {
    const {submainProgress, mainProgress, shortName, userImage} = props
    return <div style={styles.row}>
        <div style={Object.assign({}, styles.alignElements, styles.column)}>
            <div style={Object.assign({}, styles.mainProgress, {height: mainProgress * 2})}></div>
            <div style={Object.assign({}, styles.subMainProgress, {height: submainProgress * 2})}></div>
            <img src={userImage} style={styles.userImage}/>
            <label style={styles.shortName}>{shortName}</label>

        </div>
    </div>
}
export default UserTaskProgress2