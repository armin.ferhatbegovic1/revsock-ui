import {isEmpty} from "../../../services";
import React from "react";
import Dimensions from "../../../utils/Dimensions";
import Colors from "../../../utils/Colors";
import RootComponentStyles from "../../Styles/RootComponentStyles";

const styles = {
    ...RootComponentStyles,
    containerWrapper: {
        marginTop: Dimensions.doubleBaseMargin,
        display: 'flex',
        flexDirection: 'column'
    },
    radioButtons: {},
    title: {textAlign: 'left'},
    showPriceLabel: {
        fontSize: Dimensions.textSmall,
        color: Colors.black,
        textAlign: 'left'
    },
    showPriceValueCurrency: {
        fontSize: Dimensions.textMedium,
        color: Colors.red,
        marginLeft:5
    }
}

export default function ShowPrice(props) {
    const {currency, value, label, row} = props
    let tmpRow = !isEmpty(row) ? row : false
    return <div style={Object.assign({},tmpRow ? styles.flexDirectionRow : styles.flexDirectionColumn,{alignItems:tmpRow?'center':'baseline'})}>
        <label style={styles.showPriceLabel}>{label}</label>
        <div style={Object.assign({}, styles.flexDirectionRow, styles.showPriceValueCurrency)}>
            <label>{currency}</label> <label>{value}</label>
        </div>
    </div>
}