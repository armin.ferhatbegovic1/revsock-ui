import React from 'react'
import RootContainerStyles from "../../../containers/Styles/RootContainerStyles";
import Colors from '../../../utils/Colors'
import Dimensions from "../../../utils/Dimensions";

const styles = {
    ...RootContainerStyles,
    mainWrapper: {
        border: '2px solid rgba(220,220,220,1)',
        borderRadius: '10px',
        display: 'flex',
        flexDirection: 'row',
        width: 150,
        height: 41,
        backgroundColor: Colors.white,
        marginRight: Dimensions.baseMargin
    },
    buttonLabel: {
        flex: 1,
        lineHeight: '34px',
        fontSize: Dimensions.textXLarge,
        fontWeight: 'bold',
        color: 'rgba(220,220,220,1)',

    },
    showValue: {
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'center',
        flex: 1,
        backgroundColor: Colors.grayIncrementer,
        color: Colors.primary,
        fontSize: Dimensions.textLarge
    },
    label: {
        textAlign: 'left',
        fontSize: Dimensions.textMedium,
        color: Colors.textGrayTitleColor
    }
}

const IncrementerDisplay = (props) => {
    const {label} = props
    const [quantity, setQuantity] = React.useState(0);
    const handleUp = event => {
        setQuantity(quantity + 1);
    };
    const handleDown = event => {
        if (quantity !== 0) {
            setQuantity(quantity - 1);
        }
    };
    return <div style={styles.flexDirectionColumn}>
        <label style={styles.label}>{label}</label>
        <div style={styles.mainWrapper}>
            <div style={styles.buttonLabel} className='pointerButton' onClick={handleDown}>-</div>
            <div style={styles.showValue}>{quantity}</div>
            <div style={styles.buttonLabel} onClick={handleUp} className='pointerButton'>+</div>
        </div>
    </div>

}


export default IncrementerDisplay
