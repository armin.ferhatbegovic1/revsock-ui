import React from 'react'
import IconButton from '@material-ui/core/IconButton'
import Menu from '@material-ui/core/Menu'
import MenuItem from '@material-ui/core/MenuItem'
import MoreVertIcon from '@material-ui/icons/MoreVert'

class TableMenu extends React.Component {
  constructor (props) {
    super(props)
    this.state = {
      anchorEl: null,
      actions: this.props.actions,
      argumentForAction:this.props.argumentForAction
    }
  }

  handleClick = event => {
    this.setState({anchorEl: event.currentTarget})
  }

  handleClose = () => {
    this.setState({anchorEl: null})
  }

  render () {
    const {anchorEl, actions} = this.state
    const {argumentForAction}=this.props
    const open = Boolean(anchorEl)

    return (
      <div>
        <IconButton
          aria-label='More'
          aria-owns={open ? 'long-menu' : undefined}
          aria-haspopup='true'
          onClick={this.handleClick}
        >
          <MoreVertIcon />
        </IconButton>
        <Menu
          style={{marginLeft: '-30px'}}
          id='simple-menu'
          anchorEl={anchorEl}
          open={Boolean(anchorEl)}
          onClose={this.handleClose}
        >
          {
            actions.map((item,i) => {
              return <MenuItem key={i} onClick={()=>{item.action(argumentForAction); this.handleClose()}}>{item.actionLabel}</MenuItem>
            })
          }
        </Menu>
      </div>
    )
  }
}

export default TableMenu