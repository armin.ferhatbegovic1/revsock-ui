import React, { Component } from 'react'
import Button from '@material-ui/core/Button'
import Dialog from '@material-ui/core/Dialog'
import DialogActions from '@material-ui/core/DialogActions'
import DialogContent from '@material-ui/core/DialogContent'
import DialogContentText from '@material-ui/core/DialogContentText'
import DialogTitle from '@material-ui/core/DialogTitle'
import { connect } from 'react-redux'
import {
  FORM_DIALOG_ACTION,
  FORM_DIALOG_ACTION_CALLBACK,
  FORM_DIALOG_MESSAGE,
  FORM_DIALOG_OPEN
} from '../../../store/actions/types'
import {
  alertDialogType,
  autoCloseDialog,
  formMode,
  layoutType,
  positionButton,
  subMainButtonType
} from '../../../config/Constants'
import {
  handleErrorResponse,
  handleResponseCode,
  isEmpty,
  localize,
  prepareDataFormForSubmitAction
} from '../../../services'
import InputForm from '../InputForm/InputForm'
import SubMainButton from '../SubMainButton/SubMainButton'
import styles from './styles'
import Colors from '../../../utils/Colors'

const TIME_CLOSE_DIALOG = 1500

class FormDialog extends Component {
  constructor (props) {
    super(props)
    this.state = {
      formFields: null,
      response: null,
      notReturn: false
    }
  }

  componentWillReceiveProps (nextProps) {
    if (nextProps.formFields !== this.state.formFields) {
      this.setState({formFields: JSON.parse(JSON.stringify(nextProps.formFields))})
    }
  }

  submitFormAction = async () => {
    const {formFields} = this.state
    const {action} = this.props
    const data = prepareDataFormForSubmitAction(formFields)
    const response = await action(data)
    this.afterSubmitActionResponse(response)
  }

  afterSubmitActionResponse = (responseData) => {
    const {afterResponseCallback} = this.props
    let response = ''
    if (!isEmpty(responseData.error)) {
      response = handleErrorResponse(responseData)
    }
    else {
      this.setState({notReturn: true})
      response = handleResponseCode(responseData)
    }
    this.setState({response})
    if (!isEmpty(afterResponseCallback)) {
      afterResponseCallback()
    }
  }

  closeDialog = () => {
    const {dispatch} = this.props
    dispatch({type: FORM_DIALOG_OPEN, payload: false})
    dispatch({type: FORM_DIALOG_ACTION, payload: null})
    dispatch({type: FORM_DIALOG_ACTION_CALLBACK, payload: null})
    this.setState({formFields: null, response: null, notReturn: false})
    dispatch({type: FORM_DIALOG_MESSAGE, payload: ''})
  }

  getTitle (type) {
    switch (type) {
      case alertDialogType.WARNING:
        return localize('warningMessage')
      case alertDialogType.ERROR:
        return localize('errorMessage')
      case alertDialogType.INFO:
        return localize('infoMessage')
      case alertDialogType.CONFIRM:
        return localize('confirmMessage')
      default:
        return ''
    }
  }

  renderSubmitForm () {
    const {formFields} = this.state
    return !isEmpty(formFields) ? <InputForm formElements={formFields}
      prepareDataAndSubmit={this.submitFormAction} formContainer={styles.formContainer}
      buttonLabel={localize('cardBid.confirm')}
      mode={formMode.NEW} data={null} layout={layoutType.COLUMNS_1} lastElementInLayout={layoutType.COLUMNS_1}
      saveButtonPosition={positionButton.RIGHT}>
      <SubMainButton mini={true} label={localize('cardDeal.close')} buttonType={subMainButtonType.SECONDARY}
        onClick={() => this.closeDialog()} />
    </InputForm> : null
  }

  returnBackOnForm = () => {
    this.setState({response: null})
  }

  renderActionsAfterResponse () {
    const {autoClose} = this.props
    const {response, notReturn} = this.state
    const action = notReturn === true || autoClose !== autoCloseDialog.AUTO_CLOSE_DIALOG_FALSE ? this.closeDialog : this.returnBackOnForm
    return !isEmpty(response) ? <DialogActions><Button onClick={action}
      color='primary'>{localize('ok')}</Button></DialogActions> : null
  }

  renderResponse () {
    const {response, notReturn} = this.state
    const {autoClose} = this.props
    if (notReturn || autoClose === autoCloseDialog.AUTO_CLOSE_DIALOG_TRUE) {
      this.autoCloseDialog()
    }
    return <label>{response}</label>
  }

  autoCloseDialog = () => {
    setTimeout(() => {
      this.closeDialog()
    }, TIME_CLOSE_DIALOG)
  }


  renderContent () {
    const {response} = this.state
    const {type, message} = this.props
    let dialogContent = ''
    switch (type) {
      case alertDialogType.DEFAULT:
        dialogContent = message
        break
      default:
        dialogContent = <DialogContentText style={{paddingTop:24}}>{isEmpty(response) ? message : this.renderResponse()}</DialogContentText>
    }
    return dialogContent
  }

  render () {
    const { open, type, title} = this.props
    const {response} = this.state
    let dialogTitle = !isEmpty(title) ? title : this.getTitle(type)
    return (
      <div>
        <Dialog
          open={open}
          onClose={this.closeDialog}
          aria-labelledby='form-dialog-title'
          aria-describedby='form-dialog-description'
        >
          <DialogTitle style={{backgroundColor: Colors.secondary}}> <label style={{color: Colors.white}}>{dialogTitle}</label></DialogTitle>
          <DialogContent>
            {this.renderContent()}
            {isEmpty(response) ? this.renderSubmitForm() : ''}
          </DialogContent>
          {this.renderActionsAfterResponse()}
        </Dialog>
      </div>
    )
  }

}

function mapStateToProps (store) {
  return {
    open: store.formDialog.open,
    message: store.formDialog.message,
    type: store.formDialog.type,
    title: store.formDialog.title,
    action: store.formDialog.action,
    formFields: store.formDialog.formFields,
    afterResponseCallback: store.formDialog.afterResponseCallback,
    autoClose: store.formDialog.autoClose
  }
}

export default connect(mapStateToProps)(FormDialog)