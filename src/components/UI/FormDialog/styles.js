import RootComponentStyles from '../../Styles/RootComponentStyles'
import Dimensions from '../../../utils/Dimensions'

const styles = {
  ...RootComponentStyles,
  formContainer: {
    paddingTop: Dimensions.baseMargin,
    paddingBottom: 0,
    paddingLeft: Dimensions.baseMargin,
    paddingRight: Dimensions.baseMargin
  }
}

export default styles