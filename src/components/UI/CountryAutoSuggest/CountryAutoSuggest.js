import React from 'react'
import AutoSuggest from '../AutoSuggest/AutoSuggest'
const GET_COUNTRIES = '/country?q='

const countryAutoSuggest = (props) => {
  const {label, placeholder, onChange, errorMessage, error, value, disabled, style} = props
  return (<AutoSuggest label={label} placeholder={placeholder} requestLink={GET_COUNTRIES} fieldForShow='name'
    valueHandler={onChange} errorMessage={errorMessage} isValid={error} value={value} disabled={disabled}
    style={style} />)
}

export default countryAutoSuggest