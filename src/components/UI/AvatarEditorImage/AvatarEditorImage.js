import React, { Component } from 'react'
import Slider from '@material-ui/lab/Slider'
import AvatarEditor from 'react-avatar-editor'
import withStyles from '@material-ui/core/styles/withStyles'
import Button from '@material-ui/core/Button'
import Dimensions from '../../../utils/Dimensions'
import { buttonTypeHandler, deviceType, subMainButtonType } from '../../../config/Constants'
import { awaitFunction, isEmpty, localize } from '../../../services'
import EditIcon from '@material-ui/icons/AddAPhoto'
import ButtonIcon from '../ButtonIcon/ButtonIcon'
import styles from './styles'
import Images from '../../../utils/Images'
import { connect } from 'react-redux'

const IMAGE_WIDTH = 250
const AD = 8
const INPUT_FIELD_IMG_ID = 'contained-button-file-2'
const INPUT_TYPE__FILE = 'file'

function calculateSizeOfImage () {
  if (Dimensions.getDevice() === deviceType.LARGE_DESKTOP)
    return IMAGE_WIDTH
  if (Dimensions.getDevice() === deviceType.DESKTOP)
    return IMAGE_WIDTH / 2
  if (Dimensions.getDevice() === deviceType.TABLET || Dimensions.getDevice() === deviceType.PHONE)
    return IMAGE_WIDTH / 2
}

class AvatarEditorImage extends Component {
  constructor (props) {
    super(props)
    this.state = {
      scale: 1,
      radius: 0,
      rotate: 0,
      hovered: false,
      saveButtonDisabled: true
    }
  }

  handleChange = (event, value, name) => {
    this.setState({[name]: value})
  }

  handleRotateLeft = () => {
    const {rotate} = this.state
    this.setState({rotate: rotate - 90, saveButtonDisabled: false})
  }
  handleRotateRight = () => {
    const {rotate} = this.state
    this.setState({rotate: rotate + 90, saveButtonDisabled: false})
  }

  onClickSave = () => {
    if (this.editor) {
      const img = this.editor.getImageScaledToCanvas().toDataURL()
      this.props.handleSaveImage(img)
      this.setState({scale: 1, saveButtonDisabled: false, rotate: 0})
    }
  }

  setEditorRef = (editor) => this.editor = editor

  setImageHovered = async (state) => await awaitFunction(this.setState({hovered: state}))

  renderOptionForUploadImage () {
    const {editImage} = this.props
    if (editImage) {
      return <div style={Object.assign({}, {
        top: -1 * (calculateSizeOfImage()) / 2,
        height: calculateSizeOfImage() / 2,
        width: calculateSizeOfImage()
      }, styles.userImageAddButtonContainer)}>
        <div style={Object.assign({}, {marginLeft: (calculateSizeOfImage() - 55) / 2}, styles.imageUploadButton)}>
          <ButtonIcon label={localize('submit')} buttonType={subMainButtonType.NEUTRAL} type={buttonTypeHandler.SUBMIT}
            handleAction={this.uploadImage}>
            <EditIcon />
          </ButtonIcon>
        </div>
      </div>
    }
  }

  uploadImage = () => {this.upload.click()}


  renderOptionsForEditImage () {
    const {scale, saveButtonDisabled} = this.state
    const {classes, editImage} = this.props
    return <div style={!editImage ? styles.imageEditOptionsContainerHidden : styles.imageEditOptionsContainerVisible}>
      <Slider
        disabled={!editImage}
        classes={{container: classes.slider}}
        value={scale}
        aria-labelledby='label'
        min={0}
        max={10}
        step={0.1}
        onChange={(event, value) => {this.handleChange(event, value, 'scale')}}
      />
      <Button variant='contained' size='small' color='primary' style={styles.btnContainer} disabled={!editImage}
        onClick={this.handleRotateLeft}>
        {localize('newDealScreen.left')}
      </Button>
      <Button variant='contained' size='small' color='primary' style={styles.btnContainer} disabled={!editImage}
        onClick={this.handleRotateRight}>
        {localize('newDealScreen.right')}
      </Button>
      <Button variant='contained' size='small' color='primary' style={styles.btnContainer}
        disabled={!editImage || saveButtonDisabled}
        onClick={this.onClickSave}>{localize('newDealScreen.saveImage')}</Button>
    </div>
  }

  render () {
    const {src, mini, imageUploadCallBack} = this.props
    const {scale, radius, rotate, hovered} = this.state
    return (
      <div style={styles.dealImageContainer}>
        <div
          style={mini ? styles.miniUserImage : Object.assign({}, {
            width: (calculateSizeOfImage() + AD),
            height: (calculateSizeOfImage() + AD)
          }, styles.userImage)}
        >
          <div
            onMouseEnter={() => this.setImageHovered(true)}
            onMouseLeave={() => this.setImageHovered(false)}>
            <AvatarEditor
              ref={this.setEditorRef}
              image={!isEmpty(src) ? src : Images.logo}
              width={calculateSizeOfImage()}
              height={calculateSizeOfImage()}
              border={0}
              scale={scale}
              borderRadius={radius}
              rotate={rotate}
            />

            {hovered === true ?
              this.renderOptionForUploadImage()
              : null}
            <input
              ref={(ref) => this.upload = ref}
              accept='image/*'
              style={{display: 'none'}}
              id={INPUT_FIELD_IMG_ID}
              type={INPUT_TYPE__FILE}
              onClick={(event) => {
                event.target.value = null
              }}
              onChange={(event) => {
                imageUploadCallBack(event.target.files)
                event.target.value = null
              }}
            />
          </div>
        </div>
        {this.renderOptionsForEditImage()}
      </div>
    )
  }
}

const mapStateToProps = state => {
  return {
    screen: state.screen,
    filterData: state.filterData
  }
}

export default connect(mapStateToProps)(withStyles(styles)(AvatarEditorImage))