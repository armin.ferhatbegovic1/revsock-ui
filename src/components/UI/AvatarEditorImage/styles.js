import Colors from '../../../utils/Colors'
import Dimensions from '../../../utils/Dimensions'
import { deviceType } from '../../../config/Constants'

//const IMAGE_HEIGHT = 250

const styles = {
  slider: {
    marginTop: Dimensions.doubleBaseMargin,
    padding: '22px 0px'
  },

  btnContainer: {
    margin: '10px 20px',
    color: Colors.white,
    textTransform: 'unset'
  },

  userImage: {
    border: '4px solid ' + Colors.yellow,
    //  borderRadius: '50%',
    boxShadow: '5px 5px 5px 5px' + Colors.color5,
    margin: '0 auto'
  },
  miniUserImage: {
    width: Dimensions.userImageMiniWidth,
    height: Dimensions.userImageMiniHeight
    //   borderRadius: '50%'
  },
  miniUserImage2: {
    width: Dimensions.userImageMiniWidth,
    height: Dimensions.userImageMiniHeight
    // borderRadius: '50%'
  },

  userImageAddButtonContainer: {
    position: 'relative',
    backgroundColor: Colors.secondary,
    //  borderBottomLeftRadius: IMAGE_HEIGHT / 2,
    // borderBottomRightRadius: IMAGE_HEIGHT / 2,
    opacity: '0.7'

  },
  imageUploadButton: {
    marginTop: 0
  },
  dealImageContainer: {
    margin: Dimensions.doubleBaseMargin,
    paddingLeft: Dimensions.doubleBaseMargin,
    paddingRight: Dimensions.doubleBaseMargin
  },
  imageEditOptionsContainerVisible: {
    ...Dimensions.getDevice() !== deviceType.PHONE ? {visibility: 'visible'} : {
      display: 'flex',
      flexDirection: 'column'
    }
  },
  imageEditOptionsContainerHidden: {
    ...Dimensions.getDevice() !== deviceType.PHONE ? {visibility: 'hidden'} : {display: 'none'}
  }
}

export default styles