import React from 'react'
import {connect} from "react-redux";
import {withRouter} from "react-router-dom";
import {withStyles} from "@material-ui/core";
import RootContainerStyles from "../../../containers/Styles/RootContainerStyles";
import Colors from '../../../utils/Colors'
import Grid from "@material-ui/core/Grid";
import Dimensions from "../../../utils/Dimensions";

const styles = {
    ...RootContainerStyles,
    title: {
        color: Colors.primary,
        fontSize: Dimensions.textMedium
    },
    containerSection: {
        borderBottom: '3px solid #E7E7E7',
        marginLeft: 16,
        marginRight: 16
    }
}

const SectionMainTitle = (props) => {
    const {children} = props
    return <Grid item xs={12} sm={12} style={styles.containerSection}><label
        style={Object.assign({}, styles.title)}>{children}</label></Grid>
}

const mapStateToProps = state => ({
    auth: state.auth,
    screen: state.screen
})

export default connect(mapStateToProps)(withRouter(withStyles(styles)(SectionMainTitle)))
