import React from 'react'
import {connect} from "react-redux";
import {withRouter} from "react-router-dom";
import {withStyles} from "@material-ui/core";
import RootContainerStyles from "../../../containers/Styles/RootContainerStyles";

const styles = {
    ...RootContainerStyles
}

const RedirectButton = (props) => {
    const {label, route, history} = props
    return <label className='redirectButton' onClick={() => history.push(route)}
                  style={Object.assign({}, styles.redirectMainButton)}>{label}</label>
}

const mapStateToProps = state => ({
    auth: state.auth,
    screen: state.screen
})

export default connect(mapStateToProps)(withRouter(withStyles(styles)(RedirectButton)))
