import React from 'react'
import styles from './styles'
import Button from '@material-ui/core/Button'

const subMainButton = (props) => {
  const {label, disabled, buttonType, type,mini,onClick} = props
  return <Button variant='contained' style={Object.assign({}, mini?styles.buttonMini:styles.button, styles[buttonType])} onClick={onClick}
    disabled={disabled} type={type ? type : ''}>
    {label}
  </Button>
}

export default subMainButton

