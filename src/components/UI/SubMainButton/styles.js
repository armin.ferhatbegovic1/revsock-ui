import RootComponentStyles from '../../Styles/RootComponentStyles'
import Colors from '../../../utils/Colors'
import Dimensions from "../../../utils/Dimensions";

const styles = {
  ...RootComponentStyles,
  button:{
    fontFamily: "PingFang Regular",
    borderRadius: 40,
    width:Dimensions.doubleBaseMargin*7,
    padding:Dimensions.baseMargin,
    fontWeight: 'bold',
    textTransform:'unset',
    margin:Dimensions.baseMargin,
  //  border:'1px solid #3324FF'
  },
  buttonMini:{
    borderRadius: 30,
    fontFamily: "PingFang Regular",
    // width:Dimensions.doubleBaseMargin*6,
    minWidth:Dimensions.doubleBaseMargin*6,
    //addingTop:Dimensions.baseMargin,
    // paddingBottom:Dimensions.baseMargin,
    paddingTop:Dimensions.baseMargin-2,
    paddingBottom:Dimensions.baseMargin-2,
    paddingLeft:15,
    paddingRight:15,
    textTransform:'unset',
    backgroundColor:Colors.blue,
 //   border:'1px solid #3324FF',

  },
  primary: {
    color: Colors.secondary,
    backgroundColor: Colors.gray
  },
  secondary: {
    boxShadow: '0px 0px 16px 0px #0B7FFF',
    color: Colors.white,
    backgroundColor: '#0B7FFF'
  },
  tertiary: {
    color: Colors.secondary,
    backgroundColor: Colors.secondaryLight3
  }
}

export default styles
