import React from 'react'
import {connect} from "react-redux";
import {withRouter} from "react-router-dom";
import {withStyles} from "@material-ui/core";
import RootContainerStyles from "../../../containers/Styles/RootContainerStyles";
import Colors from '../../../utils/Colors'
import Dimensions from "../../../utils/Dimensions";
import {isEmpty} from '../../../services'

const styles = {
    ...RootContainerStyles,
    title: {
        fontFamily: "PingFang Bold",
        lineHeight:'45px',
        color: Colors.textBlackColor,
        fontSize: Dimensions.textLarge,
        marginLeft: 16,

    },
    innerSectionTitleContainer: {
        width: Dimensions.fullWidth,
        flex:1,
        textAlign: 'left'

    },
    bottomLine: {
        borderBottom: '3px solid #E7E7E7',

    }
}

const InnerSectionTitle = (props) => {
    const {children, bottomLine} = props
    let bottomLineTmp = !isEmpty(bottomLine) ? bottomLine : true
    return <div style={Object.assign({}, styles.innerSectionTitleContainer, bottomLineTmp ? styles.bottomLine : {})}>
        <label
            style={Object.assign({}, styles.title)}>{children}</label></div>
}

const mapStateToProps = state => ({
    auth: state.auth,
    screen: state.screen
})

export default connect(mapStateToProps)(withRouter(withStyles(styles)(InnerSectionTitle)))
