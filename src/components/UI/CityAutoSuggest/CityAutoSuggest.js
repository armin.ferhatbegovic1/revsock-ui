import React from 'react'
import AutoSuggest from '../AutoSuggest/AutoSuggest'

const GET_CITIES_BY_COUNTRY = '/cities?country='
const cityAutoSuggest = (props) => {
  const {label, placeholder, countryCode, onChange, isValid, value, disabled, style} = props
  const requestLink = GET_CITIES_BY_COUNTRY + countryCode + '&q='
  return (<AutoSuggest label={label} placeholder={placeholder} requestLink={requestLink} fieldForShow='cityName'
    valueHandler={onChange} isValid={isValid} value={value} disabled={disabled} style={style} />)
}

export default cityAutoSuggest