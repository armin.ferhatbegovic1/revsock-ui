import React, {Component} from 'react'
import SelectListMultiple from '../SelectListMultiple/SelecListMultiple'
import {isEmpty} from '../../../services/index'
import connect from 'react-redux/es/connect/connect'

class SelectListCategories extends Component {
    constructor(props) {
        super(props)
        this.state = {
            categories: []
        }
    }

    async getCategoriesList() {
        //  return await getRequest(itemEndpoints.LIST_CATEGORIES)
    }

    async componentDidMount() {
        const categories = await this.getCategoriesList()
        this.setState({categories})
    }

    async componentWillReceiveProps(nextProps) {
        if (nextProps.formDialog !== this.props.formDialog) {
            const categories = await this.getCategoriesList()
            this.setState({categories})
        }
    }

    render() {
        const {categories} = this.state
        const {onChange, handleDelete, error, valueProperty, disabled, placeholder, errorMessage, required, label} = this.props
        return (
            !isEmpty(categories) ? (
                <SelectListMultiple valueProperty={valueProperty} fieldName={'dealCategory'} fieldForShow='name'
                                    handleChange={onChange} placeholder={placeholder} label={label}
                                    itemsList={categories} required={required} errorMessage={errorMessage} error={error}
                                    disabled={disabled}
                                    handleDelete={handleDelete}/>
            ) : null)
    }
}

const mapStateToProps = state => {
    return {
        formDialog: state.formDialog
    }
}

export default connect(mapStateToProps)(SelectListCategories)