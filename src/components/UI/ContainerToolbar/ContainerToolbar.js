import React from 'react'
import styles from './styles'
import Toolbar from '@material-ui/core/Toolbar/Toolbar'
import AppBar from '@material-ui/core/AppBar/AppBar'
import Typography from '@material-ui/core/Typography/Typography'

export const ContainerToolbar = (props) => {
  return (
    <AppBar position='static' color='default' style={styles.appBarContainer}>
      <Toolbar >
        <Typography variant='h5' color='inherit'>
          {props.title}
        </Typography>
        {props.children}
      </Toolbar>
    </AppBar>
  )

}

export default ContainerToolbar
