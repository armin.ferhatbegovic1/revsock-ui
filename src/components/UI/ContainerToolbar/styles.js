import RootComponentStyles from '../../Styles/RootComponentStyles'
import Colors from '../../../utils/Colors'
import Dimensions from '../../../utils/Dimensions'

const styles = {
  ...RootComponentStyles,
  appBarContainer: {
    color: Colors.black,
    backgroundColor: Colors.toolbarColor,
    //  marginTop:'-10px',
    height:Dimensions.baseMargin*8,
    justifyContent: 'center'
  },
  toolbarContainer:{
    display: 'flex',
    justifyContent: 'space-between'
  }

}
export default styles










