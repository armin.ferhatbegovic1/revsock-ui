import React from "react";
import Dimensions from "../../../utils/Dimensions";
import RootComponentStyles from "../../Styles/RootComponentStyles";
import Colors from "../../../utils/Colors";

const styles = {
    ...RootComponentStyles,

    title: {
        textAlign: 'left',
        marginLeft: Dimensions.doubleBaseMargin,
        color: 'black',
        fontSize: Dimensions.textMedium,
        fontWeight: 'bold',
        marginTop: Dimensions.baseMargin
    },
    subtitle: {
        textAlign: 'left',
        marginLeft: Dimensions.doubleBaseMargin,
        color: Colors.shortNameColor
    },
    column: {
        display: 'flex',
        flexDirection: 'column',
        color: Colors.graySecondary,
        fontSize: Dimensions.textXSmall,
        fontWeight: 'bold'
    },
    verticalCenter: {
        alignSelf: 'center'
    },
    container: {
        padding: Dimensions.doubleBaseMargin
    },
    titleMini: {
        textAlign: 'left',
        marginLeft: Dimensions.baseMargin,
        color: 'black',
        fontSize: Dimensions.textXSmall,
        marginTop: Dimensions.baseMargin
    },
    subtitleMini: {
        textAlign: 'left',
        paddingTop: Dimensions.baseMargin / 2,
        marginLeft: Dimensions.baseMargin,
        fontSize: Dimensions.textXXXSmall,
    }
}

const PercentTasksStatus = (props) => {
    const {label, status, children, mini} = props
    return <div style={Object.assign({}, styles.row, styles.container)}>
        <div style={Object.assign({}, styles.column, styles.verticalCenter)}>
            {children}
        </div>
        <div style={styles.column}>
            <label style={mini ? styles.titleMini : styles.title}>{label}</label>
            <label style={mini ? styles.subtitleMini : styles.subtitle}>{status}</label>
        </div>
    </div>
}
export default PercentTasksStatus