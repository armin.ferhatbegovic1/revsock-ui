import React, { Component } from 'react'
import FormControl from '@material-ui/core/FormControl'
import MenuItem from '@material-ui/core/MenuItem'
import Select from '@material-ui/core/Select/Select'
import Input from '@material-ui/core/Input/Input'
import Chip from '@material-ui/core/Chip/Chip'
import InputLabel from '@material-ui/core/InputLabel'
import FormHelperText from '@material-ui/core/FormHelperText'
import styles from './styles'
import ClickAwayListener from '@material-ui/core/ClickAwayListener/ClickAwayListener'

const ITEM_HEIGHT = 48
const ITEM_PADDING_TOP = 8
const MenuProps = {
  PaperProps: {
    style: {
      maxHeight: ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP,
      width: 250
    }
  }
}

class SelectListMultiple extends Component {

  handleClickAway = () => {
    //  console.log('Clicked away')
  }

  changeIndex () {
    const {valueProperty, itemsList, fieldForShow} = this.props
    const selectedValues = []
    for (let i = 0; i < valueProperty.length; i++) {
      selectedValues.push(itemsList.find(item => item[fieldForShow] === valueProperty[i][fieldForShow]))
    }
    return selectedValues
  }

  render () {
    const {itemsList, fieldForShow, style, error, disabled, handleDelete, handleChange, label, placeholder, errorMessage, required} = this.props

    return (
      <FormControl style={Object.assign({}, styles.formControl, styles.selectListContainer)} error={!error}>
        <InputLabel htmlFor='select-multiple-chip'>{placeholder}</InputLabel>
        <ClickAwayListener onClickAway={this.handleClickAway}>
          <Select
            style={style}
            label={label}
            multiple
            value={this.changeIndex()}
            onChange={handleChange}
            input={<Input inputProps={{required}} />}
            disabled={disabled}
            renderValue={selected => (
              <div style={styles.chips}>
                {selected.map(value => (
                  !disabled ? <Chip key={value['_id']} label={value[fieldForShow]} style={styles.chips}
                      onDelete={handleDelete(value[fieldForShow])} /> :
                    <Chip key={value['_id']} label={value[fieldForShow]} style={styles.categoryChip} />
                ))}
              </div>
            )}
            MenuProps={MenuProps}
          >
            {itemsList.map(name => (
              <MenuItem key={name['_id']} value={name} onClick={true}>
                {name[fieldForShow]}
              </MenuItem>
            ))}
          </Select>
        </ClickAwayListener>
        {!error ? <FormHelperText id='component-error-text'>{errorMessage}</FormHelperText> : ''}
      </FormControl>
    )
  }
}

export default SelectListMultiple