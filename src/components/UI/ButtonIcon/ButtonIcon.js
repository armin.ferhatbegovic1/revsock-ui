import React from 'react'
import { withStyles } from '@material-ui/core/styles'
import Fab from '@material-ui/core/Fab'
import styles from './styles'

function ButtonIcon (props) {
  const {handleAction, children, buttonType, mini,style,disabled} = props
  return (
    <Fab color='primary' style={Object.assign({},styles[buttonType+'Button'], mini? styles.miniButton: null,style)} aria-label='Add' onClick={() => {handleAction()}} disabled={disabled}>
      {children}
    </Fab>
  )
}

export default withStyles(styles)(ButtonIcon)
