import React, { Component } from 'react'
import Button from '@material-ui/core/Button'
import Dialog from '@material-ui/core/Dialog'
import DialogActions from '@material-ui/core/DialogActions'
import DialogContent from '@material-ui/core/DialogContent'
import DialogContentText from '@material-ui/core/DialogContentText'
import DialogTitle from '@material-ui/core/DialogTitle'
import { DIALOG_MESSAGE, DIALOG_OPEN, DIALOG_ACTION } from '../../../store/actions/types'
import { alertDialogType } from '../../../config/Constants'
import { localize, isEmpty } from '../../../services'
import { connect } from 'react-redux'
import Colors from '../../../utils/Colors'

function mapStateToProps (store) {
  return {
    open: store.dialog.open,
    message: store.dialog.message,
    type: store.dialog.type,
    title: store.dialog.title,
    action: store.dialog.action
  }
}

class AlertDialog extends Component {

  handleClose = () => {
    this.closeDialog()
  }

  closeDialog = () => {
    const {dispatch} = this.props
    dispatch({type: DIALOG_OPEN, payload: false})
    dispatch({type: DIALOG_ACTION, payload: null})
    setTimeout(
      function () {
        dispatch({type: DIALOG_MESSAGE, payload: ''})
      },
      500
    )
  }

  getTitle () {
    const {type} = this.props
    switch (type) {
      case alertDialogType.WARNING:
        return localize('warningMessage')
      case alertDialogType.ERROR:
        return localize('errorMessage')
      case alertDialogType.INFO:
        return localize('infoMessage')
      case alertDialogType.CONFIRM:
        return localize('confirmMessage')
      case alertDialogType.DEFAULT:
        return ''
      default:
        return null
    }
  }

  handleAction = () => {
    const {action} = this.props
    action()
    this.handleClose()
  }

  getButtons () {
    const {type} = this.props
    let button = ''
    switch (type) {
      case alertDialogType.WARNING:
        button =
          <DialogActions><Button onClick={this.handleClose} color='primary'>{localize('ok')}</Button></DialogActions>
        break
      case alertDialogType.ERROR:
        button = localize('errorMessage')
        break
      case alertDialogType.INFO:
        button = localize('infoMessage')
        break
      case alertDialogType.CONFIRM:
        button = <DialogActions><Button onClick={this.handleAction} color='primary'>{localize('yes')}</Button><Button
          onClick={this.handleClose} autoFocus color='primary'>{localize('cancel')}</Button></DialogActions>
        break
      case alertDialogType.DEFAULT:
        button = ''
        break
      default:
        return null
    }
    return button
  }

  renderContent () {
    const {type, message} = this.props
    let dialogContent = ''
    switch (type) {
      case alertDialogType.DEFAULT:
        dialogContent = message
        break

      default:
        dialogContent = <DialogContent><DialogContentText style={{paddingTop:24}}>{message}</DialogContentText> </DialogContent>
    }
    return dialogContent
  }

  render () {
    const {open, title} = this.props
    let dialogTitle = !isEmpty(title) ? title : this.getTitle()
    return (
      <div>
        <Dialog
          open={open}
          onClose={this.handleClose}
          aria-labelledby='alert-dialog-title'
          aria-describedby='alert-dialog-description'
        >
          {dialogTitle ? <DialogTitle style={{backgroundColor: Colors.secondary}}>
            <label style={{color: Colors.white}}>{dialogTitle}</label>
          </DialogTitle> : null}
          {this.renderContent()}
          {this.getButtons()}
        </Dialog>
      </div>
    )
  }
}

export default connect(mapStateToProps)(AlertDialog)