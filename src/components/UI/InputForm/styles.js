import RootComponentStyles from '../../Styles/RootComponentStyles'
import Dimensions from '../../../utils/Dimensions'
import Colors from '../../../utils/Colors'

const styles = {
    ...RootComponentStyles,
    formContainer: {
        flexDirection: 'column',
      /*  paddingLeft: Dimensions.tripeBaseMargin + 10,
        paddingRight: Dimensions.tripeBaseMargin + 10,*/
        paddingTop: Dimensions.doubleBaseMargin,
        paddingBottom: Dimensions.doubleBaseMargin
    },
    fieldContainer: {
        display: 'flex'
    },
    submitButton: {
        display: 'flex',
        justifyContent: 'center'
    },
    editButton: {
        display: 'flex',
        justifyContent: 'flex-end'
    },
    formElementFieldCenter: {
        /* alignItems: 'center',*/
        flexDirection: 'column',
        display: 'flex',
        justifyContent: 'space-around'
    },
    countryCityContainer: {
        display: 'flex',
        color: Colors.secondary,
        fontWeight: 'bold',
        fontStyle: 'italic'
    },
    countryCityField: {
        maxWidth: Dimensions.inputFields.inputXXLarge * 2
    },
    uploadImageContainer: {
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center'
    },
    alignSelfCenter: {
        alignSelf: 'center'
    },
    uploadImageButton: {
        marginTop: 20
    },
    propertyValueField: {
        color: Colors.secondary,
        fontWeight: 'bold',
        fontStyle: 'italic'
        /* fontSize: Dimensions.getDevice() === deviceType.LARGE_DESKTOP ? Dimensions.textSmall : Dimensions.textXXXSmall*/
    },
    formItem: {
        marginTop: Dimensions.baseMargin,
        marginBottom: Dimensions.baseMargin
    },
    textFieldButtonRow: {
        display: 'flex',
        flexDirection: 'row'
    },
    buttonWithTextField: {
        flex: '30%'
    },
    textFieldWithButtonLeft: {
        flex: '70%'
    },
    mainButtonWithTextField: {
        borderRadius: 0,
        marginTop: 0,
      marginBottom: 0,
      padding: '11.5px 16px',
      marginLeft:'-3px'
    }
}

export default styles