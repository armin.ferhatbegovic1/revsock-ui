import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Snackbar, SnackbarContent } from '@material-ui/core'
import Colors from '../../../utils/Colors'
import { TOAST_OPEN, TOAST_MESSAGE } from '../../../store/actions/types'
import { TOAST_HIDE_TIME, toastType } from '../../../config/Constants'

const styles = {
  toastContent: {
    width: 200,
    height: 60
  }
}

function mapStateToProps (store) {
  return {
    open: store.toast.open,
    message: store.toast.message,
    type: store.toast.type
  }
}

class AppToast extends Component {
  handleAppToastClose = () => {
    const {dispatch} = this.props
    dispatch({type: TOAST_OPEN, payload: false})
    dispatch({type: TOAST_MESSAGE, payload: ''})
  }

  render () {
    const {open, message, type} = this.props
    return (
      <Snackbar
        anchorOrigin={{vertical: 'bottom', horizontal: 'right'}}
        autoHideDuration={TOAST_HIDE_TIME}
        onClose={this.handleAppToastClose}
        open={open}
        snackbarcontentprops={{
          'aria-describedby': 'snackbar-message-id'
        }}
      >
        <SnackbarContent
          style={Object.assign({}, styles.toastContent, {
            background: type === toastType.SUCCESS ? Colors.positive : Colors.alert
          })}
          message={message}
        />
      </Snackbar>
    )
  }
}

export default connect(mapStateToProps)(AppToast)
