import RootComponentStyles from '../../Styles/RootComponentStyles'
import Colors from '../../../utils/Colors'

const styles = theme => ({
  ...RootComponentStyles,
  root: {
   // height: 70,
    flexGrow: 1,
    //  width:Dimensions.inputFields.input3XLarge,
    //  maxWidth:Dimensions.inputFields.input3XLarge,
    margin: '0 auto'
  },
  container: {
    position: 'relative'
  },
  suggestionsContainerOpen: {
    position: 'absolute',
    zIndex: 1,
    marginTop: 1,
    left: 0,
    right: 0
  },
  suggestion: {
    display: 'block'
  },
  suggestionsList: {
    margin: 0,
    padding: 0,
    listStyleType: 'none'
  },
  divider: {
    height: 1 * 2
  },
  propertyValueField:{
    color: Colors.secondary,
    lineHeight:1
  }
})
export default styles