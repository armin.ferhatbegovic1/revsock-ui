import React, { Component } from 'react'
import Autosuggest from 'react-autosuggest'
import match from 'autosuggest-highlight/match'
import parse from 'autosuggest-highlight/parse'
import TextField from '@material-ui/core/TextField'
import Paper from '@material-ui/core/Paper'
import MenuItem from '@material-ui/core/MenuItem'
import Popper from '@material-ui/core/Popper'
import { withStyles } from '@material-ui/core/styles'
import FormHelperText from '@material-ui/core/FormHelperText'
import styles from './styles'
import { getRequest, isEmpty } from '../../../services'
import Dimensions from '../../../utils/Dimensions'

class AutoSuggest extends Component {
  constructor (props) {
    super(props)
    this.state = {
      popper: !isEmpty(props.value) ? props.value : '',
      suggestions: [],
      requestLink: props.requestLink,
      fieldForShow: props.fieldForShow,
      errorMessage: props.errorMessage,
      isValid: props.isValid
    }
  }

  componentWillUnmount () {
    this.setState({popper: ''})
  }

  componentWillReceiveProps (nextProps) {
    if (nextProps.value !== this.props.value) {
      this.setState({popper: nextProps.value})
    }
  }

  renderInputComponent (inputProps) {
    const {classes, disabled, inputRef = () => {}, ref, ...other} = inputProps
    return (
      <TextField
        fullWidth
        disabled={disabled}
        InputProps={{
          classes: {
            input: (disabled ? classes.propertyValueField : '') + ' ' + classes['propertyValueField' + Dimensions.getDevice()]
          },
          inputRef: node => {
            ref(node)
            inputRef(node)
          }
        }}
        {...other}
      />
    )
  }

  getSuggestions (value) {
    const {requestLink} = this.props
    return (async () => {
      const inputWord = value.trim()
      const inputValue = inputWord.charAt(0).toUpperCase() + inputWord.slice(1)
      const link = requestLink + inputValue
      try {
        return await getRequest(link)
      } catch (e) {
      }
    })()
  }

  renderSuggestion = (suggestion, {query, isHighlighted}) => {
    const {fieldForShow} = this.state
    const matches = match(suggestion[fieldForShow], query)
    const parts = parse(suggestion[fieldForShow], matches)

    return (
      <MenuItem selected={isHighlighted} component='div'>
        <div>
          {parts.map((part, index) =>
            part.highlight ? (
              <span key={String(index)} style={{fontWeight: 500}}>
              {part.text}
            </span>
            ) : (
              <strong key={String(index)} style={{fontWeight: 300}}>
                {part.text}
              </strong>
            )
          )}
        </div>
      </MenuItem>
    )
  }

  getSuggestionValue = (suggestion) => {
    const {valueHandler} = this.props
    const {fieldForShow} = this.state
    valueHandler(suggestion)
    return suggestion[fieldForShow]
  }

  handleSuggestionsFetchRequested = ({value}) => {
    (async () => {
      const suggestions = await this.getSuggestions(value)
      if (!isEmpty(suggestions)) {
        this.setState({suggestions})
      }
    })()
  }

  handleSuggestionsClearRequested = () => {
    this.setState({
      suggestions: []
    })
  }

  handleChange = name => (event, {newValue}) => {
    this.setState({
      [name]: newValue
    })
  }

  errorText () {
    const {errorMessage} = this.state
    const {isValid} = this.props
    return isValid ? null : <FormHelperText error>{errorMessage}</FormHelperText>
  }

  render () {
    const {classes, label, placeholder, isValid, disabled, style} = this.props
    const autoSuggestProps = {
      renderInputComponent: this.renderInputComponent,
      suggestions: this.state.suggestions,
      onSuggestionsFetchRequested: this.handleSuggestionsFetchRequested,
      onSuggestionsClearRequested: this.handleSuggestionsClearRequested,
      getSuggestionValue: this.getSuggestionValue,
      renderSuggestion: this.renderSuggestion
    }
    return (
      <div className={classes.root}>
        <div />
        <Autosuggest
          {...autoSuggestProps}
          inputProps={{
            disabled: disabled,
            error: !isValid,
            classes,
            style: style,
            label,
            placeholder,
            value: this.state.popper,
            onChange: this.handleChange('popper'),
            inputRef: node => {
              this.popperNode = node
            },
            InputLabelProps: {
              shrink: true
            }
          }}
          theme={{
            suggestionsList: classes.suggestionsList,
            suggestion: classes.suggestion,
            style: style
          }}
          renderSuggestionsContainer={options => (
            <Popper anchorEl={this.popperNode} open={Boolean(options.children)}>
              <Paper

                square
                {...options.containerProps}
                style={{width: this.popperNode ? this.popperNode.clientWidth : null}}
              >
                {options.children}
              </Paper>
            </Popper>
          )}
        />
        {this.errorText()}
      </div>
    )
  }
}

export default withStyles(styles)(AutoSuggest)