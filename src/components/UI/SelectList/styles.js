import RootContainerStyles from '../../Styles/RootComponentStyles'
import Dimensions from '../../../utils/Dimensions'
const styles = {
  ...RootContainerStyles,
  selectList: {
    flex: 1,
    width:Dimensions.fullWidth,
    maxWidth:Dimensions.inputFields.input3XLarge
  }

}

export default styles