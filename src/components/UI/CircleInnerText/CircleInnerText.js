import React from 'react'
import RootContainerStyles from "../../../containers/Styles/RootContainerStyles";
import Colors from '../../../utils/Colors'
import Dimensions from "../../../utils/Dimensions";

const circleR = Dimensions.tripeBaseMargin + 5
const styles = {
    ...RootContainerStyles,
    mainWrapper: {
        width: circleR,
        height: circleR,
        borderRadius: 50,
        lineHeight: circleR + 'px',
        border: '2px solid rgba(210,210,210,1)',
        textAlign: 'center',
    },
    activeWrapper: {
        border: '2px solid ' + Colors.primary,
        color: Colors.primary
    },
    circleLabel: {
        fontSize: Dimensions.textMedium
    }
}

const CircleInnerText = (props) => {
    const {value, label} = props
    const [active, setActive] = React.useState(false);
    const clicked = event => {
        setActive(!active);
    };

    return <div style={Object.assign({}, styles.mainWrapper, active ? styles.activeWrapper : {})}
                className='pointerButton' onClick={clicked}>
        <label className='pointerButton' onClick={clicked} style={styles.circleLabel}>{label}</label>
    </div>

}


export default CircleInnerText
