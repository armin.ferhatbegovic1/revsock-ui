import React from 'react'
import {connect} from "react-redux";
import {withRouter} from "react-router-dom";
import {withStyles} from "@material-ui/core";
import RootContainerStyles from "../../../containers/Styles/RootContainerStyles";
import Colors from '../../../utils/Colors'
import Grid from "@material-ui/core/Grid";
import Dimensions from "../../../utils/Dimensions";

const styles = {
    ...RootContainerStyles,
    title: {
        color: Colors.textBlackColor,
        fontSize: Dimensions.textMedium,
        marginLeft:16
    }
}

const SectionSubTitle = (props) => {
    const {children} = props
    return <Grid item xs={12} sm={12}><label
        style={Object.assign({}, styles.title)}>{children}</label></Grid>
}

const mapStateToProps = state => ({
    auth: state.auth,
    screen: state.screen
})

export default connect(mapStateToProps)(withRouter(withStyles(styles)(SectionSubTitle)))
