import RootComponentStyles from '../../Styles/RootComponentStyles'
import Dimensions from '../../../utils/Dimensions'
import Colors from "../../../utils/Colors";

const styles = {
  ...RootComponentStyles,
  mainButton: {
    fontFamily: "PingFang Regular",
    fontWeight: 'bold',
    fontSize: Dimensions.textSmall-2,
    color: Colors.white,
    textTransform:'unset',
    width: Dimensions.fullWidth,
  /*  marginTop:Dimensions.baseMargin,
    marginBottom:Dimensions.baseMargin*/

  }
}

export default styles