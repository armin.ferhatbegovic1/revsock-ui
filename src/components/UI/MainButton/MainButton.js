import React from 'react'
import styles from './styles'
import Button from '@material-ui/core/Button'
import {isEmpty} from '../../../services'

const mainButton = (props) => {
    const {label, disabled, buttonType, type, width, style} = props
    let maxWidth = !isEmpty(width) ? {maxWidth: width} : {}
    let tmpStyle=!isEmpty(style)?style:{}
    return <Button variant='contained'
                   style={Object.assign({},{...tmpStyle}, maxWidth, buttonType === 'primary' ? styles.primaryButton : styles.secondaryButton, styles.mainButton)}
                   disabled={disabled} type={type ? type : ''}>
        {label}
    </Button>
}

export default mainButton