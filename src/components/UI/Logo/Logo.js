import React from 'react'
import styles from './styles'
import Images from '../../../utils/Images'
import { isEmpty, localize } from '../../../services'
import { goToHomeScreen } from '../../../services/GlobalServices'
import Dimensions from "../../../utils/Dimensions";

const Logo = ({mini, isTablet, header, history}) => {
  const goToLoginScreen = () => !isEmpty(history) ? goToHomeScreen(history) : null
  const idLogo = !isEmpty(history) ? 'rootLogo' : 'logo'

  return <div
    style={header ? styles.logoHeaderContainer : styles.logoContainer} onClick={goToLoginScreen} id={idLogo}>
    <img
      src={Images.logo}
      style={Dimensions.getWidth()>600?styles.logo:styles.logoMobile}
      alt={localize('logo')}
    />
  </div>
}

export default Logo
