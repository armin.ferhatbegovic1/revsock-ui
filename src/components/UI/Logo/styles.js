import RootComponentStyles from '../../Styles/RootComponentStyles'
import Dimensions from '../../../utils/Dimensions'

const styles = {
    ...RootComponentStyles,
    logo: {
        width: 30,
        height: 29
    },
    logoMobile: {
        width: 30,
        height: 29
    },
    miniLogo: {
        width: 'auto',
        height: Dimensions.logoMiniHeight
    },
    logoContainer: {
        width: 200,
        display: 'flex',
        justifyContent: 'center'
    },
    logoHeaderContainer: {
        marginTop: 0,
        display: 'flex',
        justifyContent: 'start'
    }
}

export default styles
