import styles from '../../Section2/styles'
import React from 'react'

const SectionImageWithLabel = (props) => {
    const {label,white} = props
    return <div style={Object.assign({}, styles.flexDirectionColumn, {alignItems: 'center'})}>
        <label style={white?styles.sectionTitleWhite:styles.sectionTitle}>{label}</label>
    </div>
}
export default SectionImageWithLabel
