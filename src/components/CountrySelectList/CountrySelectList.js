
import React, { Component } from 'react'
import Select, { components } from 'react-select'



export default class InfluencersForm extends Component {
    constructor() {
        super();
        this.handleInfluencerName = this.handleInfluencerName.bind(this);
    }
    handleInfluencerName(event) {
        console.log(event);
    }
    render() {
        const influencers = [
            { value: "abc", label: "abc" },
            { value: "def", label: "def" }
        ];

        const ValueContainer = ({ children, ...props }) => {
            return (
                components.ValueContainer && (
                    <components.ValueContainer {...props}>
                        {!!children && (
                            <i
                                className="fa fa-search"
                                aria-hidden="true"
                                style={{ position: 'absolute', left: 6,maxWidth:400}}
                            />
                        )}
                        {children}
                    </components.ValueContainer>
                )
            );
        };

        const DropdownIndicator = props => {
            return (
                components.DropdownIndicator && (
                    <components.DropdownIndicator {...props}>
                        <i
                            className="fa fa-search"
                            aria-hidden="true"
                        />
                    </components.DropdownIndicator>
                )
            );
        };

        const styles = {
            valueContainer: base => ({
                ...base,
                paddingLeft: 24
            })
        }

        return (
            <div >
                <div>
                    <Select
                        options={influencers}
                        isMulti={false}
                        onChange={this.handleInfluencerName}
                        isSearchable={true}
                        components={{ DropdownIndicator, ValueContainer }}
                        classNamePrefix="vyrill"
                        styles={styles}
                    />
                </div>
            </div>
        );
    }
}