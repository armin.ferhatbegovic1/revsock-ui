export const LayoutType = {
    empty: 'Empty',
    dashboard: 'Dashboard',
    home: 'Home'
}

export const role = {
    ADMIN: 'Admin',
    USER: 'User'
}

export const uploadDealImage = {
    MAX_UPLOAD_DEAL_IMAGE_WIDTH: 2000,
    MAX_UPLOAD_DEAL_IMAGE_HEIGHT: 2000,
    MIN_UPLOAD_DEAL_IMAGE_WIDTH: 400,
    MIN_UPLOAD_DEAL_IMAGE_HEIGHT: 400
}

export const alertDialogType = {
    WARNING: 'Warning',
    ERROR: 'Error',
    INFO: 'Info',
    CONFIRM: 'Confirm',
    DEFAULT: 'Default'
}

export const table = {
    TABLE_ROWS_PER_PAGE: 10
}

export const response = {
    OK: 200,
    BAD: 400
}
export const request = {
    GET: 'GET',
    POST: 'POST',
    DELETE: 'DELETE',
    PUT: 'PUT'
}
export const toastNotification = {
    SHOW: true,
    HIDE: false
}
export const toastType = {
    SUCCESS: 'success',
    FAILURE: 'failure'
}

export const dateFormat = {
    YYYYMMDD: 'YYYY-MM-DD',
    MMDDYYYY: 'MM-DD-YYYY',
    MMMMDDYYYY: 'MMMM DD, YYYY'
}

export const formMode = {
    EDIT: 'EDIT',
    NEW: 'NEW',
    VIEW: 'VIEW'
}

export const dealFormCode = {
    VALID_FORM: 'VALID_FORM',
    INVALID_FORM: 'INVALID_FORM',
    FORM_SAVED: 'FROM_SAVED'
}
export const menuItemType = {
    DIVIDER: 'DIVIDER',
    TITLE: 'TITLE',
    LINK: 'LINK'
}

export const cardType = {
    PLACEHOLDER: 'PLACEHOLDER',
    ORIGINAL: 'ORIGINAL',
    PLACEHOLDER_IMAGE: 'PLACEHOLDER_IMAGE'
}

export const deviceType = {
    PHONE: 'Phone',
    TABLET: 'Tablet',
    DESKTOP: 'Desktop',
    LARGE_DESKTOP: 'LargeDesktop'
}

export const inputTypeFormElement = {
    COUNTRY_AUTO_SUGGEST: 'COUNTRY_AUTO_SUGGEST',
    CITY_AUTO_SUGGEST: 'CITY_AUTO_SUGGEST',
    SELECT_LIST_CATEGORIES: 'SELECT_LIST_CATEGORIES',
    TEXT_FIELD: 'TEXT_FIELD',
    CHECKBOX: 'CHECKBOX',
    SWITCH: 'SWITCH',
    IMAGE: 'IMAGE',
    TEXT_AREA: 'TEXT_AREA',
    SELECT_LIST: 'SELECT_LIST',
    UPLOAD_EDIT_IMAGE: 'UPLOAD_EDIT_IMAGE',
    MAIN_BUTTON: 'MAIN_BUTTON',
    TEXT_FIELD_WITH_BUTTON: 'TEXT_FIELD_WITH_BUTTON'
}

export const layoutType = {
    COLUMNS_1: 12,
    COLUMNS_2: 6,
    COLUMNS_3: 4,
    COLUMNS_4: 3
}

export const positionButton = {
    CENTER: 'center',
    RIGHT: 'flex-end',
    LEFT: 'flex-start'
}
export const dealSaveType =
    {
        NEW_DATA_SAVE: 'NEW_DATA_SAVE',
        DATA_SAVE: 'DATA_SAVE',
        DATA_SAVE_AND_PAY: 'DATA_SAVE_AND_PAY',
        NEW_DATA_SAVE_AND_PAY: 'NEW_DATA_SAVE_AND_PAY'
    }

export const formDialog = {
    SHOW: true, HIDE: false
}
export const MAILERLITE_KEY = 'f3bbd4d92ecf8b5277c6550f5df51137'
export const REDIRECT_PAGE_TIME = 1000
export const TOAST_HIDE_TIME = 3000
export const requestHeader = () => ({
    "Content-Type": "text/plain",
})

export const loaderVisbility = {
    SHOW: true,
    HIDE: false
}

export const THRESHOLD_COUNT = 10
export const BATCH_SIZE = 10

export const autoCloseDialog = {
    AUTO_CLOSE_DIALOG_TRUE: true,
    AUTO_CLOSE_DIALOG_FALSE: false
}
export const subMainButtonType = {
    PRIMARY: 'primary',
    SECONDARY: 'secondary',
    TERTIARY: 'tertiary',
    NEUTRAL: 'neutral'
}
export const buttonTypeHandler = {
    BUTTON: 'button',
    SUBMIT: 'submit'
}

export const itemType = {
    OFFER: 1,
    REQUEST: 2
}

export const USER_TOKEN = 'JWT'
export const TOKEN_EXPIRATION_DATE = 'token-expiration-date'
export const REMEMBER_USER = 'REMEMBER_USER'

export const PUBLISHED_DEALS_SORT_KEY = 'PUBLISHED_DEALS_SORT'

export const rectangleType = {
    BLUE: 'blue',
    GRAY: 'gray'
}
