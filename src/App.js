import React, {Component} from 'react'
import CssBaseline from '@material-ui/core/CssBaseline'
import {MuiThemeProvider} from '@material-ui/core/styles'
import {connect, Provider} from 'react-redux'
import AppToast from './components/UI/AppToast/AppToast'
import AlertDialog from './components/UI/AlertDialog/AlertDialog'
import store from './store'
import RoutesList from './components/Routes/Routes'
import FormDialog from './components/UI/FormDialog/FormDialog'
import Loader from './components/UI/Loader/Loader'
import {withRouter} from 'react-router-dom'
import BrowserRouter from 'react-router-dom/es/BrowserRouter'
import {updateDimensions} from './store/actions/screenAction'
import CookiesProvider from 'react-cookie/cjs/CookiesProvider'
import withCookies from 'react-cookie/cjs/withCookies'
import Cookies from "universal-cookie";
import {USER_TOKEN} from "./config/Constants";
import {isEmpty} from "./services";
import jwt_decode from 'jwt-decode'
import {setCurrentUser} from './store/actions/userActions'
import {logoutUser} from './store/actions/authentication'

class App extends Component {

    componentDidMount() {
        const {dispatch} = this.props
        window.addEventListener('resize', () => dispatch(updateDimensions()))
    }


    async componentWillMount() {
        const {dispatch, history} = this.props
        const cookie = new Cookies()
        const jwtToken = cookie.get(USER_TOKEN)
        if (!isEmpty(jwtToken)) {
            const decoded = jwt_decode(jwtToken)
            dispatch(setCurrentUser(decoded.user))
            // await setUserProfileInfo(dispatch)
            const currentTime = Date.now() / 1000
            if (decoded.exp < currentTime) {
                dispatch(logoutUser(history))
                window.location.href = '/'
            } else {
               // history.push('/')
            }
        }
        window.removeEventListener('resize', this.resize)
    }

    render() {
        const {settings, auth} = this.props
        return (
            <MuiThemeProvider theme={settings.theme}>
                <CssBaseline/>
                <div style={{height: '100vh'}}>
                    <AppToast/>
                    <AlertDialog/>
                    <Loader/>
                    <FormDialog/>
                    <RoutesList authenticate={auth}/>
                </div>
            </MuiThemeProvider>
        )
    }
}

const mapStateToProps = state => {
    return {
        settings: state.settings,
        auth: state.auth,
        screen: state.screen
    }
}
const ConnectedApp = withCookies(withRouter(connect(mapStateToProps)(App)))

const Root = () => {
    return <CookiesProvider><BrowserRouter><Provider store={store}><ConnectedApp/></Provider></BrowserRouter>
    </CookiesProvider>
}
export default Root