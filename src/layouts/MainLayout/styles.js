const styles = theme => ({
    root: {
        display: 'flex',
        marginTop: 0
    },
    content: {
        flexGrow: 1,
        marginLeft: 0,
        paddingLeft: 0,
        paddingRight: 0,
        overflowX: 'hidden',
        overflowY: 'hidden',
        marginTop: 0
    },
    contentShift: {
        transition: theme.transitions.create(['width', 'margin'], {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.enteringScreen
        })
    }
})

export default styles