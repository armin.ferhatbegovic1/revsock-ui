import React, {Component, Fragment} from 'react'
import {withStyles} from '@material-ui/core/styles'
import {connect} from 'react-redux'
import Header from '../../components/Header/Header'
import styles from './styles'
import Dimensions from '../../utils/Dimensions'
import {deviceType} from '../../config/Constants'

class MainLayout extends Component {
    state = {
        open: Dimensions.getDevice() === deviceType.PHONE ? false : true
    }

    handleToggleDrawer = () => {
        this.setState(prevState => {
            return {open: !prevState.open}
        })
    }

    render() {
        const {classes, children, logout} = this.props
        const {open} = this.state
        return (
            <Fragment>
                <div className={classes.root}>
                    <Header
                        logout={logout}
                        handleToggleDrawer={this.handleToggleDrawer}
                        open={open}
                    />
                    <main
                    >
                        {children}
                    </main>
                </div>
            </Fragment>
        )
    }
}

export default connect()(withStyles(styles)(MainLayout))
