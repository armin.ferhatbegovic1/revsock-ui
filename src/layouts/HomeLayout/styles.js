import Colors from "../../utils/Colors";

const drawerWidth = 240;

const styles = theme => ({
  root: {
    display: "flex",
    backgroundColor:Colors.mainBackground,
    height:'98vh'
  },
  content: {
    marginTop:80,
    flexGrow: 1,
  //  padding: 30,

    overflowX: "hidden"
  },
  contentShift: {
    marginLeft: drawerWidth,
    width: `calc(100% - ${drawerWidth}px)`,
    transition: theme.transitions.create(["width", "margin"], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen
    })
  }
})

export default styles